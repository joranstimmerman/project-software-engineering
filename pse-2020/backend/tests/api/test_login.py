from app.models import Account, User
from flask_login import login_user
import pytest


@pytest.fixture
def account_unverified():
    return Account(account_id=1, username="test", email="a@a.a",
        password="test123")


@pytest.fixture
def account_verified():
    return Account(account_id=1, username="test", email="a@a.a",
        password="test123", account_activated=True, user_id=1)


@pytest.fixture
def sample_user():
    return User(user_id=1, blockchain_public_key="abcd")


def test_logout(db, client, account_verified, sample_user):
    db.session.add(sample_user)
    db.session.add(account_verified)
    db.session.commit()

    rv = client.post("/api/logout")

    assert rv.status_code == 200


def test_login_failure(db, client, account_unverified):
    db.session.add(account_unverified)
    db.session.commit()

    rv = client.post("/api/login", json={})

    assert rv.status_code == 400, "Should not accept bad request"

    data = {"email": account_unverified.email, "password": "bad_pwd"}
    rv = client.post("/api/login", json=data)

    assert rv.status_code == 400, "Passwords should match"

    data = {"email": account_unverified.email, "password": account_unverified.password}
    # Hash the password
    account_unverified.set_password(account_unverified.password)
    rv = client.post("/api/login", json=data)

    assert rv.status_code == 400, "Should not accept unverified account"


def test_login(db, client, account_verified, sample_user):
    db.session.add(sample_user)
    db.session.add(account_verified)
    db.session.commit()

    data = {"email": account_verified.email, "password": account_verified.password}
    # Hash the password
    account_verified.set_password(account_verified.password)

    rv = client.post("/api/login", json=data)

    assert rv.status_code == 200, "Should be able to login"
