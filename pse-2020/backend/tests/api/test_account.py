import pytest
from app.models import Account, User
from flask_login import login_user
import json


@pytest.fixture
def sample_account():
    return {
        "username": "test",
        "email": "a@a.a",
        "password": "test123"
    }


@pytest.fixture
def account_unverified():
    return Account(account_id=1, username="test", email="a@a.a",
        password="test123")


@pytest.fixture
def account_verified():
    return Account(account_id=1, username="test", email="a@a.a",
        password="test123", account_activated=True, user_id=1)


@pytest.fixture
def sample_user():
    return User(user_id=1, blockchain_public_key="abcd")


@pytest.fixture
def sample_link():
    return {
        "website": "Chess.com",
        "username": "Hans"
    }


def test_signup(db, client, sample_account):
    rv = client.post("/api/account/signup", json={})

    assert rv.status_code == 400, "Should not accept bad request"

    rv = client.post("/api/account/signup", json=sample_account)

    assert rv.status_code == 200, "Should be able to signup"

    rv = client.post("/api/account/signup", json=sample_account)

    assert rv.status_code == 401, "Should not accept duplicate entries"


def test_activate(db, client, account_unverified):
    db.session.add(account_unverified)
    db.session.commit()

    rv = client.post("/api/account/activate/" + "bad_token")

    assert rv.status_code == 400, "Should not accept a bad token"

    token = account_unverified.get_activate_token()

    rv = client.post("/api/account/activate/" + token)

    assert rv.status_code == 200, "Should be able to activate"


def test_activate_duplicate(db, client, account_verified, sample_user):
    db.session.add(sample_user)
    db.session.add(account_verified)
    db.session.commit()

    token = account_verified.get_activate_token()

    rv = client.post("/api/account/activate/" + token)

    assert rv.status_code == 400, "Should not allow double account activation"


def test_send_reset_password(db, client, account_unverified):
    db.session.add(account_unverified)
    db.session.commit()

    rv = client.post("/api/account/reset/password", json={})

    assert rv.status_code == 400, "Should not accept bad request"

    sample_email = {"email": account_unverified.email}
    rv = client.post("/api/account/reset/password", json=sample_email)

    assert rv.status_code == 200, "Should be able to send reset mail"


def test_reset_password(db, client, account_unverified):
    db.session.add(account_unverified)
    db.session.commit()

    rv = client.get("/api/account/reset/password/a", json={})

    assert rv.status_code == 400, "Should not accept bad request"

    token = "bad_token"
    data = {"password": "abcd", "token": token}

    rv = client.get("/api/account/reset/password/" + token, json=data)

    assert rv.status_code == 401, "Should not accept a bad token"

    token = account_unverified.get_reset_token()
    data = {"password": "abcd", "token": token}

    rv = client.get("/api/account/reset/password/" + token, json=data)

    assert rv.status_code == 200, "Should be able to reset password"



def test_modify(db, client, account_verified, sample_user):
    db.session.add(sample_user)
    db.session.add(account_verified)
    db.session.commit()

    data = {"email": account_verified.email, "password": account_verified.password}
    # Hash the password
    account_verified.set_password(account_verified.password)

    rv = client.post("/api/login", json=data)

    rv = client.post("/api/account/modify", json={})

    assert rv.status_code == 400, "Should not accept bad request"

    rv = client.post("/api/account/modify", json={"password": "sample123"})

    assert rv.status_code == 200, "Should be able to modify"



def test_retrieve_current_account_data(db, client, account_verified, sample_user):
    db.session.add(sample_user)
    db.session.add(account_verified)
    db.session.commit()

    rv = client.get("/api/account/retrieve/current")

    assert rv.status_code == 200


def test_retrieve_current_account_all_data(db, client, account_verified, sample_user):
    db.session.add(sample_user)
    db.session.add(account_verified)
    db.session.commit()

    rv = client.get("/api/account/retrieve/current/all")

    assert rv.status_code == 200


def test_link(db, client, account_verified, sample_user, sample_link):
    db.session.add(sample_user)
    db.session.add(account_verified)
    db.session.commit()

    rv = client.post("/api/account/link", json={})

    assert rv.status_code == 400, "Should not accept bad request"

    rv = client.post("/api/account/link", json=sample_link)

    assert rv.status_code == 200, "Should be able to link"

    rv = client.post("/api/account/link", json=sample_link)

    assert rv.status_code == 400, "Should not accept the duplicate link"


def test_unlink(db, client, account_verified, sample_user, sample_link):
    db.session.add(sample_user)
    db.session.add(account_verified)
    db.session.commit()

    rv = client.post("/api/account/unlink", json=sample_link)

    assert rv.status_code == 400, "Link should exist"

    rv = client.post("/api/account/link", json=sample_link)

    rv = client.post("/api/account/unlink", json={})

    assert rv.status_code == 400, "Should not accept bad request"

    rv = client.post("/api/account/unlink", json=sample_link)

    assert rv.status_code == 200, "Should be able to unlink"
