import pytest


def test_leaderboard(client):
    rv = client.get("/api/matches/leaderboard")

    assert rv.status_code == 200, "Should be able to retrieve leaderboard"


def test_match_history(client):
    rv = client.get("/api/matches/history")

    assert rv.status_code == 200, "Should be able to retrieve match history"
