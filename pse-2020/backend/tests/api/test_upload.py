from app.models import Account, User, Websites
from flask_login import login_user
import pytest
from io import BytesIO


@pytest.fixture
def account_verified():
    return Account(account_id=1, username="test", email="a@a.a",
        password="test123", account_activated=True, user_id=1)


@pytest.fixture
def account_verified2():
    return Account(account_id=2, username="test2", email="a2@a.a",
        password="test123", account_activated=True, user_id=2)


@pytest.fixture
def sample_user():
    return User(user_id=1,
        blockchain_public_key="""-----BEGIN PUBLIC KEY-----
MIGeMA0GCSqGSIb3DQEBAQUAA4GMADCBiAKBgFNxIu7KxO+Us8id5c8N8d/g2+M2
VhzILBpsPIywCOQuttmTBNIM0ddGm2x0zny91r/ZntP12rOaCErB+TZO89/HJRJa
82J8lIXFnM8kBigklCuHHqRDA57VWfTeH0fjW855NracEafwXDPJH2QF+lPm1kVk
1+h1tq8JxNSqDvTfAgMBAAE=
-----END PUBLIC KEY-----""")


@pytest.fixture
def sample_user2():
    return User(user_id=2,
        blockchain_public_key="""-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCi5JxuVLA14Fvn5kTDZsVx1Dwi
nC0FQiLqL6mHMWAkyqr0o7612tzpkdK/XxpaTBcQk1/aJtCFH4YfWk0O3HfrGf/d
2vDVwyvBBwUcTP+lti4Vdp48ICcGUTg4TqMdiP/rfmnZp8XosNXxMfo9PYFI9ZXw
V308gfvpoBKBaZA0YQIDAQAB
-----END PUBLIC KEY-----""")


@pytest.fixture
def sample_key():
    return b"""-----BEGIN RSA PRIVATE KEY-----
MIICWgIBAAKBgFNxIu7KxO+Us8id5c8N8d/g2+M2VhzILBpsPIywCOQuttmTBNIM
0ddGm2x0zny91r/ZntP12rOaCErB+TZO89/HJRJa82J8lIXFnM8kBigklCuHHqRD
A57VWfTeH0fjW855NracEafwXDPJH2QF+lPm1kVk1+h1tq8JxNSqDvTfAgMBAAEC
gYAHBEGCwQxUdTW1DgoQIkUPpXH4Jk6Rdwe2ZRmmXvswbPk1TJagLzHKH4bcVOo0
NSaE1YD0o/zzE2XVG4CgX31EYHr4lYGxlwOPXSyxnmPmj3cvsBEZKQoqaaOD50Xq
hTP+RIXV62Ssh7w6ir4GLI22+6JI/TrDLryF7MaoElWnUQJBAJeCfMYTOlAZXsHm
ssQu+LOfN6t3BKLRk8HbSmPMuCQPIxcYhoJ2YfMdNIixJCvoBS5BzSB1r8+ynrhj
gQVdmScCQQCM/RIlJPLcHHbs2qHUIjVewlvOnI6qBz1M6dptOzSaETA2rhPMvxeE
hvBvRmWjK3W5lucgHaNfj594gz6CAWmJAkA7ixC2V4AnUpEm6x9+cmdTUFN0k4XC
51KGcUQ/DImhwkEsJntWcmf0P5oRzmxExR6kG0AebaH2omA2XwSpsyspAkBxyaoE
Wn67kSmWXZnwWK+US+7GoOovZepekfqc4ATDN44fA32taKi1aCbS2BEdkOzRnW3q
KKF7dbHyFJK1YChpAkAnrFirXPHxthdyeRmMllgY+BK8YOFF/fk1BiDpfvnYCEyU
E+tLQVrxf+iYAkvvxP06a+t6E8lP3/FlnnNB+RKj
-----END RSA PRIVATE KEY-----"""


@pytest.fixture
def sample_pgn():
    return b"""[Event "Live Chess"]
[Site "Chess.com"]
[Date "2020.06.09"]
[White "player1"]
[Black "player2"]
[Result "0-1"]

1. e4 e5"""


def test_catch_upload(db, client, account_verified, account_verified2,
                      sample_user, sample_user2, sample_key, sample_pgn):
    db.session.add(sample_user)
    db.session.add(sample_user2)
    db.session.add(account_verified)
    db.session.add(account_verified2)
    db.session.commit()

    data = {"email": account_verified.email, "password": account_verified.password}
    # Hash the password
    account_verified.set_password(account_verified.password)
    # Login requirement
    rv = client.post("/api/login", json=data)

    data = {"file": (BytesIO(sample_pgn), "game.pgn"),
            "private_key": (BytesIO(sample_key), "private.key")}
    rv = client.post("/api/upload", data=data)

    assert rv.status_code == 400, "Links in PGN data should be created"

    db.session.add(Websites(user_id=sample_user.user_id, website_name="Chess.com", player_name="player1"))
    db.session.add(Websites(user_id=sample_user2.user_id, website_name="Chess.com", player_name="player2"))
    db.session.commit()

    rv = client.post("/api/upload", data={})

    assert rv.status_code == 400, "Should not accept bad request"

    data = {"file": (BytesIO(sample_pgn), "game.pgn"),
            "private_key": (BytesIO(b"bad_key"), "private.key")}
    rv = client.post("/api/upload", data=data)

    assert rv.status_code == 401, "Private key should match public key"

    data = {"file": (BytesIO(sample_pgn), "game.pgn"),
            "private_key": (BytesIO(sample_key), "private.key")}
    rv = client.post("/api/upload", data=data)

    assert rv.status_code == 200, "Untested error"
