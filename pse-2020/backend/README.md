# Chesschain - Backend

The 'backend' directory of the project's codebase consists of the various requirements
and functionality that is needed to implement the back end of Chesschain.

The back end contains the following components:
- app: implements all functionality for the web application.
- docker: contains the dockerfile and dockerignore that is used to build images
- migrations: contains migration files that describe database configurations and
              files to generate and handle these files
- tests: contains testing scripts for back end functionality