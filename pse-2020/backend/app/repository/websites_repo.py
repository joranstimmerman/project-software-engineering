#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
The `website repository` contains functions for operations on the `website`
database table.
"""

from app.models import Websites


def get_website_names_by_user_id(user_id):
    """
    Function to retrieve website data that matches the given user ID.

    :param user_id:     user ID
    :type user_id:      int
    :return:            website data
    :rtype:             website object
    """
    return Websites.query.filter_by(user_id=user_id).all()


def get_websites_by_player_website(player_name, website_name):
    """
    Function to retrieve website data that matches the given player name and
    website.

    :param player_name:     third party website username
    :type player_name:      string
    :param website_name:    third party website
    :type website_name:     string
    :return:            website data
    :rtype:             website object
    """
    return Websites.query.filter_by(player_name=player_name,
                                    website_name= website_name).one_or_none()