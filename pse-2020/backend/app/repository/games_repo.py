#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
The `games repository` contains functions for operations on the `games`
database table.
"""

from app.models import Games


def get_pgn_by_game_id(game_id):
    """
    Function to retrieve data of a game that matches the given game ID.

    :param game_id:     game ID
    :type game_id:      int
    :return:            game data
    :rtype:             games object
    """
    return Games.query.filter_by(game_id=game_id).one_or_none()


def get_game_by_pgn_data(pgn_data):
    """
    Function to retrieve data of a game that matches the given PGN data.

    :param pgn_data:    PGN data
    :type pgn_data:     dictionary
    :return:            game data
    :rtype:             games object
    """
    return Games.query.filter_by(pgn_data=pgn_data).one_or_none()
