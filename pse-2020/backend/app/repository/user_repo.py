#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
The `user repository` contains functions for operations on the `user`
database table.
"""

from app.models import User
from app.repository import websites_repo


def get_user_by_id(user_id):
    """
    Function to retrieve user data that matches the given user ID.

    :param user_id:     user ID
    :type user_id:      int
    :return:            user data
    :rtype:             user object
    """
    return User.query.filter_by(user_id=user_id).one_or_none()


def get_leaderboard():
    return User.query.order_by(User.elo_rating.desc()).limit(10)


def get_user_by_player_website(player_name, website_name):
    """
    Function to retrieve user data that matches the given third party username
    and website name.

    :param player_name:     third party website username
    :type player_name:      string
    :param website_name:    third party website
    :type website_name:     string
    :return:                user data
    :rtype:                 user object
    """
    website = websites_repo.get_websites_by_player_website(player_name,
                                                           website_name)
    return get_user_by_id(website.user_id)


def get_user_by_public_key(public_key):
    """
    Function to retrieve user data that matches the given public key.

    :param public_key:      unique user public key
    :type public_key:       string
    :return:                user data
    :rtype:                 user object
    """
    return User.query.filter_by(blockchain_public_key=public_key).one_or_none()

