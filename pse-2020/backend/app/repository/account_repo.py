#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
The `account repository` contains functions for operations on the `account`
database table.
"""

from app.models import Account


def get_account_by_username(username):
    """
    Function to retrieve account data that matches a given username.

    :param username:    username
    :type username:     string
    :return:            account data
    :rtype:             account object
    """
    return Account.query.filter_by(username=username).one_or_none()


def get_account_by_email(email):
    """
    Function to retrieve account data that matches a given email address.

    :param email:       email address
    :type email:        string
    :return:            account data
    :rtype:             account object
    """
    return Account.query.filter_by(email=email).one_or_none()


def get_account_by_id(account_id):
    """
    Function to retrieve account data that matches a given account ID.

    :param account_id:  account ID
    :type account_id:   int
    :return:            account data
    :rtype:             account object
    """
    return Account.query.filter_by(account_id=account_id).one_or_none()


def get_account_by_user_id(user_id):
    """
    Function to retrieve account data that matches a given user ID.

    :param user_id:     user ID
    :type user_id:      int
    :return:            account data
    :rtype:             account object
    """
    return Account.query.filter_by(user_id=user_id).one_or_none()