#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
The `association repository` contains functions for operations on the
`association` database table.
"""

from app.models import Association


def get_unsigned_games_by_id(user_id):
    """
    Function to retrieve unsigned games of a user with a given user ID.

    :param user_id:     user ID
    :type user_id:      int
    :return:            unsigned games
    :rtype:             association object
    """
    return Association.query.filter_by(user_id=user_id, signed=False) \
        .order_by(Association.game_id.desc()).all()


def get_signed_games_by_id(user_id):
    """
    Function to retrieve signed games of a user with a given user ID.

    :param user_id:     user ID
    :type user_id:      int
    :return:            signed games
    :rtype:             association object
    """
    return Association.query.filter_by(user_id=user_id, signed=True) \
        .order_by(Association.game_id.desc()).all()


def get_all_games_by_id(user_id):
    """
    Function to get all games of a specific user
    
    :param user_id:     user ID
    :type user_id:      int
    :return:            all games of a user
    :rtype:             association object
    """
    return Association.query.filter_by(user_id=user_id).order_by(Association.game_id.desc()).all()


def get_games_by_id(game_id):
    """
    Function to retreive all games that match a given game ID.

    :param game_id:     game ID
    :type game_id:      int
    :return:            games
    :rtype:             association object
    """
    return Association.query.filter_by(game_id=game_id).all()


def get_game_by_ids(game_id, user_id):
    """
    Function to retreive a game that matches a given game ID and user ID.

    :param game_id:     game ID
    :type game_id:      int
    :param user_id:     user ID
    :type user_id:      int
    :return:            game data
    :rtype:             association object
    """
    return Association.query.filter_by(game_id=game_id, user_id=user_id)\
        .one_or_none()

def get_signed_game_by_game_id(game_id):
    """
    Function to retreive a signed game that matches a given game ID.

    :param game_id:     game ID
    :type game_id:      int
    :return:            game data
    :rtype:             association object
    """
    return Association.query.filter_by(game_id=game_id, signed=True)\
        .one_or_none()
