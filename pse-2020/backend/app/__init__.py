from flask import Flask
from flask_login import LoginManager
import os

app = Flask(__name__)

app.config['CORS_HEADERS'] = 'Content-Type'
app.secret_key = "RARA WAT BEN IK"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

login = LoginManager(app)
login.session_protection = 'strong'

from . import models
from . import api
from . import repository
from . import functionalities

@app.route("/", methods=['POST'])
def test():
    return "done", 200