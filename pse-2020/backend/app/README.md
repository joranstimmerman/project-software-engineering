# Chesschain - Backend - App

The 'app' module in ChessChain's back end implements all functionality for our
web application.

The app module contains the following components:
- api: API's that handle calls and requests that are received from our front end.
- functionalities: Contains scripts with functions for various purposes that extend simple database operations.
- models: Database Models that define our back-end database structure
- pgn_to_json: Contains the original script to convert PGN files to JSON, and some testing files
- repository: Repositories for various operations on the different database tables
- templates: Mailing templates