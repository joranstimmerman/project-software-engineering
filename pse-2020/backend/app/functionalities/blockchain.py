#!/usr/bin/env python3

# -*- coding: utf-8 -*-

"""
The `blockchain` functionalities file contains functions that are needed to
exchange data between the blockchain and our web application's back end.
"""

__author__ = [""]
__credits__ = [""]
__version__ = "3.0"
__maintainer__ = "Back-end team"
__status__ = "In progress"
__docformat__ = "reStructuredText"


from app.models import BlockchainConnection
from app.functionalities import elo
from Crypto.PublicKey import RSA
import json
from datetime import date


def get_keys(keysize=1024):
    """
    Generates a random private and public key.

    :param keysize:     size of the keys to be generated
    :type keysize:      int
    :return:            private/public key pair
    :rtype:             RSA key pair
    """
    key = RSA.generate(keysize)
    public_key = key.publickey()
    return key.exportKey().decode(), public_key.exportKey().decode()


def verify_keys(private_key, public_key):
    """
    Verifies a private and public key pair.

    :param private_key:     private key
    :type private_key:      RSA key
    :param public_key:      public key
    :type public_key:       RSA key
    :return:                `True` if public key matches private key,
                            `False` otherwise
    :rtype:                 boolean
    """
    try:
        return RSA.importKey(private_key.encode()) \
               .publickey().exportKey().decode() == public_key
    except:
        return False



def find_games(parameters):
    """
    Retreives games in the blockchain that match the given parameters.

    :param parameters:      parameters that can identify a game
    :type parameters:       dictionary
    :return:                Game data
    :rtype:                 JSON response
    """
    connection = BlockchainConnection()
    command = "find_games"
    response = connection.send_blockchain_request(command, parameters)
    connection.disconnect()
    return json.loads(response)


def add_game(game_data):
    """
    Adds a game to the blockchain.

    :param game_data:       game data that will be added in the block
    :type game_data:        JSON file
    :return:                response
    :rtype:                 JSON response
    """
    connection = BlockchainConnection()
    command = "add_block"
    response = connection.send_blockchain_request(command, game_data)
    connection.disconnect()
    return response


def add_game_admin(game_data):
    """
    Adds a game to the blockchain as an admin.

    :param game_data:       game data that will be added in the block
    :type game_data:        JSON file
    :return:                response
    :rtype:                 JSON response
    """
    connection = BlockchainConnection()
    command = "admin_add_block"
    response = connection.send_blockchain_request(command, game_data)
    connection.disconnect()
    return response


def get_all_recent_games():
    """
    Retreives all games that are in the blockchain.

    :return:                game data
    :rtype:                 JSON response
    """
    connection = BlockchainConnection()
    command = "get_all_recent_games"
    response = connection.send_blockchain_request(command)
    connection.disconnect()
    return json.loads(response)


def get_block_by_hash(block_hash):
    """
    Retreives a block in the blockchain that corresponds to the given hash.

    :param block_hash:       hash code of a block
    :type block_hash:        string
    :return:                 block
    :rtype:                  JSON response
    """
    connection = BlockchainConnection()
    command = "get_block"
    response = connection.send_blockchain_request(command, block_hash)
    connection.disconnect()
    return json.loads(response)


def get_block_by_index(index):
    """
    Retreives a block in the blockchain that corresponds to the given index.

    :param index:           index of a block
    :type index:            int
    :return:                block
    :rtype:                 JSON response
    """
    connection = BlockchainConnection()
    command = "get_block"
    response = connection.send_blockchain_request(command, index)
    connection.disconnect()
    return json.loads(response)


def get_last_block():
    """
    Retreives the last block in the blockchain.

    :return:                block
    :rtype:                 JSON response
    """
    connection = BlockchainConnection()
    command = "fetch_last_block"
    response = connection.send_blockchain_request(command)
    connection.disconnect()
    return json.loads(response)


def find_blocks(parameters):
    """
    Retreives blocks in the blockchain that corresponds to the given parameters.

    :return:                blocks
    :rtype:                 JSON response
    """
    connection = BlockchainConnection()
    command = "find_blocks"
    response = connection.send_blockchain_request(command, parameters)
    connection.disconnect()
    return json.loads(response)


def save_chain():
    """
    Saves the current blockchain.

    :return:                response
    :rtype:                 JSON response
    """
    connection = BlockchainConnection()
    command = "save_chain"
    response = connection.send_blockchain_request(command)
    connection.disconnect()
    return response


def check_chain():
    """
    Checks if the current blockchain is valid (hashes in blocks should point to
    each other correctly).

    :return:                response
    :rtype:                 JSON response
    """
    connection = BlockchainConnection()
    command = "check_chain"
    response = connection.send_blockchain_request(command)
    connection.disconnect()
    return json.loads(response)


def get_amount_games(public_key, contestant):
    """
    Retrieves the amount of games a player in the blockchain has played.

    :param public_key:          user's public key, used to identify a block
    :type public_key:           RSA key
    :param contestant:          username, used to identify a block
    :type contestant:           string

    :return:                    amount of games
    :rtype:                     int
    """
    return len(find_games(contestant + " " + public_key)["games"])


def get_elo(public_key, contestant):
    """
    Retrieves the elo of a player in the blockchain.

    :param public_key:          user's public key, used to identify a block
    :type public_key:           RSA key
    :param contestant:          username, used to identify a block
    :type contestant:           string

    :return:                    ELO rating
    :rtype:                     int
    """
    game = find_games(contestant + " " + public_key)["games"][-1]

    if game["contestant_1_public_key"] == public_key:
        return game["elo_score_contestant_1"]

    return game["elo_score_contestant_2"]


def extract_blockchain_data(game_data):
    """
    Checks wether the users that played the game are present in the blockchain.
    if not, act accordingly.

    Scenario 1: Both players are present in the blockchain.
    In this case, use the most recent elo that is know of a user in the
    blockchain.
    also, retrieve the amount of games a player has played.

    Scenario 2: Only one player is present in the blockchain
    In this case, Retrieve the data for this player from the blockchain.
    Use the PGN data for the player that is not present in the blockchain.

    Scenario 3: No players are present in the blockchain. This means that
    they're new, so set the elo standard to 600.

    :param game_data: A dictionary filled with information about that game.
    :type game_data: Dictionary

    :return: An updated version of the information about a game.
    :rtype: Dictionary
    """

    game_data["number_of_games_contestant_1"] = \
        get_amount_games(game_data["contestant_1_public_key"],
                         "contestant_1_public_key")
    game_data["number_of_games_contestant_2"] = \
        get_amount_games(game_data["contestant_2_public_key"],
                         "contestant_2_public_key")

    if game_data["number_of_games_contestant_1"] == 0:
        game_data["elo_score_contestant_1"] = 600
    else:
        game_data["elo_score_contestant_1"] = \
            get_elo(game_data["contestant_1_public_key"],
                    "contestant_1_public_key")

    if game_data["number_of_games_contestant_2"] == 0:
        game_data["elo_score_contestant_2"] = 600
    else:
        game_data["elo_score_contestant_2"] = \
            get_elo(game_data["contestant_2_public_key"],
                    "contestant_2_public_key")

    elo1, elo2 = elo.calculate_elo(game_data)
    game_data["elo_score_contestant_1"] = elo1
    game_data["elo_score_contestant_2"] = elo2

    # today = date.today()
    # game_data["date"] = today.strftime("%Y.%m.%d")

    return game_data
