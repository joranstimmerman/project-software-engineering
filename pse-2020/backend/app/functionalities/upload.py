from app import app
import json
import re
import sys
import chess.pgn

# data that could be relevant for a player to have insight to
relevant_keys = [
        "Event",
        "Site",
        "Date",
        "White",
        "Black",
        "Result",
        "WhiteElo",
        "BlackElo",
        "TimeControl",
]

# data that is necessary to store in the blockchain
required_keys = [
    "result",
    "elo_score_contestant_1",
    "elo_score_contestant_2",
    "move_list",
    "date",
    "username_1",
    "username_2",
    "contestant_1_public_key",
    "contestant_2_public_key",
]

game_data = {
    "event": "",                            # Type of chess game
    "site": "",                             # Third party
    "date": "",                             # Date game finished
    "username_1": "",                       # third party username
    "username_2": "",                       # third party username
    "result": "",                           # Winner (left white, right black)
    "elo_score_contestant_1": "",           # Elo 1 (white player)
    "elo_score_contestant_2": "",           # Elo 2 (black player)
    "time_control": "",                     # TimeControl
    "move_list": [],                        # All moves leading to victory
    "contestant_1_public_key": None,        # Public key
    "contestant_2_public_key": None,        # Public key
    "number_of_games_contestant_1": "",     # games played player 1
    "number_of_games_contestant_2": "",     # games played player 2
}


def check_results(board, result, last_played):
    """
    Check the validity of the result in data against the moves in data.

    :param board:           chessboard with all played moves registered on it
    :type board:            Chess class
    :param result:          result field from PGN data
    :type result:           string
    :param last_played:     player who played the last turn (white / black)
    :type last_played:      string

    :return:                `True` if game results are valid `False` otherwise
    :rtype:                 boolean

    .. todo:: add functionality to check for a surrendered match,
     as of now the function only checks for valid checkmates and draws
    """
    if board.is_checkmate() and (
        (last_played == "black" and result == "1-0")
        or (last_played == "white" and result == "0-1")
    ):
        return False
    elif (
        board.is_stalemate() or board.is_insufficient_material()
    ) and result != "1/2-1/2":
        return False
    else:
        return True


def parse_movelist(node, game_data):
    """
    Parse all moves that are given by a PGN file and put them into a list using
    regex. Also enter the moves onto a board so that the entered result can
    be compared to the end state of the board for legitimacy.

    :param node:        representation of the PGN file
    :type node:         Chess class
    :param game_data:   dictionary of all relevant data of the PGN files
    :type game_data:    dictionary

    :return:            dictionary filled with the moves from a particulair
                        chess game.
    :rtype:             dictionary

    """
    if game_data["move_list"] == []:
        board = chess.Board()
        turn = ["white", "black"]
        surrender = False
        i = 0


        while node.variations:
            next_node = node.variation(0)
            move = re.sub("\{.*?\}", "", node.board().san(next_node.move))

            # enter move onto board
            board.push_san(move)
            i = (i + 1) % 2

            game_data["move_list"].append(move)
            node = next_node

        if not check_results(board, game_data["result"], turn[(i + 1) % 2]):
            return None
    print(f"move {game_data}", flush=True)
    
    return game_data

def strip_site(site_name):
    """
    Get a valid website name from the PGN data without special characters,
    tags or url parts.

    :param site_name:        website found in PGN data
    :type site_name:         string
    """
    site_name = site_name.strip('https://')
    site_name = site_name.strip('http://')
    site_name = site_name.split("/")[0]
    site_name = re.sub('[:/!@#$]', '', site_name)

    return site_name

def convert_pgn_to_dict(game_data, relevant_keys, pgn_file):
    """
    Convert all relevant information from a PGN file to a dictionary

    :param game_data:       dictionary to be filled with data from PGN file
    :type game_data:        dictionary
    :param relevant_keys:   relevant game data keys
    :type relevant_keys:    list
    :param pgn_file:        pgn file with game data
    :type pgn_file:         pgn file

    :return:                dictionary with relevant data from PGN file
    :rtype:                 dictionary
    """

    game = chess.pgn.read_game(pgn_file)
    if game is None:
        return None

    data = game.headers

    # Use all relevant information from the PGN file
    # to create our custom JSON File.
    for dict_key, pgn_key in zip(game_data, relevant_keys):
        print(f"keys: {relevant_keys}", flush=True)
        if pgn_key in data:
            game_data[dict_key] = data[pgn_key]

    # Strip the site.
    game_data['site'] = strip_site(game_data['site'])

    return parse_movelist(game, game_data)


def check_required_keys(game_data):
    """
    Check if game data contains all required keys to enter match in blockchain.

    :param game_data:       game data
    :type game_data:        dictionary

    :return:                missing key in game data if there is any, otherwise
                            `None`
    :rtype:                 string
    """
    for key in required_keys:
        if not game_data[key]:
            return key
    return None


def dict_to_json(game_data):
    """
    Convert dictionary to JSON file

    :param game_data:       game data dictionary
    :type game_data:        dictionary

    :return:                game data JSON file
    :rtype:                 JSON file
    """
    return json.dumps(game_data, indent=2)
