#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
The `elo` functionalities file contains functions that are needed to
calculate the ELO score a player will gain or lose based on their game results
"""

__author__ = [""]
__credits__ = [""]
__version__ = "3.0"
__maintainer__ = "Back-end team"
__status__ = "In progress"
__docformat__ = "reStructuredText"


def calculate_elo(game_data):
    """
    Calculates a new elo for two players based on the results of that game.

    :param game_data:   information about the played game
    :type game_data:    dictionary

    :return:            new ELO rating for both players
    :rtype:             tuple
    """
    N0 = int(game_data["number_of_games_contestant_1"])
    N1 = int(game_data["number_of_games_contestant_2"])
    R0 = int(game_data["elo_score_contestant_1"])
    R1 = int(game_data["elo_score_contestant_2"])
    result_p1, result_p2 = parse_result(game_data["result"])

    P0 = probability(R0, R1)
    P1 = probability(R1, R0)

    return (
        int(R0 + K_factor(N0, R0) * (result_p1 - P0)),
        int(R1 + K_factor(N1, R1) * (result_p2 - P1)),
    )


def K_factor(N, R0):
    """
    Calculates the K-factor, which is used to determine the elo of a player.

    :param N:   total number of games played by the player
    :type N:    int
    :param R0:  current ELO rating of the player
    :type R0:   int

    :return:    K-factor
    :rtype:     int
    """
    # Before a player played 30 games the elo changes more to get to a more
    # representative starting value.
    if R0 < 2100:
        if N < 10:
            return 100
        elif N < 20:
            return 60
        elif N < 30:
            return 45
        return 32

    elif R0 >= 2100 and R0 < 2400:
        return 24

    else:
        return 16


def parse_result(result):
    """
    Parses the result that is given as a string in game data, to a float

    :param result:  match result from PGN data
    :type result:   string

    :return:        0 if white player has won, 1 if black player has won,
                    0.5 if stalemate.
    :rtype:         float
    """
    if result == "1-0":
        return 1.0, 0.0
    elif result == "0-1":
        return 0.0, 1.0
    else:
        return 0.5, 0.5


def probability(R0, R1):
    """
    Calculates the probability of player with R0 winning against player with
    R1.

    :param R0:  ELO rating of the player.
    :type R0:   int
    :param R1:  ELO rating of the opponent.
    :type R1:   int

    :return:    probability
    :rtype:     int
    """
    return 1 / (1 + pow(10, (R1 - R0) / 400))
