#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
The `statistics` functionalities file contains functionality to retreive
some match statistics of a user.
"""

from datetime import datetime
import operator


def statistics(games, account, user, websites):
    """
    Returns some match data of a user to be shown as statistics. Specifically
    their wins, losses, their ELO rating and average elo of opponents.

    :param games:       user's played games
    :type games:        list
    :param account:     user's account data
    :type account:      dictionary
    :param user:        user's blockchain identity data
    :type user:         dictionary
    :param websites:    user's third party account(s) data
    :type websites:     dictionary

    :return:            user's wins, losses, ELO rating, average elo of opponent on win,
                        average elo of opponent on loss, average elo of opponent on draw,
    :rtype:             list
    """
    wins = 0
    loses = 0
    draws = 0
    elo_rate = []
    player_names = [website_name.player_name for website_name in websites]
    avg_elo_won = 0
    avg_elo_lost = 0
    avg_elo_draw = 0
    number_games = len(games["games"])

    for game in games["games"]:
        username_1 = game["username_1"]
        username_2 = game["username_2"]
        result = game["result"]
        parsed_date = datetime.strptime(game["date"], "%Y.%m.%d")

        if username_1 in player_names:
            if result == "1-0":
                wins += 1
                avg_elo_won += game["elo_score_contestant_2"]
            elif result == "0-1":
                loses += 1
                avg_elo_lost += game["elo_score_contestant_2"]
            else:
                draws += 1
                avg_elo_draw += game["elo_score_contestant_2"]

            elo_rate.append({"date": game["date"],
                             "elo": game["elo_score_contestant_1"]})

        if username_2 in player_names:
            if result == "0-1":
                wins += 1
                avg_elo_won += game["elo_score_contestant_2"]
            elif result == "1-0":
                loses += 1
                avg_elo_lost += game["elo_score_contestant_2"]
            else:
                draws += 1
                avg_elo_draw += game["elo_score_contestant_2"]

            elo_rate.append({"date": game["date"],
                             "elo": game["elo_score_contestant_2"]})

    elo_rate.sort(key=operator.itemgetter("date"))


    avg_elo_won /= 1 if wins == 0 else int(wins)
    avg_elo_lost /= 1 if loses == 0 else int(loses)
    avg_elo_draw /= 1 if draws == 0 else int(draws)

    return wins, loses, draws, elo_rate, [int(avg_elo_won), int(avg_elo_lost), int(avg_elo_draw)]
