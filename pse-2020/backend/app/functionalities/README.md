# Chesschain - Backend - App - Functionalties

The 'functionalities' directory contains various scripts with functions that
are either not connected to front end and therefore not in an API, or desired
to be kept apart because of the extensiveness of the funtions.

It contains the following components:
- blockchain: functions to communicate with the blockchain and exchange data
- elo: calculation and parsing of ELO score
- mail: functions for sending emails
- statistics: gathering of statistics
- upload: functions for the processing of uploaded PGN data