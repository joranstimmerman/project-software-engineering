#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
The `mail` functionalities file contains functions that can be used to send out
emails to users via our back end.
"""

from app.models import mail
from app.repository import account_repo
from flask_mail import Message
from flask import render_template
from datetime import date
from app import app
import os


def send_signup_mail(account, activate_url, sender=os.environ["EMAIL_USERNAME"],
                     cc=None, bcc=None, attachments=None):
    """
    Sends an email to a user that has signed up to our web application.
    This email also contains a link to activate the account.

    :param account:         user's account data including their email address
    :type account:          dictionary
    :param activate_url:    url that can be used to verify account
    :type activate_url:     string
    :param sender:          sender email address
    :type sender:           string
    :param cc:              email addres to be cc'd
    :type cc:               string
    :param bcc:             email address to be bcc'd
    :type bcc:              string
    :param attachments:     attachments
    :type attachments:      list
    """
    subject = "Welcome to ChessChain"
    msg = Message(subject, sender=sender, recipients=[account.email])
    msg.attach('logo.png','image/png',
               open("/project/app/templates/template_files/logo.png",
               'rb').read(), 'inline', headers=[['Content-ID','<Myimage>'],])
    msg.html = render_template("account_created.html", account=account, \
                               date=date.today(), activate_url=activate_url)

    if attachments:
        msg.attach(attachments)

    if cc:
        msg.cc = cc

    if bcc:
        msg.bcc = bcc

    mail.send(msg)


def send_reset_mail(account, reset_url, sender=os.environ["EMAIL_USERNAME"],
                    cc=None, bcc=None, attachments=None):
    """
    Sends an email to a user that has requested a reset of his/her password.

    :param account:         user's account data including their email address
    :type account:          dictionary
    :param reset_url:       url that can be used to reset password
    :type reset_url:        string
    :param sender:          sender email address
    :type sender:           string
    :param cc:              email addres to be cc'd
    :type cc:               string
    :param bcc:             email address to be bcc'd
    :type bcc:              string
    :param attachments:     attachments
    :type attachments:      list
    """
    subject = "Reset Your Password"
    msg = Message(subject , sender=sender, recipients=[account.email])
    msg.attach('logo.png','image/png',
               open("/project/app/templates/template_files/logo.png",
               'rb').read(), 'inline', headers=[['Content-ID','<Myimage>'],])
    msg.html = render_template("reset_password.html", account=account, \
                               date=date.today(), reset_url=reset_url)

    if attachments:
        msg.attach(attachments)

    if cc:
        msg.cc = cc

    if bcc:
        msg.bcc = bcc

    mail.send(msg)


def send_private_key_mail(account, private_key,
                          sender=os.environ["EMAIL_USERNAME"]):
    subject = "Private Key"
    msg = Message(subject , sender=sender, recipients=[account.email])
    msg.attach('logo.png','image/png',
               open("/project/app/templates/template_files/logo.png",
               'rb').read(), 'inline', headers=[['Content-ID','<Myimage>'],])

    file = open("/project/app/templates/template_files/private_key.txt", "w")
    file.write(private_key)
    file.close()
    msg.attach("private_key.txt", "text/plain",
               open("/project/app/templates/template_files/private_key.txt",
               'rb').read())
    msg.html = render_template("private_key.html", account=account, \
                               date=date.today())
    mail.send(msg)
    os.remove("/project/app/templates/template_files/private_key.txt")