#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
The `games model` contains the games class that implements the corresponding
database table.
"""

from app.models import db


class Games(db.Model):
    """
    The `Games` class implements the database table `games`.
    """
    game_id = db.Column(db.Integer, primary_key=True)
    pgn_data = db.Column(db.String(21844), unique=True, nullable=False)
    users = db.relationship("Association",back_populates="game")

    def serialize_json(self):
        """
        Convert games data into JSON format.

        :param self:        games
        :type self:         games object

        :return:            account data
        :rtype:             JSON
        """
        return {
            "game_id": self.game_id,
            "pgn_data": self.pgn_data,
        }
