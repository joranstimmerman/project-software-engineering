#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
The `blockchain model` contains the `BlockchainConnection` class that
contains the socket connection to the blockchain and also some accompanying
functions.
"""

import socket
import json

class BlockchainConnection():
    """
    The `BlockchainConnection` class implements the socket connection from
    the back end to the blockchain and vice versa. It also contains a function
    for disconnecting and one for sending requests to the blockchain.
    """
    buff_size = 4096

    def __init__(self):
        """
        Create a blockchain connection.

        :param self:    blockchain connection
        :type token:    BlockchainConnection object
        """
        self.host = "blockchain"
        self.port = 12345
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.settimeout(3)
        self.socket.connect((self.host, self.port))
        recv = self.socket.recv(1024).decode()

    def disconnect(self):
        """
        Disconnect blockchain connection.

        :param self:    blockchain connection
        :type self:     BlockchainConnection object
        """
        self.socket.close()

    def send_blockchain_request(self, command, data=""):
        """
        Send a request to the blockchain

        :param self:        blockchain connection
        :type self:         BlockchainConnection object
        :param command:     command for the request
        :type command:      string
        :param data:        data to be sent with request
        :type data:         string

        :return:            data retreived from request
        :rtype:             string
        """
        request = command + " " + str(data)
        self.socket.send(request.encode())
        data = ""

        while True:
            received = self.socket.recv(self.buff_size).decode('utf-8')
            data += received

            if len(received) < self.buff_size:
                break

        return data
