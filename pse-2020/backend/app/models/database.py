#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
The `database model` implements our SQLAlchemy database.
"""

from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from app import app
import os

app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['DATABASE_URL']
db = SQLAlchemy(app)
migrate = Migrate(app, db)