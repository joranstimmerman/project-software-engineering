#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
The `mail model` implements the configuration that is needed to send emails from
our back end.
"""

from app import app
from flask_mail import Mail
import os


mail_config = {
    "MAIL_SERVER": 'smtp.gmail.com',
    "MAIL_PORT": 465,
    "MAIL_USE_TLS": False,
    "MAIL_USE_SSL": True,
    "MAIL_USERNAME": os.environ['EMAIL_USERNAME'],
    "MAIL_PASSWORD": os.environ['EMAIL_PASSWORD'],
    "MESSAGE_DEFAULT_SENDER": os.environ["EMAIL_USERNAME"]
}


app.config.update(mail_config)
mail = Mail(app)