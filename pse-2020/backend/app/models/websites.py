#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
The `websites model` contains the websites class that implements the
corresponding database table.
"""

from app.models import db


class Websites(db.Model):
    """
    The `Websites` class implements the database table `websites`.
    """
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.user_id'))
    website_name = db.Column(db.String(128),nullable=False)
    player_name = db.Column(db.String(128),nullable=False, unique= True)

    def serialize_json(self):
        """
        Convert website data into JSON format.

        :param self:        website data
        :type self:         websites object

        :return:            websites data
        :rtype:             JSON
        """
        return {
            "id": self.user_id,
            "user_id": self.user_id,
            "website_name": self.website_name,
            "player_name": self.player_name
        }

    def deserialize_json(self, json):
        """
        Set values from account data in JSON format, into account object.

        :param self:        website data
        :type self:         websites object
        :param json:        website data
        :type json:         JSON
        """
        self.id = json["id"]
        self.user_id = json["user_id"]
        self.website_name = json["user_id"]
        self.player_name = json["user_id"]
