#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
The `login model` implements the loading of an account after logging in.
"""

from app import login
from .account import Account


@login.user_loader
def load_account(account_id):
    """
    Retreive an account object that matches the given account ID.

    :param account_id:  account ID
    :type account_id:   int
    :return:            account
    :rtype:             account object
    """
    return Account.query.get(account_id)
