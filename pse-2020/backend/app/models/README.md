# Chesschain - Backend - App - Models

The 'models' directory contains models that describe the tables and structure of our web application's database,
and other classes/instances that are needed to implement our back end.

The following models have been implemented:
- account: database table 'accounts' and some accompanying functions
- association: database association table that connects the 'users' and 'games' tables to keep track of a blockchain user's (signed) games
- blockchain: the blockchain connection that is needed to communicate with the blockchain via the back end
- database: database set-up
- games: database table 'games' and some accompanying functions
- login: account loader
- mail: mail configuration
- user: (blockchain) user table and some accompanying functions
- websites: website table and some accompanying functions, keeps track of third party website accounts that have been linked to ChessChain accounts