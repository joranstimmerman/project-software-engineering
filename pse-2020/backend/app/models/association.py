#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
The `association model` contains the association class that implements the
corresponding database table.
"""

from app.models import db


class Association(db.Model):
    """
    The `Association` class implements the database table `association`.
    This table functions as a one-to-many connection between a user
    (see `user model`) and their games (see `games model`)
    """
    user_id = db.Column(db.Integer, db.ForeignKey("user.user_id"),
                        primary_key=True)
    game_id = db.Column(db.Integer, db.ForeignKey("games.game_id"),
                        primary_key=True)
    signed = db.Column(db.Boolean, nullable=False, default=False)
    user = db.relationship("User", back_populates="games")
    game = db.relationship("Games", back_populates="users")