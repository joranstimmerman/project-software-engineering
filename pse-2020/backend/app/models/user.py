#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
The `user model` contains the user class that implements the corresponding
database table.
"""

from app.models import db


class User(db.Model):
    """
    The `User` class implements the database table `user`.
    """
    user_id = db.Column(db.Integer, primary_key=True)
    blockchain_public_key = db.Column(db.String(), unique=True, nullable=False)
    accounts_user = db.relationship('Account', backref='user', uselist=False)
    websites_user = db.relationship('Websites', backref='user', lazy=True)
    games = db.relationship("Association", back_populates="user")
    elo_rating = db.Column(db.Integer, default=600, nullable=False)

    def serialize_json(self):
        """
        Convert user data into JSON format.

        :param self:        user
        :type self:         user object

        :return:            uesr data
        :rtype:             JSON
        """
        return {
            "user_id": self.user_id,
            "blockchain_public_key": self.blockchain_public_key,
            "elo_rating": self.elo_rating,
        }

    def deserialize_json(self, json):
        """
        Set values from user data in JSON format, into user object.

        :param self:        user
        :type self:         user object
        :param json:        user data
        :type json:         JSON
        """
        self.user_id = json["user_id"]
        self.blockchain_public_key = json["blockchain_public_key"]
        self.elo_rating = json["elo_rating"]
