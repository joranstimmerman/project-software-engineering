#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
The `account model` contains the account class that implements the corresponding
database table. It also contains some basic functions in regards to account
creation and verification.
"""

from werkzeug.security import generate_password_hash, check_password_hash
from .database import db
from flask_login import UserMixin
from app import app
from time import time
import jwt


class Account(UserMixin, db.Model):
    """
    The `Account` class implements the database table `account` and some
    accompanying functions.
    """
    account_id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(128), unique=True, nullable=False)
    password = db.Column(db.String(128), nullable=False)
    email = db.Column(db.String(128), unique=True, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.user_id'))
    account_activated = db.Column(db.Boolean, nullable=False, default=False)

    def set_password(self, password):
        """
        Set the password of an account.

        :param self:        account
        :type self:         account object
        :param password:    password to be set
        :type password:     string
        """
        self.password = generate_password_hash(password)

    def check_password(self, password):
        """
        Check if a given password matches the account.

        :param self:        account
        :type self:         account object
        :param password:    password to be checked
        :type password:     string

        :return:            `True` if password matches, `False` otherwise
        :rtype:             boolean
        """
        return check_password_hash(self.password, password)

    def get_reset_token(self, expires_in=1800):
        """
        Generate a token that can be used to reset an an account's password.

        :param self:        account
        :type self:         account object
        :param expires_in:  time it takes for the token to expire, in seconds
        :type expires_in:   int

        :return:            reset token
        :rtype:             JSON web token
        """
        return jwt.encode({"reset_password": self.account_id,
                           "exp": time() + expires_in},
                           app.config["SECRET_KEY"], algorithm="HS256") \
                           .decode("utf-8")

    def verify_reset_token(token):
        """
        Verify if a given reset token matches an account.

        :param token:       reset token
        :type token:        JSON web token

        :return:            account ID if there's a match, `None` otherwise
        :rtype:             int or `None`
        """
        try:
            account_id = jwt.decode(token, app.config['SECRET_KEY'],
                                    algorithms=['HS256'])['reset_password']
        except:
            return None

        return account_id

    def get_activate_token(self):
        """
        Generate a token that can be used to activate an account.

        :param self:        account
        :type self:         account object

        :return:            activation token
        :rtype:             JSON web token
        """
        return jwt.encode({"account": self.account_id},
                          app.config["SECRET_KEY"],
                          algorithm="HS256").decode("utf-8")

    def verify_activate_token(token):
        """
        Verify if a given activation token matches an account.

        :param token:       activation token
        :type token:        JSON web token

        :return:            account ID if there's a match, `None` otherwise
        :rtype:             int or `None`
        """
        try:
            account_id = jwt.decode(token, app.config['SECRET_KEY'],
                                    algorithms=['HS256'])['account']
        except:
            return None

        return account_id

    def is_authenticated(self):
        """
        Check if an account is authenticated.

        :param self:        account
        :type self:         account object

        :return:            `True`
        :rtype:             boolean
        """
        return True

    def is_active(self):
        """
        Check if an account is active.

        :param self:        account
        :type self:         account object

        :return:            `True`
        :rtype:             boolean
        """
        return True

    def is_anonymous(self):
        """
        Check if an account is anonymous.

        :param self:        account
        :type self:         account object

        :return:            `False`
        :rtype:             boolean
        """
        return False

    def get_id(self):
        """
        Get account's ID.

        :param self:        account
        :type self:         account object

        :return:            account ID
        :rtype:             int
        """
        return self.account_id

    def serialize_json(self):
        """
        Convert account data into JSON format.

        :param self:        account
        :type self:         account object

        :return:            account data
        :rtype:             JSON
        """
        return {
            "account_id": self.account_id,
            "username": self.username,
            "email": self.email,
            "user_id": self.user_id,
        }

    def deserialize_json(self, json):
        """
        Set values from account data in JSON format, into account object.

        :param self:        account
        :type self:         account object
        :param json:        account data
        :type json:         JSON
        """
        self.account_id = json["account_id"]
        self.username = json["username"]
        self.password = json["password"]
        self.email = json["email"]
        self.user_id = json["user_id"]
