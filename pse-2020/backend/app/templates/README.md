# Chesschain - Backend - App - Templates

The 'templates' directory contains different templates that are used when sending out emails via our back end.

The following templates have been defined:
- account_created: template for the email a user receives after creating an account, containing a link to activate the account
- reset_password: template for the email a user receives after requesting a reset of their password

The templates directory also contains a directory with 'template_images'. This now only contains our logo, but could be filled with additional images that could be used in emails.