#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
The `Matches API` contains endpoint functions for retrieving users' match data.
"""

__author__ = [""]
__credits__ = [""]
__version__ = "3.0"
__maintainer__ = "Back-end team"
__status__ = "In progress"
__docformat__ = "reStructuredText"


from flask import request
from flask_login import current_user, login_required
from app import app
from app.repository import user_repo, account_repo
from app.functionalities import blockchain


@app.route('/api/matches/leaderboard', methods=['GET'])
def leaderboard():
    """
    Function to retrieve ELO rating of all users to display on leaderboard

    :return:    `Status 200` + user data
    :rtype:     HTTP Response
    """
    leaders = user_repo.get_leaderboard()
    leaderboard = []

    for i, u in enumerate(leaders, 1):
        user_account = account_repo.get_account_by_user_id(u.user_id)
        leaderboard.append({"place": i,
                            "username": user_account.username,
                            "elo_rating": u.elo_rating,
                            "user_id": u.user_id})

    return {
        "status": "ok",
        "leaderboard": leaderboard
    }, 200


@app.route('/api/matches/history', methods=['GET'])
def match_history():
    """
    Function to retrieve all matches from blockchain

    :return:    `Status 200` + blockchain games
    :rtype:     HTTP Response

    .. todo:: actually retrieve the matches from blockchain
    """
    try:
        games = blockchain.get_all_recent_games()
    except:
        return {
            "status": "Connection to the blockchain failed"
        }, 503

    return {
        "status": "ok",
        "games": games["games"],
    }
