#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
The `Login API` contains endpoint functions for the login/logout system.
"""

__author__ = [""]
__credits__ = [""]
__version__ = "3.0"
__maintainer__ = "Back-end team"
__status__ = "In progress"
__docformat__ = "reStructuredText"


from flask import request, make_response
from flask_login import current_user, login_user, logout_user, login_required
from werkzeug.security import check_password_hash
from app import app
from app.models import db, User
from app.repository import account_repo


@app.route("/api/login", methods=["POST"])
def login():
    """
    Function to log in to an account.

    :return:    `Status 400` if login data is missing, invalid or the account
                is not activated,
                `status 200` otherwise
    :rtype:     HTTP Response
    """
    if current_user.is_authenticated:
        return {
            "status": "already logged in",
            "account_id": current_user.account_id,
            "username": current_user.username,
        }, 200

    data = request.get_json()

    if not data or "email" not in data or "password" not in data:
        return {
            "status": "Bad request"
        }, 400

    email = data["email"].lower().strip()
    password = data["password"]
    account = account_repo.get_account_by_email(email)

    if account is None or not account.check_password(password):
        return {
            "status": "Invalid username and password combination"
        }, 400

    if not account.account_activated:
        return {
            "status": "Account not activated"
        }, 400

    login_user(account)

    return {
        "status": "ok",
        "account_id": account.account_id,
        "username": account.username,
    }, 200


@app.route('/api/logout', methods=["POST"])
def logout():
    """
    Function to log out of an account.

    :return:    `Status 200`
    :rtype:     HTTP Response
    """
    logout_user()

    return {
        "status": "ok"
    }, 200
