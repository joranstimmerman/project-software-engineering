#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
The `User API` contains endpoint functions for user related functionality like
retrieving user data and signing games.
"""


__author__ = [""]
__credits__ = [""]
__version__ = "3.0"
__maintainer__ = "Back-end team"
__status__ = "In progress"
__docformat__ = "reStructuredText"


from flask import request
from flask_login import current_user, login_required
from app import app
from app.models import User, db, Games
from app.repository import user_repo, websites_repo, games_repo, association_repo, account_repo
from app.functionalities import blockchain, upload
from datetime import date
import json
import operator


@app.route("/api/user/retrieve/<int:user_id>", methods=["GET"])
def retrieve_user_data_by_userID(user_id):
    """
    Function that retrieves user, website and game data upon passing a user ID.


    :param user_id:     User ID
    :type user_id:      int
    :return:            `Status 400` if user does not exist,
                        `status 200` + user, website and game data otherwise
    :rtype:             HTTP Response

    .. todo:: actually retrieve the matches from blockchain
    """
    user = user_repo.get_user_by_id(user_id)

    if user is None:
        return {"status": "User does not exist"}, 400

    acc = account_repo.get_account_by_id(user_id)

    website_names = websites_repo.get_website_names_by_user_id(user.user_id)
    games = ""

    try:
        for website_name in website_names:
            if games == "":
                games = blockchain.find_games("username " \
                                            + website_name.player_name)
            else:
                new_games = blockchain \
                            .find_games("username " \
                                        + website_name.player_name)["games"]

                for game in new_games:
                    games["games"].append(game)
    except:
        return {
            "status": "Connection to the blockchain failed"
        }, 503

    if not games:
        return {
            "status": "No games found",
            "username": acc.username,
            "user": user.serialize_json(),
            "website_names": [name.serialize_json() for name in website_names],
            "games": []
        }, 200

    game_list = games["games"]
    game_list.sort(key=operator.itemgetter("date"))
    game_list.reverse()

    return {
        "status": "ok",
        "user": user.serialize_json(),
        "username": acc.username,
        "website_names": [name.serialize_json() for name in website_names],
        "games": game_list,
    }, 200


@app.route('/api/user/retrieve/unsigned_games', methods=['GET'])
@login_required
def unsigned_games():
    """
    Function that retrieves a user's unsigned games.

    :return:    `Status 200` + game data
    :rtype:     HTTP Response
    """
    games = association_repo.get_unsigned_games_by_id(current_user.user_id)
    list_games = []

    for game in games:
        game_repr = games_repo.get_pgn_by_game_id(game.game_id)
        list_games.append(game_repr)

    list_games.reverse()
    return {
        "status": "ok",
        "pgns":
             [{"pgn": json.loads(g.pgn_data)}
              for g in list_games]
    }, 200


@app.route('/api/user/retrieve/signed_games', methods=['GET'])
@login_required
def signed_games():
    """
    Function that retrieves a user's signed games.

    :return:    `Status 200` + game data
    :rtype:     HTTP Response
    """
    games = association_repo.get_signed_games_by_id(current_user.user_id)
    list_games = []

    for game in games:
        game_repr = games_repo.get_pgn_by_game_id(game.game_id)
        list_games.append(game_repr)
    return {
        "status": "ok",
        "pgns":
             [{"pgn": json.loads(g.pgn_data)}
              for g in list_games]
    }, 200


@app.route('/api/user/retrieve/all_games', methods=['GET'])
@login_required
def all_games():
    """
    Function that retrieves all of a user's games.

    :return:    `Status 200` + game data
    :rtype:     HTTP Response
    """
    games_og = association_repo.get_all_games_by_id(current_user.user_id)

    list_games = []
    for game in games_og:
        game_repr = games_repo.get_pgn_by_game_id(game.game_id)
        list_games.append(game_repr)

    return {
        "status": "ok",
        "pgns":
             [{"pgn": json.loads(g.pgn_data), "game_id": g.game_id, "user_signed": games_og[i].signed}
              for i, g in enumerate(list_games)]
    }, 200


@app.route('/api/user/sign', methods=['POST'])
@login_required
def sign():
    """
    Function that signs a game for a user upon request. When the private key
    is valid the new elo of the players is calculated and the data of the game
    is pushed to the blockchain and is removed from our database. Upon deletion
    the elo of the players are updated in our database.

    :return:    `Status 400`if data is missing,
                `status 401` if the game doesn't exist, or private key is not
                right
                `status 200` otherwise
    :rtype:     HTTP Response

    """

    if ("private_key" not in request.files or
        "game_id" not in request.form):
        return {
            "status": "Bad request"
        }, 400

    private_key_file = request.files["private_key"]
    private_key = str(private_key_file.read(), 'utf-8')
    public_key = current_user.user.blockchain_public_key
    game_id = request.form.get("game_id")

    if not blockchain.verify_keys(private_key, public_key):
        return {
            "status": "Private key does not pair with public key"
        }, 401

    game = association_repo.get_game_by_ids(game_id, current_user.user_id)

    if not game:
        return {
            "status": "That game does not exist"
        }, 401

    game_data = json.loads(game.game.pgn_data)

    if game_data["contestant_1_public_key"]:
        game_data["contestant_2_public_key"] = public_key
    else:
        game_data["contestant_1_public_key"] = public_key

    game_data = blockchain.extract_blockchain_data(game_data)

    if game_data is None:
        return {
            "status": "error while parsing game_data"
        }, 400

    try:
        blockchain.add_game(upload.dict_to_json(game_data))
    except:
        return {
                "status": "Connection to the blockchain failed"
            }, 503

    if public_key == game_data["contestant_1_public_key"]:
        current_user.user.elo_rating = game_data["elo_score_contestant_1"]
        u_opp = user_repo \
                .get_user_by_public_key(game_data["contestant_2_public_key"])
        u_opp.elo_rating = game_data["elo_score_contestant_2"]
    else:
        current_user.user.elo_rating = game_data["elo_score_contestant_2"]
        u_opp = user_repo \
                .get_user_by_public_key(game_data["contestant_1_public_key"])
        u_opp.elo_rating = game_data["elo_score_contestant_1"]

    # if current_user.user == u_opp:
    #     return {
    #         "status": "Cannot sign your own games!"
    #     }, 403

    db.session.add_all([current_user.user, u_opp])
    game_opponent = association_repo \
                    .get_signed_game_by_game_id(game_id)

    game.signed = True
    db.session.delete(game)
    db.session.delete(game_opponent)

    db.session.commit()

    return {
        "status": "ok"
    }, 200


@app.route('/api/user/reject', methods=['POST'])
@login_required
def reject():
    """
    Function that rejects a game that has been uploaded by another player.

    :return:    `Status 400` if data is missing,
                `status 401` if the game doesn't exist
                `status 200` otherwise
    :rtype:     HTTP Response
    """
    data = request.get_json()

    if "game_id" not in data:
        return {
            "status": "Bad request"
        }, 400

    game = association_repo.get_game_by_ids(data["game_id"],
                                            current_user.user_id)
    game_opponent = association_repo \
                    .get_signed_game_by_game_id(data["game_id"])

    db.session.delete(game)
    db.session.delete(game_opponent)
    db.session.commit()
    return {
        "status": "ok"
    }, 200
