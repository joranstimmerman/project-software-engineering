# Chesschain - Backend - App - API

The 'api' directory contains API's that can be called from various parts of the front end.

The following API's have been implemented:
- account_api: contains functionality for account creation and linking
- login_api: contains functionality for the login system
- matches_api: contains functionality for retrieving user's match data
- upload_api: contains functionality for uploading PGN files of matches, that will then be processed and eventually sent to blockchain.
- user_api: contains functionality regarding user data and signing of a user's matches