#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
The `Account API` contains endpoint functions that provide services in regards
to accounts. This includes functions that are responsible for account creation
and linking, among others.
"""

__author__ = [""]
__credits__ = [""]
__version__ = "3.0"
__maintainer__ = "Back-end team"
__status__ = "In progress"
__docformat__ = "reStructuredText"

from flask import request
from flask_login import current_user, login_required
from app import app
from app.models import Account, User, Websites, db
from app.functionalities import mail, blockchain, statistics
from app.repository import account_repo, user_repo, games_repo, \
                           websites_repo, association_repo
from datetime import date
import operator
import os

@app.route("/api/account/signup", methods=["POST"])
def signup():
    """
    Function to create an account upon sign up request.

    :return:    `Status 400` if data is missing,
                `status 401` if login data is already taken,
                `status 200` otherwise
    :rtype:     HTTP Response
    """
    data = request.get_json()

    if (not data or
        "username" not in data or
        "email" not in data or
        "password" not in data):
        return {
            "status": "Bad request"
        }, 400

    usn = data["username"].lower().strip()
    pwd = data["password"]
    email = data["email"].lower().strip()

    if account_repo.get_account_by_username(usn):
        return {
            "status": "That username is already taken"
        }, 401

    if account_repo.get_account_by_email(email):
        return {
            "status": "That Email is already in use"
        }, 401

    account = Account(username=usn, email=email)
    account.set_password(pwd)
    db.session.add(account)
    db.session.commit()

    token = account.get_activate_token()

    if 'PRODUCTION' in os.environ:
        activate_url = 'http://chesschain.nl/activate/' + token
    else:
        activate_url = 'http://localhost:3000/activate/' + token

    mail.send_signup_mail(account, activate_url)

    return {
        "status": "ok",
            # "token": token
    }, 200


@app.route("/api/account/activate/<token>", methods=["POST", "GET"])
def activate_account(token):
    """
    Function to activate an account that has been created before.

    :param token:   Token from reset email to verify that the user has indeed
                    requested a password reset
    :type token:    int
    :return:    `Status 400` if account does not exist in database,
                `status 200` + account ID + account_activated token otherwise
    :rtype:     HTTP Response
    """
    account_id = Account.verify_activate_token(token)

    account = account_repo.get_account_by_id(account_id)

    if not account:
        return {
            "status": "Invalid activation token"
        }, 400

    if account.user_id and account.account_activated:
        return {
            "status": "Your account has already been activated, try logging in"
        }, 400

    private_key, public_key = blockchain.get_keys()

    user = User(blockchain_public_key=public_key)
    db.session.add(user)
    db.session.commit()

    account.account_activated = True
    account.user_id = user.user_id

    db.session.add(account)
    db.session.commit()

    mail.send_private_key_mail(account, private_key)

    return {
        "status": "ok",
        "account_id": account.account_id,
        "account_activated": account.account_activated,
    }, 200


@app.route("/api/account/reset/password", methods=["POST", "GET"])
def send_reset_password_mail():
    """
    Function to send a user an email to reset their password.

    :return:    `Status 400` if data is missing,
                `status 200` otherwise
    :rtype:     HTTP Response
    """
    data = request.get_json()

    if not data or "email" not in data:
        return {
            "status": "Bad request"
        }, 400

    account = account_repo.get_account_by_email(data["email"])
    token = account.get_reset_token()

    if 'PRODUCTION' in os.environ:
        reset_url = 'http://chesschain.nl/reset/password/' + token
    else:
        reset_url = 'http://localhost:3000/reset/password/' + token


    mail.send_reset_mail(account, reset_url)

    return {
        "status": "ok"
    }, 200


@app.route("/api/account/reset/password/<token>", methods=["POST", "GET"])
def reset_password(token):
    """
    Function to change a user's password after reset.

    :param token:   Token from reset email to verify that the user has indeed
                    requested a password reset
    :type token:    int
    :return:        `Status 400` if data is missing,
                    `status 401` if token is invalid,
                    `status 200` + account data otherwise
    :rtype:         HTTP Response
    """
    data = request.get_json()

    if not data or "password" not in data or "token" not in data:
        return {
            "status": "Bad request"
        }, 400

    account_id = Account.verify_reset_token(token)
    account = account_repo.get_account_by_id(account_id)
    pwd = data["password"]

    if account:
        account.set_password(pwd)
        db.session.add(account)
        db.session.commit()

        return {
            "status": "ok",
        }, 200
    else:
        return {
            "status": "Invalid token"
        }, 401


@app.route("/api/account/modify", methods=["POST"])
@login_required
def modify():
    """
    Function to modify a user's account data. Requires user to be logged in.

    :return: `Status 403` if user is not logged in,
             `status 400` if data is missing,
             `status 200` otherwise
    :rtype:  HTTP Response
    """
    data = request.get_json()

    if not data or (
        "username" not in data and
        "password" not in data and
        "email" not in data
    ):
        return {
            "status": "Bad request"
        }, 400

    if "username" in data:
        current_user.username = data["username"].lower().strip()
    elif "password" in data:
        current_user.set_password(data["password"])
    elif "email" in data:
        current_user.email = data["email"]

    db.session.add(current_user)
    db.session.commit()

    return {
        "status": "ok",
        "account_id": current_user.account_id
    }, 200


@app.route("/api/account/retrieve/current", methods=["GET"])
def retrieve_current_account_data():
    """
    Function to retrieve a user's account data.

    :return: `Status 403` if user's not logged in,
             current user data otherwise
    :rtype:  HTTP Response / JSON
    """
    if current_user.is_authenticated is False:
        return {
            "status": "Not logged in"
        }, 403


    games = association_repo.get_unsigned_games_by_id(current_user.user_id)

    return {
        **current_user.serialize_json(),
        "elo_rating": current_user.user.elo_rating,
        "new_unsigned": len(games),
    }, 200


@app.route("/api/account/retrieve/current/all", methods=["GET"])
@login_required
def retrieve_current_account_all_data():
    """
    Function to retrieve a user's account, user, website, and match data.

    :return: `Status 403` if user's not logged in, \
             `status 200` + account data if user has an account but no user
                                         and other data yet, \
             `status 200` + account, user, website, and match data otherwise
    :rtype: HTTP Response

    .. todo:: retrieve the matches from blockchain
    """
    user = user_repo.get_user_by_id(current_user.user_id)

    if not user:
        return {
            "status": "ok",
            "account": current_user.serialize_json(),
            "user": None,
            "games": None,
        }, 200

    website_names = websites_repo.get_website_names_by_user_id(user.user_id)
    games = ""

    try:
        for website_name in website_names:
            if games == "":
                games = blockchain.find_games("username " \
                                            + website_name.player_name)
            else:
                new_games = blockchain \
                            .find_games("username " \
                                        + website_name.player_name)["games"]

                for game in new_games:
                    games["games"].append(game)
    except:
        return {
            "status": "Connection to the blockchain failed"
        }, 503

    unsigned_games = association_repo.get_unsigned_games_by_id(current_user.user_id)

    bug_response =  {
        "status": "No games found",
        "account": current_user.serialize_json(),
        "user": user.serialize_json(),
        "website_usernames": [name.serialize_json()
                              for name in website_names],
        "games": [],
        "number_unsigned": len(unsigned_games),
        "statistics": {"wins": 0, "loses": 0, "draws": 0,
                       "elo_rate": [{"date": date.today(),
                                     "elo": user.elo_rating}],
                      "avgs": [0, 0, 0]},
    }, 200

    if not games:
        return bug_response
    elif games["games"] == []:
        return bug_response

    wins, loses, draws, elo_rate, avgs = statistics.statistics(games,
                                                              current_user,
                                                              user,
                                                              website_names)

    games_list = games["games"]
    games_list.sort(key=operator.itemgetter("date"))
    games_list.reverse()

    return {
        "status": "ok",
        "account": current_user.serialize_json(),
        "user": user.serialize_json(),
        "website_usernames": [name.serialize_json() for name in website_names],
        "games": games_list,
        "number_unsigned": len(unsigned_games),
        "statistics": {"wins": wins, "loses": loses, "draws": draws,
                      "elo_rate": elo_rate, "avg": avgs},
    }, 200


@app.route("/api/account/link", methods=["POST"])
@login_required
def link():
    """
    Function to link user to their 3rd party chess website account

    :return:        `Status 400` if data is missing or the link already exists,
                    `status 200` otherwise
    :rtype:         HTTP Response
    """
    data = request.get_json()

    if (not data or
        "username" not in data or
        "website" not in data):
        return {
            "status": "Bad request"
        }, 400

    website = websites_repo.get_websites_by_player_website(data["username"],
                                                           data["website"])

    if website:
        return {
            "status": "That link has already been created"
        }, 400

    website = Websites(user_id=current_user.user_id,
                      website_name=data["website"],
                      player_name=data["username"])

    db.session.add(website)
    db.session.commit()

    return {
        "status": "ok"
    }, 200


@app.route("/api/account/unlink", methods=["POST"])
@login_required
def unlink():
    """
    Function to unlink user's 3rd party chess website account

    :return:        `Status 400` if data is missing or the link does not exist
                    (for that user),
                    `status 200` otherwise
    :rtype:         HTTP Response
    """
    data = request.get_json()

    if (not data or
        "username" not in data or
        "website" not in data):
        return {
            "status": "Bad request"
        }, 400

    website = websites_repo.get_websites_by_player_website(data["username"],
                                                           data["website"])

    if not website:
        return {
            "status": "Unkown link"
        }, 400

    if current_user.user_id != website.user_id:
        return {
            "status": "That link is not yours"
        }, 400

    db.session.delete(website)
    db.session.commit()

    return {
        "status": "ok"
    }, 200
