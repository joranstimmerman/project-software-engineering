#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
The `Upload API` contains endpoint functions for uploading match data (PGN)
to process and enter into the blockchain.
"""


__author__ = [""]
__credits__ = [""]
__version__ = "3.0"
__maintainer__ = "Back-end team"
__status__ = "In progress"
__docformat__ = "reStructuredText"


from flask import request
from flask_login import current_user, login_required
from app import app
from app.functionalities import blockchain, upload
from app.repository import websites_repo, games_repo
from app.models import Games, Association, db
from flask import request
import json
import io

@app.route("/api/upload", methods=["POST"])
@login_required
def catch_upload():
    """
    Function that takes an uploaded PGN file, and temporarily stores it into
    our database. PGN data can only be uploaded if the username/website in the
    data is of a 3rd party account that has been linked to our user.

    :return:    `Status 400` if PGN parsing fails, the username has not been
                linked, or the game has already been uploaded,
                `status 401`  if the account has been linked by someone else,
                `status 200` otherwise
    :rtype:     HTTP Response
    """
    if not request.files.get("file") or not request.files.get("private_key"):
        return {
            "status": "Bad request"
        }, 400

    private_key_file = request.files["private_key"]
    private_key = str(private_key_file.read(), "utf-8")
    public_key = current_user.user.blockchain_public_key

    if not blockchain.verify_keys(private_key, public_key):
        return {
            "status": "Private key does not pair with public key"
        }, 401

    # Convert the PGN file to a readable format for chess.pgn
    pgn = io.StringIO(request.files["file"].read().decode("utf-8"))
    print(pgn, flush=True)

    game_data = upload.convert_pgn_to_dict(upload.game_data,
                                           upload.relevant_keys, pgn)

    if game_data is None:
        return {
            "status": "Error while parsing PGN to dict"
        }, 400

    player1, player2 = game_data["username_1"], game_data["username_2"]
    website = game_data["site"]

    player1 = websites_repo.get_websites_by_player_website(player1, website)
    player2 = websites_repo.get_websites_by_player_website(player2, website)

    if not player1 or not player2:
        return {
            "status": "One of the usernames has not yet been linked to an \
                       account"
        }, 400

    if (current_user.user_id != player1.user_id and
        current_user.user_id != player2.user_id):
        return {
            "status": "This account is not linked by you"
        }, 401

    if games_repo.get_game_by_pgn_data(json.dumps(game_data)):
        return {
            "status": "That game already exists"
        }, 400

    p_uploader = player1
    p_opponent = player2

    game_data["contestant_1_public_key"] = None
    game_data["contestant_2_public_key"] = None

    if current_user.user_id == player2.user_id:
        p_uploader = player2
        p_opponent = player1
        game_data["contestant_2_public_key"] = public_key
    else:
        game_data["contestant_1_public_key"] = public_key


    game = Games(pgn_data=json.dumps(game_data))
    assoc = Association(user_id=p_uploader.user_id, game=game, signed=True)
    assoc2 = Association(user_id=p_opponent.user_id, game=game)

    db.session.add_all([game, assoc, assoc2])
    db.session.commit()

    return {
        "status": "ok",
    }, 200
