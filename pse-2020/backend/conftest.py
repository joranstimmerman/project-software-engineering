from app import app as _app
from app.models.database import db as _db
import pytest

@pytest.fixture(scope='session')
def db(app):
    _db.init_app(app)
    _db.create_all()

    yield _db

    _db.drop_all()


@pytest.fixture(scope='session')
def app():
    context = _app.app_context()
    context.push()

    _app.config["TESTING"] = True
    _app.config["SQLALCHEMY_DATABASE_URI"] = "postgresql+psycopg2://test:test123@database_test/db_test"
    _app.config["LOGIN_DISABLED"] = True

    yield _app

    context.pop()


@pytest.fixture(scope='function', autouse=True)
def session(db):
    connection = db.engine.connect()
    transaction = connection.begin()

    options = dict(bind=connection, binds={})
    session_ = db.create_scoped_session(options=options)

    db.session = session_

    yield session_

    transaction.rollback()
    connection.close()
    session_.remove()


@pytest.fixture(scope='session')
def client(app):
    with app.test_client() as client:
        yield client
