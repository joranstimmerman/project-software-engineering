import React from 'react';

const axios = require('axios');

const AddLink = (data) => {
  const [status, setStatus] = React.useState("")
  const [error, setError] = React.useState("")
  const [errorWebsite, setErrorWebsite] = React.useState("")
  const [errorUsername, setErrorUsername] = React.useState("")

  const handleSubmit = (e) => {
    e.preventDefault();

    let err = false
    let form = {}

    if (e.target.username.value.length) {
      form.username = e.target.username.value
    } else {
      setErrorUsername("Please fill in a username")
      err = true
    }

    if (e.target.websiteName.value.length) {
      form.website = e.target.websiteName.value
    } else {
      setErrorWebsite("Please fill in a website")
      err = true
    }

    if (!err) {
      axios.post("/api/account/link", form).then(res => {
        setStatus("Succesfully added the link")
      }).catch(error => {
        setError(error.response.data.status);
      });
    }

  }

  return (
    <>
      <h1 className="text-primary mb-4">Link a new website</h1>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="websiteName">Website Name</label>
          <input className="form-control" id="websiteName" placeholder="Enter website name" />
          <div className="errorMsg">{errorWebsite}</div>
        </div>
        <div className="form-group">
          <label htmlFor="username">Username</label>
          <input  className="form-control" id="username" placeholder="username" />
          <div className="errorMsg">{errorUsername}</div>
        </div>
        <button type="submit" className="btn btn-primary">Add Link</button>
        <div className="errorMsg">{error}</div>
        <div>{status}</div>
      </form>
    </>
  )
}


export default AddLink;
