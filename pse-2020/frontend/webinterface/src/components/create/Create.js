import React from 'react';
import { Modal, Button } from 'react-bootstrap';
import './Create.scss';

const axios = require('axios');

const Create = () => {
  const [usernameError, setUsernameError] = React.useState()
  const [emailError, setEmailError] = React.useState()
  const [emailRepeatError, setEmailRepeatError] = React.useState()
  const [passwordError, setPasswordError] = React.useState()
  const [passwordRepeatError, setPasswordRepeatError] = React.useState()
  const [checkboxError, setCheckboxError] = React.useState()

  const [show, setShow] = React.useState(false);
  const handleShow = () => { setShow(true); setContent(null); setStatus(""); }
  const handleClose = () => setShow(false);
  const [modal_content, setContent] = React.useState(null);
  const [status, setStatus] = React.useState("");

  function handleSubmit(e) {
    e.preventDefault();

    let error = false;

    if (!e.target.Username.value.length) {
      setUsernameError("Please choose a username.");
      error = true;
    } else if (e.target.Username.value.length > 15) {
      setUsernameError("Your username cannot be longer than 15 characters.");
      error = true;
    } else {
      setUsernameError();
    }

    if (!e.target.Password.value.length) {
      setPasswordError("Please choose a password.");
      error = true;
    } else if (e.target.Password.value.length < 8) {
      setPasswordError("Password needs to be at least 8 characters long.");
      error = true;
    } else {
      setPasswordError();
    }

    if (!e.target.Email.value.length) {
      setEmailError("Please fill in an email!");
      error = true;
    } else {
      setEmailError();
    }

    if (e.target.PasswordRepeat.value !== e.target.Password.value){
      setPasswordRepeatError("Passwords do not match!");
      error = true;
    } else {
      setPasswordRepeatError();
    }

    if (e.target.EmailRepeat.value !== e.target.Email.value){
      setEmailRepeatError("Emails do not match!");
      error = true;
    } else {
      setEmailRepeatError();
    }

    if (!e.target.Check1.checked) {
      setCheckboxError("It is required to agree to the T&C!");
      error = true;
    } else {
      setCheckboxError();
    }

    if (!error) {
      axios.post('/api/account/signup', {
        username: e.target.Username.value,
        email: e.target.Email.value,
        password: e.target.Password.value
      }).then(res => {
        setContent(<p>Please look at your email for the verification link</p>)
      }).catch(error => {
        setContent(null);
        setStatus(error.response.data.status);
      });
    }
  }

  const CreateForm = () => (
    <form onSubmit={handleSubmit}>
      <div className="form-group">
        <label htmlFor="Username">Username</label>
        <input type="username" className="form-control" id="Username" placeholder="Username" maxLength="15" />
        <div className="errorMsg">{usernameError}</div>
      </div>
      <div className="form-group">
        <label htmlFor="EmailLogIn">Email address</label>
        <input type="email" className="form-control" id="Email" aria-describedby="emailHelp" placeholder="Enter email"  />
        <div className="errorMsg">{emailError}</div>
      </div>
      <div className="form-group">
        <label htmlFor="EmailLogIn">Confirm email</label>
        <input type="email" className="form-control" id="EmailRepeat" aria-describedby="emailHelp" placeholder="Confirm email"  />
        <div className="errorMsg">{emailRepeatError}</div>
      </div>
      <div className="form-group">
        <label htmlFor="InputPassword">Password (must be at least 8 characters)</label>
        <input type="password" className="form-control" id="Password" placeholder="Password" />
        <div className="errorMsg">{passwordError}</div>
      </div>
      <div className="form-group">
        <label htmlFor="InputPassword">Confirm Password</label>
        <input type="password" className="form-control" id="PasswordRepeat" placeholder="Password" />
        <div className="errorMsg">{passwordRepeatError}</div>
      </div>
      <div className="form-check mb-3">
        <input type="checkbox" className="form-check-input" id="Check1" />
        <label className="form-check-label" required={true} htmlFor="Check1">I agree to the <a target="_blank" href="/terms-and-conditions">terms and conditions</a></label>
        <div className="errorMsg">{checkboxError}</div>
      </div>
      <button type="submit" className="btn btn-primary">Register</button>
      <div className="errorMsg">{status}</div>
    </form>
  )

  return (
    <>
      <Button className="signup mr-1" onClick={handleShow}>Sign up</Button>

      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >

        <Modal.Header closeButton>
          <Modal.Title>Create Account</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {
            (modal_content !== null) ? (
              modal_content
            ) : (
              <CreateForm />
            )
          }
        </Modal.Body>
      </Modal>
    </>
  );
}

export default Create;
