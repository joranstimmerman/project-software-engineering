import React from "react";
import { Table } from "react-bootstrap";
import './Leaderboard.scss';
import { useHistory } from "react-router-dom";

const LeaderboardCom = (props) => {
  /* Gather the relevant data - live */
  const history = useHistory();

  function routeUser(e) {
    history.push('/user/' + e.currentTarget.id)
  }

  function renderLeaderboard() {
    return props.leaderboard.map((u) => {
      return <tr key={u.user_id} id={u.user_id} onClick={routeUser}>
        <td><b>{u.place}.</b></td>
        <td>{u.username}</td>
        <td>{u.elo_rating}</td>
      </tr>;
    });
  }

  return (
    <Table hover className="hover leaderboard mt-4">
      <thead>
        <tr>
          <th>Place</th>
          <th>User</th>
          <th>Elo</th>
        </tr>
      </thead>
      <tbody>{renderLeaderboard()}</tbody>
    </Table>
  );
};

export default LeaderboardCom;
