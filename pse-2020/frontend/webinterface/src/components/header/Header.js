import React from "react";
import { NavLink, Link } from "react-router-dom";
import { Container, Navbar, Nav, Button, Badge } from "react-bootstrap";
import Logout from "../logout/Logout.js";
import Login from "../login/Login.js";
import NotificationBadge from '../notifications/NotificationBadge';
import Create from "../create/Create.js";
import "./Header.scss";

const axios = require('axios');

const HeaderButtons = () => {
  const [userData, setUserData] = React.useState();
  const headerLogIn = () => {setUserData(null);}
  const headerLogOut = () => {setUserData(false);}
  const [notification, setNotification] = React.useState(false)
  const notificationOff = () => {setNotification(false);}

  if (!userData) {
    axios.get("/api/account/retrieve/current", {}).then((res) => {
      setUserData(res.data);

      /* Check if there are new unsigned matches to show a notification. */
      if (res.data.new_unsigned > 0) {
        setNotification(true);
      }
    }).catch((error) => {});
  }

  function showBadge(where) {
    if (notification) {
      return <NotificationBadge
        number={userData.new_unsigned}
        where={where}
      />
    }
  }

  return userData ? (
    <div className="d-flex flex-column flex-md-row">
      <div className="d-flex justify-content-center pt-2 pb-3 py-md-0">
        <Link to="/profile">
          <Button className="profile-circle rounded-circle bg-light text-center text-dark">
            {userData.username[0].toUpperCase()}
            { showBadge("profile-badge") }
          </Button>
        </Link>
        <div className="text-light small d-flex flex-column mx-2 text-left">
          <div>{userData.username}</div>
          <div><b>{userData.elo_rating}</b></div>
        </div>
      </div>
      <div>
        <Logout
          updateHeader={headerLogOut}
          updateNotification={ notificationOff }
        />
      </div>
    </div>
  ) : (
    <div>
      <Login updateHeader={headerLogIn} />
      <Create />
    </div>
  );
};

const Header = () => {
  return (
    <Navbar expand="md">
      <Container fluid>
        <Navbar.Brand as={NavLink} to="/" className="p-0 mr-3">
          <img
            height="54"
            width="54"
            src={process.env.PUBLIC_URL + "/logo192.png"}
            alt=""
          />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="w-100 text-center text-md-left">
            <Nav.Link as={NavLink} to="/leaderboard">
              Leaderboard
            </Nav.Link>
            <Nav.Link as={NavLink} to="/blockchain">
              Blockchain
            </Nav.Link>
            <span className="flex-fill" />
            <HeaderButtons notifications={3} />
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default Header;
