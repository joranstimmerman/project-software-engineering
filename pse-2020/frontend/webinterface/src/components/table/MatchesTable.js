import React, { Component } from "react";
import Select from "react-select";
import { Table, Button } from "react-bootstrap";
import PrivateKeyModal from "../modal/Modal";
import {
  CreateRow,
  CreateUserComponent,
  CreateRowSpcer,
} from "./MatchesTableComponents";

import "./MatchesTable.scss";

const axios = require("axios");

export default class MatchesTable extends Component {
  constructor(props) {
    super(props);

    this.state = {
      show: false,
      game_active: null,
      filter: { signed_you: true, signed_other: true },
      useData: [],
    };
    this.options = [
      { value: "signed_you", label: "Verified by you" },
      { value: "signed_other", label: "Verified by opponent" },
    ];
  }

  getUserData() {
    if (!this.state.userData) {
      axios
        .get("/api/account/retrieve/current", {})
        .then((res) => {
          this.setState({ usedata: res.data });
        })
        .catch((error) => {});
    }
  }

  async componentDidMount() {
    this.getUserData();
  }

  // Toggles if modal should be shown and for what game
  showModal(e) {
    if (e) {
      this.setState({ game_active: e.target.id });
    }

    this.setState({ show: !this.state.show });
  }

  getUsersInfo(m) {
    var result = this.props.all_data.website_usernames.filter((obj) => {
      return obj.website_name === m.pgn.site;
    })[0];

    var user_info;


    if (m.pgn.username_1 === result.player_name) {
      user_info = {
        user: 1,
        signed_you: m.pgn.contestant_1_public_key !== null,
        user_other: 2,
        signed_other: m.pgn.contestant_2_public_key !== null,
      };

      if (m.pgn.result === "0-1") {
        user_info.won_you = false;
        user_info.won_other = true;
      } else if (m.pgn.result === "1-0") {
        user_info.won_you = true;
        user_info.won_other = false;
      } else {
        user_info.won_you = false;
        user_info.won_other = false;
      }

      return user_info;

    } else if (m.pgn.username_2 === result.player_name) {
      user_info = {
        user: 2,
        signed_you: m.pgn.contestant_2_public_key !== null,
        user_other: 1,
        signed_other: m.pgn.contestant_1_public_key !== null,
      };

      if (m.pgn.result === "0-1") {
        user_info.won_you = true;
        user_info.won_other = false;
      } else if (m.pgn.result === "1-0") {
        user_info.won_you = false;
        user_info.won_other = true;
      } else {
        user_info.won_you = false;
        user_info.won_other = false;
      }

      return user_info;
    }
  }

  rowButtons(m) {
    return (!m.user_signed) ? [
      <td className="row-button">
        <Button variant="primary">See Match</Button>
        <Button
          className="px-4"
          variant="success"
          id={m.game_id}
          onClick={this.showModal.bind(this)}
        >
          Sign
        </Button>
      </td>,
    ] : [
      <td className="row-button">
        <Button variant="primary">See Match</Button>
        <Button variant="success" className="disabled-btn" disabled={true}>
          Signed
        </Button>
      </td>,
    ]
  }

  rowComponents() {
    var user_info;
    return this.props.data.map((m, i) => {
      console.log("info:",this.getUsersInfo(m))
      if (
        (this.state.filter.signed_you && this.getUsersInfo(m).signed_you) ||
        (this.state.filter.signed_other && this.getUsersInfo(m).signed_other)
      ) {
        user_info = this.getUsersInfo(m);

        var table_components = [
          CreateUserComponent(m, user_info),
          <td>{m.pgn.result}</td>,
          <td>{m.pgn.date}</td>,
          <td>{m.pgn.site}</td>,
          this.rowButtons(m),
        ];

        return [
          CreateRow(table_components, m, user_info),
          CreateRowSpcer(m, false),
        ];
      }

      return null;
    });
  }

  handleSelection(selectedOptions) {
    var selected = { signed_you: false, signed_other: false };

    if (selectedOptions) {
      selectedOptions.forEach((option) => {
        selected[option.value] = true;
      });
    }

    this.setState({ filter: selected });
  }

  render() {
    return (
      <div>
        <Select
          defaultValue={this.options}
          options={this.options}
          isMulti
          onChange={this.handleSelection.bind(this)}
        ></Select>
        <Table className="matches-table">
          <thead>
            <tr>
              <th>White</th>
              <th>Black</th>
              <th>Score</th>
              <th>Game Date</th>
              <th>Site</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {this.rowComponents()}
            <tr>
              <td colSpan={7} className="start-text roboto-regular text-primary">
                <b>Start of your chain</b>
              </td>
            </tr>
          </tbody>
        </Table>
        <PrivateKeyModal
          handleClose={this.showModal.bind(this)}
          show={this.state.show}
          game_id={this.state.game_active}
        />
      </div>
    );
  }
}
