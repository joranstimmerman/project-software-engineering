import React, { Component } from "react";
import { Table, Button, Row } from "react-bootstrap";
import PrivateKeyModal from "../modal/Modal";
import {
  CreateRow,
  CreateUserComponent,
  CreateRowSpcer,
} from "./MatchesTableComponents";

import "./MatchesTable.scss";

const axios = require("axios");

export default class BlockchainMatchesTable extends Component {
  constructor(props) {
    super(props);

    this.state = {
      show: false,
      game_active: null,
      filter: { signed_you: true, signed_other: true },
      useData: [],
    };
    this.options = [
      { value: "signed_you", label: "Verified by you" },
      { value: "signed_other", label: "Verified by opponent" },
    ];
  }

  getUserData() {
    if (!this.state.userData) {
      axios
        .get("/api/account/retrieve/current", {})
        .then((res) => {
          this.setState({ usedata: res.data });
        })
        .catch((error) => {});
    }
  }

  async componentDidMount() {
    this.getUserData();
  }

  // Toggles if modal should be shown and for what game
  showModal(e) {
    if (e) {
      this.setState({ game_active: e.target.id });
    }
    this.setState({ show: !this.state.show });
  }

  eloEvolution(m, prev_game) {
    var prev_elo = 600;
    if (prev_game.pgn) {
      prev_elo = this.getUsersInfo(prev_game).elo_you;
    }

    var cur_elo = this.getUsersInfo(m).elo_you;
    var diff_elo = cur_elo - prev_elo;

    var elo_change_comp = this.getEloChange(diff_elo);

    return (
      <td>
        {prev_elo} {elo_change_comp} &rarr; {cur_elo}
      </td>
    );
  }

  getEloChange(eloChange) {
    if (eloChange > 0) {
      return (
        <span className="text-success">
          <b>+{eloChange}</b>
        </span>
      );
    } else if (eloChange < 0) {
      return (
        <span className="text-danger">
          <b>{eloChange}</b>
        </span>
      );
    } else {
      return (
        <span>
          <b>{eloChange}</b>
        </span>
      );
    }
  }

  getUsersInfo(m) {
    if (!this.props.all_data.website_usernames) {
      return {};
    }

    var result = this.props.all_data.website_usernames.filter((obj) => {
      return obj.website_name === m.pgn.site;
    })[0];

    var user_info;

    if (m.pgn.username_1 === result.player_name) {
      user_info = {
        user: 1,
        elo_you: m.pgn.elo_score_contestant_1,
        user_other: 2,
        elo_other: m.pgn.elo_score_contestant_2,
      };

      if (m.pgn.result === "0-1") {
        user_info.won_you = false;
        user_info.won_other = true;
      } else if (m.pgn.result === "1-0") {
        user_info.won_you = true;
        user_info.won_other = false;
      } else {
        user_info.won_you = false;
        user_info.won_other = false;
      }

      return user_info;
    } else if (m.pgn.username_2 === result.player_name) {
      user_info = {
        user: 2,
        elo_you: m.pgn.elo_score_contestant_2,
        user_other: 1,
        elo_other: m.pgn.elo_score_contestant_1,
      };

      if (m.pgn.result === "0-1") {
        user_info.won_you = true;
        user_info.won_other = false;
      } else if (m.pgn.result === "1-0") {
        user_info.won_you = false;
        user_info.won_other = true;
      } else {
        user_info.won_you = false;
        user_info.won_other = false;
      }

      return user_info;
    }
  }

  rowButtons(m) {
    var buttons = [
      <td className="row-button">
        <Button variant="primary">See Match</Button>
      </td>,
    ];

    return buttons;
  }

  rowComponents() {
    var prev_game;
    var user_info;

    return this.props.all_data.games.map((m, i) => {
      m = { pgn: m };

      if (i < this.props.all_data.games.length) {
        prev_game = { pgn: this.props.all_data.games[i + 1] };
      } else {
        prev_game = null;
      }

      user_info = this.getUsersInfo(m);

      const table_components = (!this.props.blockchain_page) ? [
        CreateUserComponent(m, user_info),
        <td>{m.pgn.result}</td>,
        this.eloEvolution(m, prev_game),
        <td>{m.pgn.date}</td>,
        <td>{m.pgn.site}</td>,
        this.rowButtons(m),
      ] : [
        CreateUserComponent(m, user_info),
        <td>{m.pgn.result}</td>,
        <td></td>,
        <td>{m.pgn.date}</td>,
        <td>{m.pgn.site}</td>,
        this.rowButtons(m),
      ];

      return [
        CreateRow(table_components, m, user_info),
        CreateRowSpcer(m, true),
      ];
    });
  }

  render() {
    let starting_point, elo_evolution_header;
    if (this.props.blockchain_page) {
      elo_evolution_header = "";
      starting_point = (
        <b>See more</b>
      );
    } else {
      elo_evolution_header = [<p>ChessChain</p>, <p>Elo evolution</p>];
      starting_point = (
        <b>Start of your chain at 600 elo</b>
      );
    }

    return (
      <div>
        <Row>
          <Table className="matches-table">
            <thead>
              <tr>
                <th>White</th>
                <th>Black</th>
                <th>Score</th>
                <th>{elo_evolution_header}</th>
                <th>Verified Date</th>
                <th>Site</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {this.rowComponents()}
              <tr>
                <td colSpan={7} className="start-text roboto-regular text-primary">
                  {starting_point}
                </td>
              </tr>
            </tbody>
          </Table>
          <PrivateKeyModal
            handleClose={this.showModal.bind(this)}
            show={this.state.show}
            game_id={this.state.game_active}
          />
        </Row>
      </div>
    );
  }
}
