import React from "react";

// Creates component that shows if a game is verified and by who
const CreateVerifiedBy = (m) => {
  var user1_signed = (
    <p className="text-success font-weight-bold">
      Verified by {m.pgn.username_1}
    </p>
  );
  var user1_not_signed = <p>To be verified by {m.pgn.username_1}</p>;
  var user2_signed = (
    <p className="text-success font-weight-bold">
      Verified by {m.pgn.username_2}
    </p>
  );
  var user2_not_signed = <p>To be verified by {m.pgn.username_2}</p>;

  var VerfiedComponets = [];

  if (m.pgn.contestant_1_public_key !== null) {
    VerfiedComponets.push(user1_signed);
  } else {
    VerfiedComponets.push(user1_not_signed);
  }

  if (m.pgn.contestant_2_public_key !== null) {
    VerfiedComponets.push(user2_signed);
  } else {
    VerfiedComponets.push(user2_not_signed);
  }

  VerfiedComponets.push(<p>Waiting for Blockchain placement</p>);

  return VerfiedComponets;
};

const CreateUserComponent = (m, user_info) => {
  var user1 = <td>{m.pgn.username_1} </td>;
  var user2 = <td>{m.pgn.username_2} </td>;

  if (user_info.user === 1) {
    user1 = (
      <td>
        <b>You</b>
      </td>
    );
  } else if (user_info.user === 2) {
    user2 = (
      <td>
        <b>You</b>
      </td>
    );
  }

  return [user1, user2];
};

const CreateRowSpcer = (m, blockchain) => {
  if (!blockchain) {
    return (
      <tr id="spacer" className="bg-light text-align-center">
        <td></td>
        <td></td>
        <td></td>
        <td>
          <div className="h-100 dashed-line mx-auto"></div>
        </td>
        <td></td>
        <td className="spacer-text roboto-thin">{CreateVerifiedBy(m)}</td>
      </tr>
    );
  } else {
    return (
      <tr id="spacer" className="bg-light text-align-center">
        <td></td>
        <td></td>
        <td></td>
        <td>
          <div className="h-100 dashed-line mx-auto"></div>
        </td>
        <td></td>
        <td></td>
        <td className="spacer-text roboto-thin">
          <p className="text-success font-weight-bold">Verified and placed </p>
          <p className="text-success font-weight-bold"> in ChessChain</p>
        </td>
      </tr>
    );
  }
};

const CreateRow = (table_components, m, user_info) => {
  var rowComponent = (
    <tr className="border-left border-light rounded-top">{table_components}</tr>
  );

  if (user_info.won_you) {
    rowComponent = (
      <tr className="border-left border-success">{table_components}</tr>
    );
  } else if (user_info.won_other) {
    rowComponent = (
      <tr className="border-left border-danger">{table_components}</tr>
    );
  }

  return rowComponent;
};

export { CreateVerifiedBy, CreateUserComponent, CreateRow, CreateRowSpcer };
