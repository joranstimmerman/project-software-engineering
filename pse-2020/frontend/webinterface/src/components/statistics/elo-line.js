import React from 'react';
import moment from 'moment';

import {
  VictoryChart,
  VictoryLine,
  VictoryAxis,
  VictoryLabel,
  VictoryTheme,
  VictoryTooltip,
  VictoryVoronoiContainer } from 'victory';

const EloLine = (input) => {
  const sharedAxisStyles = {
    tickLabels: {
      fontSize: 10
    },
    axisLabel: {
      padding: 39,
      fontSize: 10,
    }
  };


  const transformedInput = input.data.map((i) => {
    return {elo:i.elo, date:moment(i.date, 'YYYY.MM.DD').format("DD MMM, YYYY")}
  })



  return (
    <VictoryChart
      theme={ VictoryTheme.material }
      height={200}
      width={600}
      containerComponent={
        <VictoryVoronoiContainer
          voronoiDimension="x"
          labels={({ datum }) => `Elo: ${ datum.elo }`}
          labelComponent={
            <VictoryTooltip
              cornerRadius={0}
              flyoutStyle={{ fill: "white" }}
            />
          }
        />
      }
      domainPadding={{ y: 5 }}
    >
      <VictoryLabel
        x={300}
        y={25}
        textAnchor="middle"
        text="Elo over time"
      />
      <VictoryAxis label="Time" style={sharedAxisStyles} />
      <VictoryAxis dependentAxis label="Elo" style={sharedAxisStyles} />
      <VictoryLine
        title="Elo points over time"
        x="date"
        y="elo"
        style={{
          data: { stroke: "#5379FF" },
          parent: { border: "1px solid #ccc"}
        }}
        data={ transformedInput }
      />
    </VictoryChart>
  );
}

export default EloLine;
