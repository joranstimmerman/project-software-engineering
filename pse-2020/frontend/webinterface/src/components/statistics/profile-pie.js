import React, { useState, useEffect } from 'react';
import { VictoryPie, VictoryTooltip, VictoryContainer } from 'victory';
import "./profile-pie.scss";


const ProfilePie = (input) => {
  const [wins, setWins] = useState(0)
  const [losses, setLosses] = useState(0)
  const [draws, setDraws] = useState(0)
  const [winPercentage, setWinPercentage] = useState(0)
  const [losePercentage, setLosePercentage] = useState(0)
  const [drawPercentage, setDrawPercentage] = useState(0)

  /**
   * Set the component variables in the effect hook
   * of react.
   */
  useEffect(() => {
    const _wins = input.data[0].y
    const _loses = input.data[1].y
    const _draws = input.data[2].y
    const _total = _wins + _loses + _draws

    setWins(_wins)
    setLosses(_loses)
    setDraws(_draws)

    if (_total) {
      setWinPercentage(Math.round((_wins / _total) * 100).toString() + "%")
      setLosePercentage(Math.round((_loses / _total) * 100).toString() + "%")
      setDrawPercentage(Math.round((_draws / _total) * 100).toString() + "%")
    } else {
      setWinPercentage("0%")
      setLosePercentage("0%")
      setDrawPercentage("0%")
    }

    return () => {};
  }, [input])

  return (
    <VictoryContainer height={260} width={250}>
      <text fill="#ed1109" fontSize="17" x={90} y={130}>{ losses }</text>
      <text fill="#11e60e" fontSize="17" x={90} y={110}>{ wins }</text>
      <text fill="#a2a2a2" fontSize="17" x={90} y={150}>{ draws }</text>
      <text fill="#ed1109" fontSize="13" x={130} y={130}>{ winPercentage }</text>
      <text fill="#11e60e" fontSize="13" x={130} y={110}>{ losePercentage }</text>
      <text fill="#a2a2a2" fontSize="13" x={130} y={150}>{ drawPercentage }</text>
      <VictoryPie
        labelComponent={
          <VictoryTooltip
            pointerOrientation="right"
            dy={0}
            dx={0}
            cornerRadius={0}
            centerOffset={{ x: 0 }}
            flyoutStyle={{ stroke: "#007bff", strokeWidth: 1, backgroundColor: "white" }}
            style={{ fill : ""}}
          />
        }
        innerRadius={110}
        standalone={false}
        width={250} height={250}
        colorScale={["#11e60e", "#ed1109", "#a2a2a2"]}
        data={ input.data }
        x="x"
        y="y"
        events={[{
          target: "data",
          eventHandlers: {
            onMouseOver: () => {
              return [
                {
                  target: "data",
                  mutation: () => ({style: {fill: "gold", width: 30}})
                }, {
                  target: "labels",
                  mutation: () => ({ active: true })
                }
              ];
            },
            onMouseOut: () => {
              return [
                {
                  target: "data",
                  mutation: () => {}
                }, {
                  target: "labels",
                  mutation: () => ({ active: false })
                }
              ];
            }
          }
        }]}
      />
    </VictoryContainer>
  )
}

export default ProfilePie;
