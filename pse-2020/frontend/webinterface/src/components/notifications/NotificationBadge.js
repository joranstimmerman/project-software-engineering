import React from 'react';
import { Badge } from 'react-bootstrap';

import "./NotificationBadge.scss";

const NotificationBadge = (input) => {
  return (
    <Badge variant={`danger ${input.where}`}>{ input.number }</Badge>
  )
}


export default NotificationBadge;
