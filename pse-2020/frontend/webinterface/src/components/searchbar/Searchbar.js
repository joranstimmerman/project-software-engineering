import React, { Component } from 'react';
import { InputGroup, FormControl, Button } from 'react-bootstrap';


export default class Searchbar extends Component {
    render() {
        return (
        <InputGroup className="searchbar mb-3">
            <FormControl
            placeholder={this.props.children}
            aria-label="Search"
            aria-describedby="basic-addon1"/>
            <InputGroup.Append>
            <Button variant="primary">Search</Button>
            </InputGroup.Append>
        </InputGroup>
        )
    }
}
