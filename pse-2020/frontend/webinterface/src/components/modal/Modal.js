import React, {  } from "react";
import { Button, Modal, Row, Col } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import { useDropzone } from "react-dropzone";

const axios = require("axios");

const PrivateKeyModal = (props) => {
  const [errorModMsg, setErrorModMsg] = React.useState();
  const [_, setErrorMsg] = React.useState();
  const { acceptedFiles, getRootProps, getInputProps } = useDropzone({
    accept: ".txt",
  });

  const handleClose = () => props.handleClose();
  const history = useHistory();

  function onSign(e) {
    try {
      const formData = new FormData();
      formData.append("game_id", props.game_id);
      formData.append("private_key", acceptedFiles[0]);

      axios
        .post("api/user/sign", formData, {})
        .then((res) => {
          console.log(res);
          history.push("/profile");
          handleClose();
          setErrorModMsg();
          setErrorMsg();
          window.location.reload(false);
        })
        .catch((error) => {
          console.log(error);

          setErrorModMsg(error.response.data.status);
        });
    } catch (error) {
      console.log("help");
    }
  }

  const file_info = acceptedFiles.map((file) => {
    return (
      <div>
        {file.path} - {file.size} bytes
      </div>
    );
  });

  function onReject(e) {
    e.preventDefault();
    try {
      axios
        .post("/api/user/reject", {
          game_id: props.game_id,
        })
        .then((res) => {
          console.log(res);
          window.location.reload(false);
        })
        .catch((error) => {
          console.log(error);
          if (
            error.response.data.status ===
              "One of the usernames has not yet been linked to an account" ||
            error.response.data.status ===
              "That username has not yet been linked to an account"
          ) {
            setErrorModMsg(error.response.data.status);
          } else {
            handleClose();
            setErrorMsg(error.response.data.status);
          }
        });
    } catch (error) {
      console.log("help");
    }
  }

  function handleSubmit(e) {
    e.preventDefault();
    if (acceptedFiles.length < 1) {
      setErrorModMsg("Please fill in a private key!");

      return;
    } else {
      setErrorMsg();
    }

    onSign(e);
  }

  return (
    <Modal
      show={props.show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
    >
      <Modal.Header closeButton>
        <Modal.Title>Sign match</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <form>
          <div className="form-group">
            <div>
              <p>
                You are about to sign a chessmatch uploaded by another player.
                Please provide your <b> private key </b> and verify that this
                match is legitmate or <b>reject</b> the match.
              </p>
            </div>{" "}
            <div className="dropzone" {...getRootProps()}>
              <input {...getInputProps()} />
              <span>
                Drag 'n' drop your private key here, or click to select files
              </span>
            </div>
            {file_info}
            <div className="errorMsg">{errorModMsg}</div>
          </div>
          <Row>
            <Col>
              <Button
                onClick={handleSubmit}
                sm={8}
                type="submit"
                className="btn btn-primary"
              >
                Sign this match
              </Button>
            </Col>
            <Col className="text-right">
              <Button
                variant="danger"
                type="submit"
                className="btn btn-primary"
                onClick={onReject}
              >
                Reject this match
              </Button>
            </Col>
          </Row>
        </form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default PrivateKeyModal;
