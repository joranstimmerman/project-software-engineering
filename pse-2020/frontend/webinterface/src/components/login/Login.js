import React from "react";
import { Modal, Button } from "react-bootstrap";
import "./Login.scss";

const axios = require("axios");

const Login = (props) => {
  const [emailError, setEmailError] = React.useState();
  const [passwordError, setPasswordError] = React.useState();
  const [resetEmailError, setEmailResetError] = React.useState();

  const [show, setShow] = React.useState(false);
  const handleShow = () => setShow(true);
  const handleClose = () => setShow(false);

  const [showForgot, setShowForgot] = React.useState(false);
  const handleShowForgot = () => setShowForgot(true);
  const handleCloseForgot = () => setShowForgot(false);

  function handleLogin(e) {
    e.preventDefault();

    let error = false;

    if (!e.target.Password.value.length) {
      setPasswordError("Please fill in a Password!");
      error = true;
    } else {
      setPasswordError();
    }
    if (!e.target.Email.value.length) {
      setEmailError("Please fill in an email!");
      error = true;
    } else {
      setEmailError();
    }

    if (!error) {
      /* Axios call to Login*/
      try {
        axios.post("/api/login", {
          email: e.target.Email.value,
          password: e.target.Password.value,
        }).then((res) => {
          props.updateHeader();
          handleClose();
        }).catch((error) => {
          setPasswordError(error.response.data.status);
        });
      } catch (error) {
        setPasswordError("ERROR: Failed to send data");
      }
    }
  }

  function handleReset(e) {
    e.preventDefault();

    let error = false;

    if (!e.target.resetEmail.value.length) {
      setEmailResetError("Please fill in an email!");
      error = true;
    } else {
      setEmailResetError();
    }
    if (!error) {
      /* Axios call to Login*/
      try {
        axios.post("/api/account/reset/password", {
          email: e.target.resetEmail.value,
        });

        handleCloseForgot();
      } catch (error) {
        console.log("ERROR: Failed to send data");
        console.log(error);
      }
    }
  }

  return (
    <>
      <Button className="login mr-1" onClick={handleShow}>
        Login
      </Button>

      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>Login</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form onSubmit={handleLogin}>
            <div className="form-group">
              <label htmlFor="EmailLogIn">Email address</label>
              <input
                type="email"
                className="form-control"
                id="Email"
                aria-describedby="emailHelp"
                placeholder="Enter email"
              ></input>
              <div className="errorMsg">{emailError}</div>
            </div>
            <div className="form-group">
              <label htmlFor="exampleInputPassword1">Password</label>
              <input
                type="password"
                className="form-control"
                id="Password"
                placeholder="Password"
              ></input>
              <small
                id="emailHelp"
                className="form-text text-muted"
                onClick={() => {
                  handleClose();
                  handleShowForgot();
                }}
              >
                Forgot Password?
              </small>
              <div className="errorMsg">{passwordError}</div>
            </div>

            <button type="submit" className="btn btn-primary">
              Log in
            </button>
          </form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>

      {/* Reset Password modal*/}
      <Modal
        show={showForgot}
        onHide={handleCloseForgot}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>Reset password</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form onSubmit={handleReset}>
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">Email address</label>
              <input
                type="email"
                className="form-control"
                id="resetEmail"
                aria-describedby="emailHelp"
                placeholder="Enter email"
              ></input>
              <div className="errorMsg">{resetEmailError}</div>
            </div>

            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </form>
        </Modal.Body>
        <Modal.Footer>
          <button
            type="secondary"
            className="btn btn-secondary"
            onClick={() => {
              handleCloseForgot();
              handleShow();
            }}
          >
            Login
          </button>
          <Button variant="secondary" onClick={handleCloseForgot}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default Login;
