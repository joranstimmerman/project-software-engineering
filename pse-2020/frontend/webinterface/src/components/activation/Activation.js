import React, { Component } from 'react';
import { Modal, Button } from 'react-bootstrap';
import Homepage from '../../pages/homepage/Homepage';

const axios = require('axios');

export default class Activation extends Component {
  state = {
    title: "Activiating your account...",
    status: <div className="text-center"><div className="spinner-border"><span className="sr-only">Loading...</span></div></div>,
    show: true
  }

  handleClose() {
    this.setState({show: false});
  }

  componentDidMount() {
    const token = window.location.pathname.split("/").pop();

    axios.post('/api/account/activate/' + token).then(res => {
      this.setState({title: "Account Activated!", status:
        <p>
          Your account has been activated!<br />
          Your private key has been send to your email adress
        </p>
      });
    }).catch(error => {
      this.setState({title: "Account Activation failed", status:
        <>
          <p>Account activation failed</p>
          <p>{error.response.data.status}</p>
        </>
      });
    });

    /* Remove the token from the url */
    window.history.pushState({}, '', '/');
  }

  render() {
    return (
      <>
        <Modal
          show={this.state.show}
          onHide={this.handleClose.bind(this)}
          backdrop="static"
          keyboard={false}
        >
          <Modal.Header closeButton>
            <Modal.Title>{this.state.title}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div>
              {this.state.status}
            </div>
          </Modal.Body>
          <Modal.Footer>
          <Button variant="secondary" onClick={this.handleClose.bind(this)}>
            Close
          </Button>
          </Modal.Footer>
        </Modal>

        <Homepage />
      </>
    )
  }
}
