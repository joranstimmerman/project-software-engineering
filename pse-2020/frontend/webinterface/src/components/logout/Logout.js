import React from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import { Button } from 'react-bootstrap';
const axios = require('axios');

const Logout = (props) => {
  const location = useLocation();
  const history = useHistory();

  function handleLogout(e) {
    e.preventDefault();

    try {
      axios.post('/api/logout', {}).then(res => {
        props.updateHeader();
        props.updateNotification();

        if (location.pathname === "/profile") {
          history.push("/");
        }
      }).catch(error => {});
    } catch (error) {
      console.log("ERROR: Failed to send data");
    }
  }

  return <Button className="logout" onClick={handleLogout}>Logout</Button>;
};

export default Logout;
