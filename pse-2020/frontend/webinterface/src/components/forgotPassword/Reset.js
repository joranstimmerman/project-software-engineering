import React from 'react';
import { Modal, Button } from 'react-bootstrap';
import Homepage from '../../pages/homepage/Homepage';

const axios = require('axios');

const Reset = () => {
  const [status, setStatus] = React.useState()
  const [passwordError, setPasswordError] = React.useState()
  const [passwordRepeatError, setPasswordRepeatError] = React.useState()

  const [show, setShow] = React.useState(true);
  const handleClose = () => setShow(false);

  function handleSubmit(e) {
    e.preventDefault();
    console.log(e)
    var token = window.location.pathname.split("/").pop()
    let error = false

    if (!e.target.Password.value.length) {
        setPasswordError("Please choose a password.");
        error = true;
    }

    if (e.target.Password.value.length < 8) {
        setPasswordError("Password needs to be at least 8 characters long.");
        error = true;
    }

    if (e.target.PasswordRepeat.value !== e.target.Password.value) {
        setPasswordRepeatError("Passwords do not match!");
        error = true;
    }

      if (!error) {
        try {
          axios.post('/api/account/reset/password/' + token, {
            password: e.target.Password.value,
            token: token
          }).then((response) => {
            setStatus("Password reset! You can now login")

            /* Remove the token from the url */
            window.history.pushState({}, '', '/');
          }, (error) => {
            setStatus("Could not reset password with token")
          });
        }
        catch (error) {
            setStatus("Could not reset password with token")
        }
      }
  }

  return (
    <>
      <Homepage />

      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >

        <Modal.Header closeButton>
          <Modal.Title>Reset Password</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h3>Enter your new password</h3>
          <form onSubmit={handleSubmit}>
          <div className="form-group">
            <label htmlFor="Password">New Password</label>
            <input type="password" className="form-control" id="Password" placeholder="password" ></input>
            <div className="errorMsg">{passwordError}</div>
          </div>
          <div className="form-group">
            <label htmlFor="PasswordRepeat">Repeat new Password</label>
            <input type="password" className="form-control" id="PasswordRepeat" placeholder="password"></input>
            <div className="errorMsg">{passwordRepeatError}</div>
          </div>
          <button type="submit" className="btn btn-primary">Change</button>
          <div >{status}</div>
          </form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}

export default Reset
