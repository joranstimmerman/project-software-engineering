import "./Profile.scss";
import React from "react";
import { Tab, Row, Col, Nav, Container } from "react-bootstrap";
import Upload from "../../pages/upload/Upload";
import Matches from "../../pages/matches/Matches";
import BlockchainMatches from "../../pages/matches/BlockchainMatches";
import AddLink from "../addLink/AddLink";
import EditProfile from "./EditProfile"
import DisplayLinks from "../links/Links"
import ProfileStatistics from "./ProfileStatistics";

import NotificationBadge from "../notifications/NotificationBadge";

const axios = require("axios");

const Profile = () => {
  const [data, setData] = React.useState(null);
  const [errorMsg, setErrorMsg] = React.useState("");

  function updateData() {
    axios.get("/api/account/retrieve/current/all", {}).then((res) => {
      setData(res.data);
    }).catch((error) => {
      setErrorMsg(error.response.data.status);
    });
  }

  if (!data) {
    updateData()
  }

  if (data) {
    return (
      <Container className="py-4 d-flex flex-column justify-content-center">
        <Tab.Container id="left-tabs-example" defaultActiveKey="My Statistics">
          <Row>
            <Col md={9} className="pr-md-5 order-md-first order-last">
              <Tab.Content>
                <Tab.Pane eventKey="Upload Match">
                  <Upload />
                </Tab.Pane>
                <Tab.Pane eventKey="My Matches">
                  <Matches data={data} />
                </Tab.Pane>
                <Tab.Pane eventKey="Blockchain Matches">
                  <BlockchainMatches data={data} />
                </Tab.Pane>
                <Tab.Pane eventKey="Add Link">
                  <AddLink />
                </Tab.Pane>
                <Tab.Pane eventKey="My Statistics">
                  <ProfileStatistics />
                </Tab.Pane>
                <Tab.Pane eventKey="My Profile">
                  <EditProfile data={data}/>
                  {DisplayLinks(data.website_usernames)}
                </Tab.Pane>
              </Tab.Content>
            </Col>

            <Col md={3} className="mb-3 mb-md-0 ">
              <div className="p-2 bg-light rounded user-nav">
                <Nav variant="pills" className="flex-column">
                  <Nav.Item>
                    <Nav.Link eventKey="My Statistics">My Statistics</Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link eventKey="Blockchain Matches">Blockchain Matches</Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link eventKey="My Matches">Submitted Matches
                      { data.number_unsigned > 0 ?
                        <NotificationBadge
                          number={ data.number_unsigned }
                          where={"unsigned-badge"}
                        /> : ''
                      }
                    </Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link eventKey="Upload Match">Upload Match</Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link eventKey="Add Link">Add Link</Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link eventKey="My Profile">My Profile</Nav.Link>
                  </Nav.Item>
                </Nav>
              </div>
            </Col>
          </Row>
        </Tab.Container>
      </Container>
    );
  } else {
    return (
      <div className="d-flex justify-content-center align-items-center">
        <h1>{errorMsg}</h1>
      </div>
    );
  }
};

export default Profile;
