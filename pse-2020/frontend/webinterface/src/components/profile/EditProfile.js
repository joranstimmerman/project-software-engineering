import "./Profile.scss";
import React from "react";
import { Table, Button } from "react-bootstrap";

const axios = require("axios");

const EditProfile = (props) => {
  const [showEdit, setShowEdit] = React.useState(false);
  const [submitErrorMsg, setSubmitErrorMsg] = React.useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    var form = {};
    if (e.target.username.value.length) {
      form.username = e.target.username.value;
    }

    if (e.target.email.value.length) {
      form.email = e.target.email.value;
    }

    axios
      .post("/api/account/modify", form)
      .then((res) => {
        window.location.reload();
      })
      .catch((error) => {
        setSubmitErrorMsg(error.response.data.status);
      });
  };

  function display_keys(key) {
    switch (key) {
      case "email":
        return "Email address";
      case "username":
        return "Username";
      default:
        return key;
    }
  }

  return !showEdit ? (
    <>
      <h1 className="text-primary mb-4">Profile Information</h1>
      <Table>
        <tbody>
          {Object.entries(props.data.account).map(([key, val]) =>
            key === "account_id" || key === "user_id" ? null : (
              <tr key={key}>
                <td className="font-weight-bold">{ display_keys(key) }</td>
                <td>{val}</td>
              </tr>
            )
          )}
        </tbody>
      </Table>
      <Button className="mb-3" onClick={() => setShowEdit(true)}>Edit profile</Button>
    </>
  ) : (
    <>
      <h1 className="text-primary mb-4">Edit Profile Information</h1>
      <form onSubmit={handleSubmit}>
        <Table>
          <tbody>
            {Object.entries(props.data.account).map(([key, val]) =>
              key === "account_id" || key === "user_id" ? null : (
                <tr key={key + '_e'}>
                  <td className="font-weight-bold"><label htmlFor={key} class="m-0">{ display_keys(key) }</label></td>
                  <td className="p-1">
                    <input
                      type={key}
                      id={key}
                      class="form-control"
                      defaultValue={val}
                    />
                  </td>
                </tr>
              )
            )}
          </tbody>
        </Table>
        <Button type="submit" className="mb-3">Save</Button>
        <Button className="float-right" onClick={() => setShowEdit(false)}>Cancel</Button>
        <div className="errorMsg">{submitErrorMsg}</div>
      </form>
    </>
  );
};

export default EditProfile;
