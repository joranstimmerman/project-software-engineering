import React, { useEffect, useState } from 'react';
import { Row, Col } from 'react-bootstrap';
import EloLine from '../statistics/elo-line';
import ProfilePie from '../statistics/profile-pie';
import "./ProfileStatistics.scss";
import moment from 'moment';

const axios = require('axios');

const ProfileStatistics = () => {
  /**
   * Create Hooks for variables related to the statistics
   * of a user such as wins, losses and draws etc.
   */
  const [wins, setWins] = useState(0)
  const [losses, setLosses] = useState(0)
  const [draws, setDraws] = useState(0)
  const [minElo, setMinElo] = useState(0)
  const [minEloDate, setMinEloDate] = useState(0)
  const [maxElo, setMaxElo] = useState(0)
  const [maxEloDate, setMaxEloDate] = useState(0)

  const [currElo, setCurrElo] = useState(0)
  const [avgWon, setAvgWon] = useState(0)
  const [avgLost, setAvgLost] = useState(0)
  const [avgDraw, setAvgDraw] = useState(0)
  const [eloRate, setEloRate] = useState([])
  const [errMsg, setErrMsg] = useState(null)

  /**
   * Obtain the number of wins, losses and draws
   * from an individual player.
   */
  useEffect(() => {
    axios.get("/api/account/retrieve/current/all")
      .then((res) => {

        const _eloRate = res.data.statistics.elo_rate

        setWins(res.data.statistics.wins)
        setLosses(res.data.statistics.loses)
        setDraws(res.data.statistics.draws)
        setAvgWon(res.data.statistics.avg[0])
        setAvgLost(res.data.statistics.avg[1])
        setAvgDraw(res.data.statistics.avg[2])
        setEloRate(_eloRate)

        let min = Number.MAX_VALUE
        let min_date = ""
        let max = 0
        let max_date = ""


        _eloRate.forEach(function (o) {
          min = Math.min(min, o.elo)
          max = Math.max(max, o.elo)

          if (min === o.elo){
            min_date = moment(o.date, 'YYYY.MM.DD').format("DD MMM, YYYY")
          }

          if (max === o.elo){
            max_date = moment(o.date, 'YYYY.MM.DD').format("DD MMM, YYYY")
          }
        });

        setMinEloDate(min_date)
        setMinElo(min)
        setMaxEloDate(max_date)
        setMaxElo(max)
        setCurrElo(_eloRate[_eloRate.length - 1].elo);

      })
      .catch((res) => {
        setErrMsg(res)
      })
    }, []);

  /**
   * Load the right data for the pie on the
   * statistics page.
   */
  let pieData = [
    { x: "wins", y: wins, label: `${wins} wins` },
    { x: "losses", y: losses, label: `${losses} losses` },
    { x: "draws", y: draws, label: `${draws} draws` },
  ]

  return (
    <>
      <h1 className="text-primary mb-4">Elo statistics</h1>
      <div className="elochart mb-4">
        <EloLine data={ eloRate }/>
      </div>
      <Row className="panels justify-content-md-center">
        <Col md={3} className="panel mx-2 mb-2">
          <div className="stats-title">Highest Rating</div>
          <div className="stats-value">{ maxElo }</div>
          <div className="stats-description">{maxEloDate}</div>
        </Col>
        <Col md={3} className="panel mx-2 mb-2">
          <div className="stats-title">Lowest Rating</div>
          <div className="stats-value">{ minElo }</div>
          <div className="stats-description">{minEloDate}</div>
        </Col>
        <Col md={3} className="panel mx-2 mb-2">
          <div className="stats-title">Current Rating</div>
          <div className="stats-value">{ currElo }</div>
          <div className="stats-description"></div>
        </Col>
      </Row>
      <Row className="bottom-info mt-4 justify-content-md-center">
        <Col sm={6}>
          <Row className="opp-stats-title">Average Opponent Rating when I:</Row>
          <Row className="stats-box-row">
            <Col className="avg-box avg-opp-won">
              <div className="stats-title">Won</div>
              <div className="stats-value"> { avgWon } </div>
            </Col>
            <Col className="avg-box avg-opp-lost">
              <div className="stats-title">Lost</div>
              <div className="stats-value">{ avgLost }</div>
            </Col>
            <Col className="avg-box avg-opp-draw">
              <div className="stats-title">Draw</div>
              <div className="stats-value">{ avgDraw }</div>
            </Col>
          </Row>
        </Col>
        <Col sm={5}>
          <ProfilePie data={ pieData }/>
        </Col>
      </Row>
    </>
  )
}

export default ProfileStatistics;
