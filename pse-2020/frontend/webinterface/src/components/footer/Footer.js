import React from 'react';
import { Link } from 'react-router-dom'
import { Container, Row, Col } from 'react-bootstrap';
import gitlablogo from "./gitlablogo.png"

import './Footer.scss';

const Footer = () => (
  <footer>
    <Container className="text-center">
      <Row>
        <Col>
          <h3>Explore</h3>
          <Link to="/leaderboard">Leaderboard</Link>
          <Link to="/blockchain">Matches</Link>
        </Col>
        <Col>
          <h3>Code</h3>
          <a href="https://gitlab-fnwi.uva.nl/11808918/pse-2020-blockchain.git">
            Blockchain
            <img className="ml-2 d-none d-md-inline-block" height={24} src={gitlablogo} alt="" />
          </a>
          <a href="https://gitlab-fnwi.uva.nl/11808918/pse-2020.git">
            Website
            <img className="ml-2 d-none d-md-inline-block" height={24} src={gitlablogo} alt="" />
          </a>
          <a href="https://docs.chesschain.nl">Documentation</a>
        </Col>
        <Col>
          <h3>Contact</h3>
          <a href="mailto:pse.chesschain@gmail.com">Contact Us</a>
        </Col>
        <Col className="d-none d-sm-block text-right">
          <Link to="/">
            <img height="117" width="117" src={process.env.PUBLIC_URL + '/logo192.png'} alt="" />
          </Link>
        </Col>
      </Row>
    </Container>
  </footer>
);

export default Footer;
