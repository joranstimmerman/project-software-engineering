import "./User.scss";
import React from "react";
import { Tab, Nav, Row, Col, Container, Table } from "react-bootstrap";
import MatchesTable from "../../components/table/MatchesTable";

const axios = require("axios");

const User = () => {
  const [data, setData] = React.useState();
  const [errorMsg, setErrorMsg] = React.useState("");

  const userID = window.location.pathname.split("/").pop();

  if (isNaN(userID)) {
    console.log("ERROR: The userId should be a number");
  } else if (!data) {
    axios.get("/api/user/retrieve/" + userID).then((res) => {
      setData(res.data);
    }).catch((error) => {
      if (error.response){
        setErrorMsg(error.response.data.status);
      }
    });
  }

  const DisplayUser = (user) => {
    function display_keys(key) {
      switch (key) {
        case "elo_rating":
          return "Elo Rating";
        default:
          return key;
      }
    }

    return (
      <>
        <h2 className="mt-4">User information</h2>

        <Table className="text-left">
          <tbody>
            {Object.entries(user).map(([key, val]) => (
              key === "blockchain_public_key" || key === "user_id" ? (
                null
                ) : (
                  <tr key={key}>
                    <td className="font-weight-bold">{display_keys(key)}</td>
                    <td>{val}</td>
                  </tr>
                )
              )
            )}
          </tbody>
        </Table>
      </>
    );
  };

  const DisplayLinks = (users) => {
    let accounts = [];

    for (let i = 0; i < users.length; i++) {
      accounts.push(
        <tr key={users[i].website_name}>
          <th className="font-weight-bold">{users[i].player_name}</th>
          <th className="font-weight-bold">{users[i].website_name}</th>
        </tr>
      );
    }

    return (
      <>
        <h2 className="mt-3">Chess Accounts</h2>
        <Table>
          <thead>
            <tr>
              <th>Username</th>
              <th>Website</th>
            </tr>
          </thead>
          <tbody>
            {
              accounts.length ? (
                accounts
              ) : (
                <tr><td colSpan={2} className="text-center"><b>This user doesn't have any chess accounts</b></td></tr>
              )
            }
          </tbody>
        </Table>
      </>
    );
  };

  if (data) {
    return (
      <Container className="py-4 d-flex flex-column justify-content-center">
        <Tab.Container id="left-tabs-example" defaultActiveKey="Profile">
          <Row>
            <Col md={9} className="pr-md-5 order-md-first order-last">
              <h1 className="text-primary">
                {data.website_names.length ? data.website_names[0].player_name : data.username}
              </h1>
              <Tab.Content>
                <Tab.Pane eventKey="Profile">
                  {DisplayUser(data.user)}
                  {DisplayLinks(data.website_names)}
                </Tab.Pane>

                <Tab.Pane eventKey="Matches">
                  <h2 className="mt-4">Matches</h2>
                  <MatchesTable data={[]}></MatchesTable>
                </Tab.Pane>
              </Tab.Content>
            </Col>
            <Col md={3} lg={2} className="mb-3 mb-md-0 offset-lg-1">
              <div className="p-2 bg-light rounded user-nav">
                <Nav variant="pills" className="flex-column">
                  <Nav.Item>
                    <Nav.Link eventKey="Profile">Profile</Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link eventKey="Matches">Matches</Nav.Link>
                  </Nav.Item>
                </Nav>
              </div>
            </Col>
          </Row>
        </Tab.Container>
      </Container>
    );
  } else {
    return (
      <div className="d-flex justify-content-center align-items-center">
        <h1>{errorMsg}</h1>
      </div>
    );
  }
};

export default User;
