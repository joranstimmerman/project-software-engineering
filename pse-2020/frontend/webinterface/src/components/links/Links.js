import React from "react";
import { Table } from "react-bootstrap";
const axios = require("axios");


const DisplayLinks = (users) => {
    const accounts = [];

    for (var i = 0; i < users.length; i++) {
      accounts.push(
        <tr>
          <th className="font-weight-bold ">{users[i].player_name}</th>
          <th className="font-weight-bold">{users[i].website_name}</th>
          <button
            class="btn btn-danger btn-sm rounded-0"
            type="button"
            data-toggle="tooltip"
            data-placement="top"
            onClick={HandleLinkDelete.bind(
              this,
              users[i].player_name,
              users[i].website_name
            )}
            data-original-title="Delete"
          >
            <i class="fa fa-trash"></i>
          </button>
        </tr>
      );
    }

    return (
      <>
        <h1 className="text-primary mb-4">Chess Accounts</h1>
        <Table className="">
          <thead>
            <tr>
              <th key="name">Username</th>
              <th key="black">Website</th>
            </tr>
          </thead>
          <tbody>
            {
              accounts.length ? (
                accounts
              ) : (
                <tr><td colSpan={2} className="text-center"><b>You don't have any chess accounts linked</b></td></tr>
              )
            }
          </tbody>
        </Table>
      </>
    );
  };

  const HandleLinkDelete = (username, website) => {
    var form = {
      username: username,
      website: website,
    };

    axios
      .post("/api/account/unlink", form)
      .then((res) => {
        window.location.reload();
      })
      .catch((error) => {});
  };

  export default DisplayLinks