## Files

### /components

The components of the website are in here. These include things like modals, the header and footer of the website, the user page and the visualisation of the player's statistics.

### /pages

The pages of the website are in here. These include the homepage, blockchain page, leaderboard and upload page.

### App.js

The main javascript file that routes the user to all the pages.

### App.scss

Another global css file which imports _Global.scss.

### _Global.scss

A global css file for the style of the website.

### index.js

Imports App.js.
