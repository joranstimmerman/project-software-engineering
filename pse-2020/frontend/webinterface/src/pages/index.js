import Homepage from './homepage/Homepage';
import Leaderboard from './leaderboard/Leaderboard';
import Matches from './matches/Matches';
import Blockchain from './blockchain/Blockchain';
import Upload from './upload/Upload';
import TermsConditions from './termsConditions/TermsConditions';

import "./index.scss";


/* Export all pages from this folder */
export {
    Homepage,
    Leaderboard,
    Matches,
    Blockchain,
    Upload,
    TermsConditions
}
