import React from "react";
import BlockchainMatchesTable from "../../components/table/BlockchainMatchesTable";

import "./Matches.scss";

const Component = (props) => (
  <>
    <h1 className="text-primary mb-4">Blockchain Matches</h1>
    <BlockchainMatchesTable all_data={props.data} />
  </>
);

export default Component;