import React, { Component } from "react";
import MatchesTable from "../../components/table/MatchesTable";

import "./Matches.scss";

const axios = require("axios");

export default class Matches extends Component {
  constructor(props) {
    super(props);

    this.state = {
      //Shows marker who's info window is active
      matches_data: []
    };
  }


  getMatches() {
    try {
      axios.get("/api/user/retrieve/all_games", {}).then((res) => {
        this.setState({ "matches_data": res.data.pgns });
      });
    } catch (error) {}
  }

  /* Gather the relevant data - live */
  async componentDidMount() {
    await this.getMatches();

  }

  render() {
    return (
      <>
        <h1 className="text-primary mb-4">Submitted Matches</h1>
        <MatchesTable data={this.state.matches_data} all_data={this.props.data} blockchain={false} />
      </>
    );
  }
}
