import React, { Component } from "react";
import { Container } from "react-bootstrap";
import BlockchainMatchesTable from "../../components/table/BlockchainMatchesTable";
import BlockchainMatches from "../matches/BlockchainMatches";

const axios = require("axios");

export default class Search extends Component {
  constructor(props) {
    super(props);

    this.state = {
      /* Shows marker who's info window is active */
      matches_data: {games :[]},
      error: ""
    };
  }

  getMatches() {
    axios.get("/api/matches/history", {}).then((res) => {
      this.setState({ "matches_data": res.data})
    }).catch(error => {
      this.setState({ "error": error.response.data.status})
    });
  }

  /* Gather the relevant data - live */
  async componentDidMount() {
    await this.getMatches();
  }

  render = () => (
    <Container className="content py-4">
      <h1 className="text-primary">Newest matches in the blockchain</h1>
      <BlockchainMatchesTable all_data={this.state.matches_data} blockchain_page={true}></BlockchainMatchesTable>
    </Container>
  )
}
