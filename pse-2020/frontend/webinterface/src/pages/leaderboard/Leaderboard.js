import React, { Component } from 'react';
import { Container } from 'react-bootstrap';
import LeaderboardCom from "../../components/leaderboard/LeaderboardCom";

import "./Leaderboard.scss";

const axios = require("axios");

export default class Leaderboard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      leaderboard_data: [],
    };
  }

  getLeaderboard() {
    try {
      axios.get("/api/matches/leaderboard", {}).then((res) => {
        this.setState({"leaderboard_data": res.data.leaderboard });
      });
    } catch (error) {
        console.log("test2")
    }
  }

  /* Gather the relevant data - live */
  async componentDidMount() {
    await this.getLeaderboard();
  }

  render = () => (
    <Container className="content py-4">
      <h1 className="text-primary">Chesschain Leaderboard</h1>
      <i className="fas fa-award"></i>
      <LeaderboardCom leaderboard={this.state.leaderboard_data} />
    </Container>
  )
}
