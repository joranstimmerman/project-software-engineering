import React, { Component, useMemo } from "react";
import { Button, Modal } from "react-bootstrap";

const PrivateKeyModal = (props) => {
  const [errorMsg, setErrorMsg] = React.useState();

//   const handleShow = () => setShow(true);
//   const handleClose = () => setShow(false);

  const [showForgot, setShowForgot] = React.useState(false);
  const handleShowForgot = () => setShowForgot(true);
  const handleCloseForgot = () => setShowForgot(false);

    function handleSubmit(e) {
        var error = false;
        if (!e.target.Privatekey.value.length) {
          setErrorMsg("Please fill in a private key!");
          error = true;
        } else {
          setErrorMsg();
        }
        props.uploadFn()
    }

  return (
    <Modal
    show={props.show}
    onHide={handleClose}
    backdrop="static"
    keyboard={false}
  >      
  <Modal.Header closeButton>
        <Modal.Title>Verify with private key</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <form onSubmit={handleSubmit}>
          <div className="form-group">
            <div>
                <p>You are about to upload your chessmatch, please provide your <b> private key </b> and verify that this match is legitmate.</p>
            </div>
            <input
              type="text"
              className="form-control"
              id="Privatekey"
              placeholder="Chesschain private key"
            ></input>
            <div className="errorMsg">{errorMsg}</div>
          </div>
          <button type="submit" className="btn btn-primary">
            Upload and sign your match
          </button>
        </form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default PrivateKeyModal;
