import React, { useMemo, useRef } from "react";
import { Button, Modal } from "react-bootstrap";
import { useDropzone } from "react-dropzone";
import "./Upload.scss";

const axios = require("axios");

const activeStyle = {
  borderColor: "#2196f3",
};

const acceptStyle = {
  borderColor: "#00e676",
};

const rejectStyle = {
  borderColor: "#ff1744",
};

const UploadDropzone = () => {
  const {
    acceptedFiles,
    getRootProps,
    getInputProps,
    isDragActive,
    isDragAccept,
    isDragReject,
  } = useDropzone({ accept: ".txt, .pgn" });

  const [show, setShow] = React.useState(false);
  const [status, setStatus] = React.useState("");
  const [errorModMsg, setErrorModMsg] = React.useState();
  const [errorMsg, setErrorMsg] = React.useState();
  const pgn_file = useRef();
  const pk_file = useRef();

  const handleShow = () => {
    var file = acceptedFiles[0];
    pgn_file.current = file;
    setShow(true);
  };

  const handleClose = () => setShow(false);

  const style = useMemo(
    () => ({
      ...(isDragActive ? activeStyle : {}),
      ...(isDragAccept ? acceptStyle : {}),
      ...(isDragReject ? rejectStyle : {}),
    }),
    [isDragActive, isDragReject, isDragAccept]
  );

  function onFileUpload(e) {
    e.preventDefault();
    const formData = new FormData();
    formData.append("file", pgn_file.current);
    formData.append("private_key", pk_file.current);
    try {
      axios
        .post("/api/upload", formData, {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        })
        .then((res) => {
          setStatus("Succesfully added the game!");
          window.location.reload(false);
        })
        .catch((error) => {
          setStatus("");
          if (
            error.response.data.status ===
              "One of the usernames has not yet been linked to an account" ||
            error.response.data.status ===
              "That username has not yet been linked to an account"
          ) {
            setErrorModMsg(error.response.data.status);
          } else {
            handleClose();
            setErrorMsg(error.response.data.status);
          }
        });
    } catch (error) {}
  }

  function handleSubmit(e) {
    e.preventDefault();

    console.log(acceptedFiles);
    console.log("test");

    if (pgn_file.current === acceptedFiles[0]) {
      setErrorModMsg("Please provide private key");
      return;
    } else {
      pk_file.current = acceptedFiles[0];
      setErrorMsg();
      setErrorModMsg();
    }

    onFileUpload(e);
  }

  function handleDelete(e) {
    window.location.reload(false);
  }

  const pgn_file_info = acceptedFiles.map((file) => {
    if (!show) {
      return (
        <div>
          {file.path} - {file.size} bytes
        </div>
      );
    }

    return null;
  });

  const pk_file_info = acceptedFiles.map((file) => {
    if (show && file !== pgn_file.current) {
      return (
        <div>
          {file.path} - {file.size} bytes
        </div>
      );
    }

    return null;
  });

  return (
    <div>
      <div className="dropzone w-100" {...getRootProps({ style })}>
        <input {...getInputProps()} />
        <span>
          Drag 'n' drop your .pgn chessmatch here, or click to select files
        </span>
      </div>
      {pgn_file_info}

      <Button
      className="float-right mt-3"
        disabled={acceptedFiles.length === 0}
        variant="danger"
        onClick={handleDelete}
      >
        <i class="fa fa-trash"></i>
      </Button>
      <Button
        className="float-right mt-3"
        disabled={acceptedFiles.length === 0}
        onClick={handleShow}
      >
        Upload
      </Button>
      <div className="form-group">
        <div className="errorMsg">{errorMsg}</div>
        <div>{status}</div>
      </div>
      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>Verify with private key</Modal.Title>
        </Modal.Header>
        <form onSubmit={handleSubmit}>
          <Modal.Body>
            <div className="form-group">
              <div>
                <p>
                  You are about to upload your chessmatch, please provide your{" "}
                  <b> private key </b> and verify that this match is legitmate.
                </p>
              </div>
              <div
                className="dropzone private-key"
                {...getRootProps({ style })}
              >
                <input {...getInputProps()} />
                <span>Upload your private key</span>
              </div>
              <div className="errorMsg">{errorModMsg}</div>
              <div className="text-success">{status}</div>

              {pk_file_info}
            </div>
          </Modal.Body>
          <Modal.Footer>
            <button type="submit" className="btn btn-primary">
              Upload and sign your match
            </button>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
          </Modal.Footer>
        </form>
      </Modal>
    </div>
  );
};

const Upload = () => (
  <div className="justify-content-md-center align-items-center">
    <h1 className="text-primary mb-4">Upload your chessmatch</h1>
    <UploadDropzone />
  </div>
);

export default Upload;
