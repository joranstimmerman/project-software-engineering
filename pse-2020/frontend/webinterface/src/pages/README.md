## Files

### /blockchain

This page shows the blockchain, consisting of only signed matches. The blockchain is accessed using `axios.get("/api/matches/history" , {}).`

### /homepage

This page is the homepage of the website.

### /leaderboard

This page shows the leaderboard of the top players based on their Elo rating. The leaderboard data is retrieved using the `/api/matches/leaderboard` and uses the leaderbord component in the /components folder.

### /matches

This page contains the matches played, including your own matches, signed and unsigned. From here, the user is able to sign unsigned matches.

### /termsConditions

This page contains the terms and conditions for using the application.

### /upload

This page allows a user to upload .PGN files to import into ChessChain. In this case, if a username is not linked to an account, the upload page will show an error.

### index.js

This file exports all the pages in this folder.
