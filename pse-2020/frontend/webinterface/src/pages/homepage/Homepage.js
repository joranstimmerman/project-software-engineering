import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import image1 from './image1.jpg';
import image2 from './image2.jpg';

import './Homepage.scss';

export default class Homepage extends Component {
  render = () => (
    <>
      <Container fluid className="landing">
        <div className="background"></div>
        <Container>
          <h1>Ranking System using<br />Blockchain Technology</h1>
          <p className="pt-2 pt-md-4">ChessChain enables chess players all over
            the world to build up their ELO rating in a simple, consistent
            and trustworthy method.
          </p>
          <img src={image1} alt="" />
        </Container>
      </Container>
      <Container fluid className="stats py-3 d-flex justify-content-around">
        <div>2020<br />Started</div>
        <div>{this.getMatches()}<br />Matches</div>
        <div>+100<br />Countries</div>
        <div>{this.getUsers()}<br />Users</div>
      </Container>
      <Container className="content">
        <Row className="py-3">
          <Col md={6} className="order-md-last d-flex align-items-center text-justify mt-4 mb-2 mt-md-0 mb-md-0">
            <div>
              With ChessChain you can add chess games played on various
              platforms and have them all build towards <b>one global ELO rating</b>.
              We provide a high level of <b>trustworthiness</b> with our transaction
              system, that requires verification of matches from both parties.
              Create your account now and start climbing the leaderboard!
            </div>
          </Col>
          <Col md={6} className="my-4">
            <img src={image2} alt="" className="w-100" />
          </Col>
        </Row>
      </Container>
    </>
  )

  getMatches = () => {
    return '+40k';
  }

  getUsers = () => {
    return '+20k';
  }
}
