import React from "react";
import {
  Homepage,
  Leaderboard,
  Matches,
  Blockchain,
  Upload,
  TermsConditions,
} from "./pages/index";
import Header from "./components/header/Header";
import Footer from "./components/footer/Footer";
import Profile from "./components/profile/Profile";
import Activation from "./components/activation/Activation";
import User from "./components/user/User";
import Reset from "./components/forgotPassword/Reset";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./App.scss";

const App = () => (
  <Router>
    <Header />

    <Switch>
      <Route path="/upload">
        <Upload />
      </Route>
      <Route path="/blockchain">
        <Blockchain />
      </Route>
      <Route path="/leaderboard">
        <Leaderboard />
      </Route>
      <Route path="/user">
        <User />
      </Route>
      <Route path="/matches">
        <Matches />
      </Route>
      <Route path="/profile">
        <Profile />
      </Route>
      <Route path="/activate">
        <Activation />
      </Route>
      <Route path="/reset">
        <Reset />
      </Route>
      <Route path="/terms-and-conditions">
        <TermsConditions />
      </Route>
      <Route path="/">
        <Homepage />
      </Route>
    </Switch>

    <Footer />
  </Router>
);

export default App;
