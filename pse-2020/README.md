# ChessChain - Team C - PSE 2020

This project consists of a web application where people all over the world can fill in the results of their chess games to get elo ratings.
This web application gives a uniform elo rating, which is obtained by your chess games across different platforms.

This is achieved by using a blockchain. In this blockchain, each of your games is stored as a transaction between two players. The elo rating can be calculated from this.

Furthermore, a user of the web application can gain insight into their current elo rating, climb the high score and gain insight into the strategies of other players. In addition, ChessChain guarantees that all transactions between two players are validated, so that everyone using our blockchain technology has a guarantee that no fraud has been committed.

## Documentation

Please check out our [documentation](docs.chesschain.nl) for information on how to use and install the code of this codebase.

## Acknowledgments

* Thanks to Ana Oprescu for designing the PSE course
* Thanks to Jelle van Dijk for guiding the team and overseeing the progress.
* Thanks to Stephen Swatman for creating the "Competitive Game Ranking using Blockchain" case (as specified on [canvas](https://canvas.uva.nl/courses/10521/pages/competitive-game-ranking-using-blockchain-team-c?module_item_id=504378)).

## Division of tasks

In this section, we'll go into great detail what every team memberaccomplished. This is divided into the different teams that wereinitially made for this project. Some people occur several times becausethey had a different role in addition to their role as a developer.

### Backend

The responsibility of the backend is to ensure that all data andinteractions required to fulfill the customer journey through ourapplication are delivered. For this, we've set up a team of experiencedmembers that have this knowledge.

**Michelle Schrijnemaekers:**

* PGN to JSON conversion
* Documentation for most backend files
* Algorithm that validates chess moves
* Writing basic API calls
* Code reviews

**Cas Steigstra:**

* Writing several API's:
  * Signup
  * Modify account data
  * Account data retrieval
  * Leaderboard retrieval
  * Signing games
  * Signed and unsigned game retrieval
  * Upload process
  * Creating and verifying public and private keys
  * Linking and unlink (website, username) to account
  * Adding games to the blockchain
  * Proper blockchain functions necessary to calculate ELO.
* Database development:
  * Correct (fix) the database model
  * Setup a proper proxy such that CORS errors are fixed and the login actually works.
* Testing:
  * Create a testing environment with pytest
  * Create pytest files such that the backend API's have 100% test coverage.
* Help with any errors or questions that arise when the frontend requests data from the backend.
* Familiarise with Flask, Flask_SQLAlchemy, Docker, PostgreSQL, Pytest, React Axios.

**Joris Smeets:**

* Database development:
  * Initial design of the database
  * Develop and setup the database
* Writing several API's:
  * Signing games
  * Signed and unsigned game retrieval
  * Upload process
  * Creating and verifying public and private keys
  * Adding games to the blockchain
  * Proper blockchain functions necessary to calculate ELO.
* Wrote an algorithm that calculates the elo of two opponents based on their previous elo and games played.

**Jurre Brandsen:**

* Upload process:
  * Wrote a script that converts a PGN to a JSON file.
    * Valid move validation
    * Determine who won
    * Parse website names
    * Parse all moves made that lead to victory
  * Incorporated the calculation of elo into this script
  * Rewriting the scripts to API calls
  * Revamp the elo calculation script
* Sphinx Documentation set up
  * Configuration
  * .rst files
* Documentation / writing comments

**Joran Timmerman:**

* Set up backend:
  * Docker containers via docker compose
  * File structure
  * Database connection via Flask with postgres / adminer
  * Fixing errors and small things so that everything that is merged works together
* Developing functionalities in the backend (API's)
  * Ability to send emails
  * account activation
  * Forgot your password
  * private key
  * Mail templates for the previous three API's
  * Set up test environment with pytest
  * Login
  * Connection to the blockchain and functions to execute commands
  * Database migrations
  * Obtain data for profile page and a user
  * Account-based statistics are games
  * Obtaining all matches from the blockchain
  * Rejecting a played game
  * Adding games to the blockchain

### Blockchain

There is currently no reliable, global system for keeping track of the ratings (such as an Elo rating)of chess or go players.
Blockchain provides a solution to this by providing a global and tamper-proof ledger of games played to keep track of the ratings of players.

**Lennart de Boer:**

* Blockchain Development:
  * Research
  * Setting up a framework for interaction with the blockchain
  * Participated in the development for peer-2-peer communication between nodes
  * Research for threads in python to apply this in the peer-2-peer network
  * Working on a server that handles API calls on the basis of sockets
  * Lock system of nodes, to make the chain more secure.
* Integration with backend:
  * Merge peer-2-peer network and server with communication back-end
* Docker:
  * In-depth / research into docker (composition)
  * Set up docker for blockchain
  * Research into setting up socket communication within a docker compose project
  * Establish communication between two docker compose projects
* Unit tests
* Demo for blockchain

**Jari Andersen:**

* Blockchain development:
  * Research
  * Blockchain API
    * The blockchain api is the backbone of the assignment (has all the 'database-like' functionalities of a database and all integrity functionalities of a blockchain)
  * Investigate locks for node communication (block pushes in multiple agent systems, when to push a block and when to wait).
  * Made a server that handles received messages.
  * Helped with the design of nodes (nodes is the distributed part of the blockchain).
  * Admin functionality
  * Storing live data in the chain
* Documentation:
  * Documentation for automatic documentation added to blockchain API and server.
  * Canvas pages written about APIs and requests to the server.
* Integration with backend:
  * Explained to Joran how the blockchain works and how to apply it in the backend.
  * Making sure that the backend and the blockchain can communicate
* Docker:
  * Ensured that 2 dockers on 1 computer can communicate with each other (so that we separate blockchain from backend and frontend).
  * Made sure we can let dockers communicate over different networks
    * done by port forwarding
* Unit tests

**Joris op ten Berg:**

* Peer-to-Peer development:
  * Research
  * Socket development
  * Network flooding
  * Security features:
    * Locks
    * Communication verification
* Docker:
  * Initial setup
  * Testing if the docker works and can communicate
* Documentation
* Unit tests
* development of node communication between multiple nodes

**Stan van der Kroon:**

* Blockchain development:
  * Research
  * Signing and verification of transactions
* Peer-to-Peer network development:
  * Ability to send messages between peers
  * Ability to request a lock
  * Ability to request a block
* Docker:
  * Initial setup
  * Testing if the docker works and can communicate
* Documentation
* Unit tests

**Fabian van Stijn:**

* Blockchain development:
  * Research
  * Verification of transactions
  * Encryption
* Documentation
* Unit tests

**Roberto Volpe Garcia:**

* Development of Blockchain data structure (peer programming)
* Research on node communication
* Writing README's for all directories of the blockchain repository.

### Frontend

The frontend provides a clean UI for a user to navigate the application.
In addition, this framework needs to be able to communicate with the backend to retrieve and send data.

**Niels Keinke:**

* Login system
* The framework of the web pages
* The first setup of the profile page
* The user page where any user can be viewed
* Account activation
* Password reset
* The footer
* Frontend the system to add chess accounts to the profile
* Aided in the design of the front end

**Silver Lee-A-Fong:**

* Design of pages:
  * Homepage
  * Profile
  * Statistics
  * Leaderboard
  * Matches
* Development:
  * Matches
  * Leaderboard
  * Statistics

**Tijs Teulings:**

* Matches page (both blockchain and signed)
* Upload page
* Logout
* Update profile page
* Link login and account creation with backend
* Link leaderboard with backend
* Tweaks to Navbar

**Yasser Kalloe:**

* Documentation of the frontend
* Registration page

**Joran Timmerman:**

* Frontend Components:
  * Forgot your password
  * Activation account
  * Statistics

### Team leads

The responsibility of the team leads is to ensure that the goal that was set is achieved in the timeframe.
For this, methods like SCRUM and agile are used. This team is also responsible for the communication between the teams.

**Jurre Brandsen (Projectmanager):**

* SCRUMmaster:
  * Learn everyone that is not familiar with SCRUM SCRUM / agile (user stories, poker, acceptance criteria, todo / doing / done)
  * Setting up the SCRUM environment for each team.
  * Organisation around the daily standups. (Every team and all team leads came together daily)
  * Prepare and supervise retrospective
  * Come up with user stories
  * Roadmapping of the various sprints
  * Sprint planning
* Making presentations of the Friday morning demos
* Guiding the team leads with their tasks as lead if they were inexperienced.
* Prepare plenary meetings
* Maintain team page on Canvas and written pages

**Fabian van Stijn / Joris op ten berg (blockchain), Michelle Schrijnemaekers (backend), Tijs Teulings (frontend):**

* Plan which user stories should be prioritized per sprint
* Organize Trello:
  * Track user stories
  * Assign team members
  * Monitor time per ticket
  * Writing the user stories
  * Assign acceptance criteria to each of the user stories
* Daily stand-up with development team
* Presenting the demo on Fridays
* Write recaps at the end of a sprint.

### Tech leads

The tech leads ensure that the technology that is used to build this application is state-of-the-art.
They also oversee the whole technical process by using issues and merge requests in gitlab.

**Joran Timmerman (backend):**

* Point of contact for technical questions about backend
* Technical communication between the different teams
* Peer programming / teaching backend skills
* Code polishing / making sure everyone complies with the coding standard
* Git master of the backend
* Code reviews

**Joris op ten Berg / Stan van der Kroon (blockchain):**

* Point of contact for technical questions regarding the Blockchain
* Technical communication between the different teams
* Peer programming / teaching blockchain skills
* Git master of the blockchain
* Code polishing / making sure everyone complies with the coding standard
* Principal investigator of blockchain technology
* Setting up the blockchain structure for communication with the Blockchain
* Code reviews

**Dylan Potharst (frontend):**

* Point of contact for technical questions regarding frontend
* Technical communication between the different teams
* Peer programming / teaching frontend skills
* Setting up the React framework
* Setting up and maintenance of the development and production servers
* Set up docker for the frontend and documentation
* Code polishing / making sure everyone complies with the coding standard
* Git master of the frontend
* Code reviews
