Setting up the database
=======================

To make adjustments to the database make sure the images are running. Using
flask and docker-compose database operations can be executing using the following
command:

.. code-block:: bash

    docker-compose exec backend flask db ..


When your database is out of sync with the one on master execute to get the
adjustments made by others.

.. code-block:: bash

    docker-compose exec backend flask db upgrade

Because of recent changes in the way we did migrations your local database may
say it is missing some revisions (a migration file). To fix this use the following
commands. Make sure you have the right migration files on your local branch
(thus the same as in master). It will delete all database entries. In order execute:


.. code-block:: bash

    docker-compose down # This will remove the container, deleting all tables in the database

.. code-block:: bash

    docker-compose build

.. code-block:: bash

    docker-compose up

.. code-block:: bash

    docker-compose exec backend flask db upgrade
