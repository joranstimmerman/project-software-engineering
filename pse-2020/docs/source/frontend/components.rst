Components
===========

/activation
-----------

The message, placed inside a modal, to tell the user if their account has been activated successfully,
in which case their private key will be sent to their email address. To check if the account has been successfully activated,
the API in `/api/account/activate` is called with the token from the email.

/addLink
---------

A form to allow the user to link an external website to their account.
This calls `/api/account/link` with the form to register.
For now, users have to fill in the website and their username on that website,
which will be displayed on their profile.

/create
-------

This component is a form which allows the user to create an account.
It uses the `/api/account/signup` to register the account and send a verification email to the new user.

/footer
--------

The footer of the webpage.
Links included here allow the user to navigate the site,
check out the GitLab repo and contact the ChessChain team.

/forgotPassword
----------------

After the user requested a password reset,
they will have to click a link.
That link will open this form where the user can pick a new password.
This password is posted along with the mail token to `/api/account/reset/password`.

/header
-------

The header of the website, which allows for site navigation.
Once the user has logged in, the header will show their username and Elo rating,
along with the logout button.

/leaderboard
-------------

The leaderboard of the top chess players on ChessChain.
Clicking a name in the leaderboard will show that user's page.

/login
-------

The login form, which is placed inside a modal, allows the user to log into their account.
This is done using `/api/login`.
If the user needs to reset their password,
a different modal will appear with a password reset form which is posted to `/api/account/reset/password`, sending a confirmation email to the user.

/logout
--------

The logout button, which also places the user back on the homepage if they log out from their profile page.

/profile
---------

The profile page, where a logged in user can see their data,
as well as modify their email and username.
This page also allows the user to upload their PGN data,
see their own matches, matches to be verified, statistics, and add a website link, which opens the modal from the `/addLink` directory.
Removal of external websites is also possible through this page.

/statistics
------------

Visualisations of the logged in player's statistics,
viewable from the profile page.
This consists of a line graph to display the history of the players Elo,
as well as their highest, lowest and current elo, as well as a pie chart showing the player's win/loss/draw ratio.

/table
-------

The table showing the matches consisting of the players, result and Elo changes.
From here, the user can sign not yet signed matches, reject said matches or upload their own .PGN file.
Signing a match and uploading a .PGN file require the user's private key.
When the .PGN file is uploaded, it, along with the private key, is posted to `/api/upload`, and when a match is signed,
the private key and game ID are posted to /api/user/sign.

/user
------

This displays the details about a user, including their public key, Elo rating, and external chess accounts.
