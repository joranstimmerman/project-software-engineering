The frontend
============

This is the frontend of the ChessChain application.
The frontend directory consists of the user interface.
It is powered by React Bootstrap and Axios and uses pages and components to construct the web application.

User functionalities are based on a predefined customer journey through the application.
These are in no particular order:

* Create and confirm an account, which also gives the user a private key
* Login and reset a password
* Upload chess match data from a .PGN file
* Sign and reject other people's matches
* Display Elo changes for each match
* See one's own statistics
* See the ChessChain leaderboard
* Link external chess accounts

Installation
=============

To set-up all the dependencies purely to develop on the frontend, run the following in the project directory:

``npm install``

Available Scripts
------------------

In the project directory, you can run:

``npm start``

Runs the app in the development mode.
Open localhost_ to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

``npm test``

Launches the test runner in the interactive watch mode.
See the section about tests_ for more information.

``npm run build``

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.
Your app is ready to be deployed!

See the section about deployment_ for more information.

``npm run eject``

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

Learn More
-----------

You can learn more in the Create React App documentation in order to get started_

To learn React, check out the React documentation_

File structure of Frontend
=================================

/components
------------

The components of the website are in here. These include things like modals, the header and footer of the website, the user page and the visualisation of the player's statistics.

.. toctree::
   :maxdepth: 1
   :caption: File structure of componets:

   components.rst


/pages
------

The pages of the website are in here. These include the homepage, blockchain page, leaderboard and upload page.

.. toctree::
   :maxdepth: 1
   :caption: File structure of pages:

   pages.rst

App.js
-------

The main javascript file that routes the user to all the pages.

App.scss
---------

Another global css file which imports _Global.scss.

_Global.scss
-------------

A global css file for the style of the website.

index.js
---------

Imports App.js.

.. _localhost: http://localhost:3000
.. _tests: https://facebook.github.io/create-react-app/docs/running-tests
.. _deployment: https://facebook.github.io/create-react-app/docs/deployment
.. _started: https://facebook.github.io/create-react-app/docs/getting-started
.. _documentation: https://reactjs.org/
