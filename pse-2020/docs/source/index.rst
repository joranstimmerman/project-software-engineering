Welcome to ChessChain's documentation!
=======================================

This project consists of a web application where people all over the world can fill in the results of their chess games to get elo rating.
This web application gives a uniform elo rating, which is obtained by your chess games across different platforms.
This is achieved by using a blockchain. In this blockchain, each of your games is stored as a transaction between two players.
The elo rating can be calculated from this.

Furthermore, a user of the web application can gain insight into their current elo rating,
climb the high score and gain insight into the strategies of other players.

In addition, **ChessChain** guarantees that all transactions between two players are validated,
so that everyone using our blockchain technology has a guarantee that no fraud has been committed.

Installation
============
Clone or download this repository to a folder of your preference.
Then, follow the instructions in Docker_ and Database_

Contents
========

.. image:: schematic-overview.png
  :alt: Schematic overview of the whole application
  :width: 900

The ChessChain application consists of multiple parts. Because ChessChain
is a web application, there is a frontend and a backend.
These two components communicate with eachother using API calls.
Furthermore, the backend and the blockchain also communicate with eachother
using an blockchain api that works with commando's.
Data from the Blockchain is periodically cached in the database of the web application.
The complete contents is hosted on a server which has multiple dockers running.

.. toctree::
   :maxdepth: 3
   :caption: Backend:

   backend/backend.rst

.. toctree::
   :maxdepth: 3
   :caption: Frontend:

   frontend/frontend.rst

.. toctree::
   :maxdepth: 3
   :caption: Blockchain:

   blockchain/blockchain.rst

.. toctree::
   :maxdepth: 3
   :caption: Docker:

   docker/docker.rst
.. _Docker:

.. toctree::
   :maxdepth: 3
   :caption: Database:

   database/database.rst
.. _Database:


.. toctree::
   :maxdepth: 3
   :caption: Tests:

   tests/pytest.rst

Authors
=======
**J. Brandsen** - *Project Manager*

    **M. Schrijnemaekers** - *Backend, Teamlead*

      **J. Timmerman** - *Techlead*

      **J. Smeets** -*Developer*

      **C. Steigstra** - *Developer*

    **T. Teulings** - *Frontend, Teamlead*

      **D. Potharst** - *Techlead*

      **Y. Kalloe** - *Developer*

      **N. Keinke** - *Developer*

      **S. Lee-A-Fong** - *Developer*

    **F. van Stijn** - *Blockchain, Teamlead*

      **J. op ten Berg** - *Techlead*

      **J. Andersen** - *Developer*

      **L. de Boer** - *Developer*

      **S. van der Kroon** - *Developer*

      **R. Volpe Garcia** - *Developer*

Acknowledgments
===============

* Thanks to Ana Oprescu for designing the PSE course
* Thanks to Jelle van Dijk for guiding the team and overseeing the progress.
* Thanks to Stephen Swatman for creating the "Competitive Game Ranking using Blockchain" case (as specified on canvas_ ).


.. _canvas: https://canvas.uva.nl/courses/10521/pages/competitive-game-ranking-using-blockchain-team-c?module_item_id=504378
