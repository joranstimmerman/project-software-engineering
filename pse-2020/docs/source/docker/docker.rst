Installation guide
==================

The program uses docker for encapsulating the application. Assuring Consistency
across multiple development environments.

The Docker containers can be setup using docker-compose. Use the following
commands.

To build the images:

.. code-block:: bash

    docker-compose build


To run the image in a container:

.. code-block:: bash

    docker-compose up
