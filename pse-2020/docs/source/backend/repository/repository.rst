repository
==========

The 'repositories' directory contains repositories paired with different models,
mostly to perform operations on certain database tables.
The following repositories have been implemented:

* **account_repo**: operations on 'account' database table, to retreive account data
* **association_repo**: operations on 'association' database table, to retrieve a user's games
* **games_repo**: operations on 'games' database table, to retrieve a game's details (PGN)
* **user_repo**: operations on 'user' database table, to retrieve blockchain user's data
* **websites_repo**: operations on 'websites' database table, to retreive a user's third party account data

**Account_repo:**
-----------------

.. automodule:: app.repository.account_repo
    :members:
    :undoc-members:
    :show-inheritance:

**Association_repo:**
---------------------

.. automodule:: app.repository.association_repo
    :members:
    :undoc-members:
    :show-inheritance:

**Games_repo:**
-----------------

.. automodule:: app.repository.games_repo
    :members:
    :undoc-members:
    :show-inheritance:

**User_repo:**
-----------------

.. automodule:: app.repository.user_repo
    :members:
    :undoc-members:
    :show-inheritance:

**Websites_repo:**
-------------------

.. automodule:: app.repository.websites_repo
    :members:
    :undoc-members:
    :show-inheritance:
