Documentation of models
=======================

The 'models' directory contains models that describe the tables and structure of our web application's database,
and other classes/instances that are needed to implement our back end.
The following models have been implemented:

* **account**: database table 'accounts' and some accompanying functions
* **association**: database association table that connects the 'users' and 'games' tables to keep track of a blockchain user's (signed) games
* **blockchain**: the blockchain connection that is needed to communicate with the blockchain via the back end
* **database**: database set-up
* **games**: database table 'games' and some accompanying functions
* **login**: account loader
* **mail**: mail configuration
* **user**: (blockchain) user table and some accompanying functions
* **websites**: website table and some accompanying functions, keeps track of third party website accounts that have been linked to ChessChain accounts

**Account:**
-----------------

.. automodule:: app.models.account
    :members:
    :undoc-members:
    :show-inheritance:

**Association:**
-----------------

.. automodule:: app.models.association
    :members:
    :undoc-members:
    :show-inheritance:


**Blockchain:**
-----------------

.. automodule:: app.models.blockchain
    :members:
    :undoc-members:
    :show-inheritance:


**Database:**
-----------------

.. automodule:: app.models.database
    :members:
    :undoc-members:
    :show-inheritance:


**Games:**
-----------------

.. automodule:: app.models.games
    :members:
    :undoc-members:
    :show-inheritance:

**Login:**
-----------------

.. automodule:: app.models.login
    :members:
    :undoc-members:
    :show-inheritance:

**Mail:**
-----------------

.. automodule:: app.models.mail
    :members:
    :undoc-members:
    :show-inheritance:

**User:**
-----------------

.. automodule:: app.models.user
    :members:
    :undoc-members:
    :show-inheritance:

**Websites:**
-----------------

.. automodule:: app.models.websites
    :members:
    :undoc-members:
    :show-inheritance:
