Backend functionalities
===============================

The 'backend' directory of the project's codebase consists of the various requirements
and functionality that is needed to implement the back end of Chesschain.
The back end contains the following components:

* **app**: implements all functionality for the web application.
* **docker**: contains the dockerfile and dockerignore that is used to build images
* **migrations**: contains migration files that describe database configurations and
  files to generate and handle these files
* **tests**: contains testing scripts for back end functionality

The backend uses flask as a framework and a postgres database for storing the required data

API:
----
API's that handle calls and requests that are received from our front end.

.. toctree::
   :maxdepth: 2

   api/api.rst

Functionalities:
----------------
Contains scripts with functions for various purposes that extend simple database operations.

.. toctree::
   :maxdepth: 2

   functionalities/functionalities.rst

Models:
-------
Database Models that define our back-end database structure

.. toctree::
   :maxdepth: 2

   models/models.rst

Repository:
-----------
Repositories for various operations on the different database tables

.. toctree::
   :maxdepth: 2

   repository/repository.rst
