Testing
=======

The backend and the blockchain are provided with test coverage. In the backend
the various API's are tested and in the blockchain the functionalities of the
blockchain and it's various nodes are tested.

For the tests the framework pytest is used. This allows us to make use of
predefined fixtures to create test resources. And allows us to execute the
tests easily.

backend
-------

The backend uses a test database to perform its tests. It contains the same
tables as the original production database.

Running tests
=============

To run the tests the docker containers should be active, so run:

.. code-block:: bash

    docker-compose up

If the docker images aren't build yet, execute the build command first:

.. code-block:: bash

    docker-compose build

After the tests can be executed by simply running the following commands, make
sure you are in the repositoy where u want to execute the tests.

For the backend:

.. code-block:: bash

    docker-compose exec backend pytest

For the blockchain:

.. code-block:: bash

    docker-compose exec blockchain pytest