Blockchain
==========

The websites where the chess games are played are like true nodes in the blockchain.
Each node keeps a ledger (a kind of database that is the same everywhere) and passes on the games (transactions of 2 players).
The nodes are interconnected in a network and therefore when one node makes a transaction to another node, it ends up on every node with every ledger.

.. image:: blockchain.png
  :alt: This image shows a visual guide on how the blockchain is set up.

This image shows a visual guide on how the blockchain is set up.

.. image:: blockchain2.jpeg
  :alt: This image shows a visual guide on how the blockchain connected to the rest of the application.

This image shows a visual guide on how the blockchain connected to the rest of the application.

Basic user accounts on the blockchain
-------------------------------------
To create a user account we need to generate a unique random public and private key which are used with hashing and encryption to send something on the blockchain.
By Hashing the data and signing this with the private key we create a digital signature which is verifiable by the receiver.
We use hashing to be able to see if any changes in the data or the sender have been made while it was sent.

Basic verification of transactions
----------------------------------
A transaction needs te be verified to be added to a block.
A user generates a private and publick key on registartion of the account.
The private and public key are a pair.
This verification can be done with a signature.
The signature is encrypted using the private key of the user who is sending the transaction.
This signature can be decrypted by using the public key of the user that send the transaction.

Peer-to-peer communication in python
------------------------------------
For peer-to-peer communication between the nodes that participate in our blockchain, we used the p2pnetwork module by Maurice Snoeren.
This module implements a number of low-level socket functions and can be built upon easily, making it a good fit for our project.

Ensuring the validity of Blockchain
-----------------------------------
A block consists out of 2 important factors:

#. the data
#. the previous hash.

The data stores the given data and the previous hash shows the previous block but with all of its contents (the data and previous hash) hashed.
Lets say that we have 3 blocks. Block 3 points to block 2, block 2 points to block 1, etc.
If someone where to alter the data in block 2 we can check the validity of the chain by using the make_hash function in the block class.
We do that in the following manner:

#. take previous_hash out of block3
#. use `make_hash` in block2
#. compare the result with `previous_hash` and we see that there is a difference.

We do not want the block_ID (hash of block) as a field in the block.
This doesn't work because we assign the hash to the block_ID after creating the hash.
If we where to make a new hash of the block now we would have a different block_ID value so we also will get a different hash if we where to use `make_hash(self)` again.
This is why we use the make_hash function instead.

Backend blockchain communication commands
-----------------------------------------

================================= ================================== ========
Command                           Purpose                            Response
================================= ================================== ========
`get_block <block_hash>`          Grab a block with a hash or index  The data of the block or string: “block not found” if the block was not found.
`fetch_last_block`                Take the last block from the chain The data of the block or string: “last block not found” if the block was not found.
`check_chain`                     Check the integrity of the entire  The string: "chain correct" if the chain is correct, otherwise the string: "chain incorrect".
                                  chain by `previous_hash` and
                                  `make_hash()` in the Blockchain
                                  class and Block class.
`add_block <data>`                Add a block to the queue and let   The string: “block added” if the block has been successfully added, otherwise the srting: “block NOT added”
                                  the node communication determine
                                  when we add it to the chain.
                                  Give data as a dictionary.
`save_chain`                      Stores the chain in a file         The string: “save successful” if the chain has been successfully saved, otherwise the string: “save failed”
                                  called blockchain.json.
`find_games <parameter> <value>`  Finds all games with certain       Returns all the games
                                  parameter value pair
                                  (key value in dict).
`find_blocks <parameter> <value>` Finds all blocks with certain      Returns the blocks with at least 1 game with the parameter with the value "value".
                                  parameter value pair
                                  (key value in dict).
================================= ================================== ========

Blockchain Documentation
========================
.. toctree::
   :maxdepth: 1

   overview.rst


FAQ
===

How to add data to a block?
----------------------------
By using the data section in declaring a Block.

.. code-block:: Python

    Block(<INSERT JSON FILE HERE>, previous_hash)

How do we extract all the blocks where the winner was userwin?
--------------------------------------------------------------
.. code-block:: Python

    foundBlocks = new_chain.find_blocks("winner", "userwin")

This will return the entire block where a game contains userwin as the winner.
If you are only interested in the specific games where userwin is the winner,
you'll have to use the function `find_games()`:

.. code-block:: Python

    foundGames = new_chain.find_games("winner", "userwin")

This will only return the first game from our example instead of the whole block.

How do we retrieve data out of the blockchain?
----------------------------------------------
By using the `find_blocks function` in the Blockchain class.

We take the following json data as an example.

.. code-block:: JSON

    {
    "blocks": [
        {
        "data": [
                "genesis_block"
            ],
            "previous_hash": "10"
        },
        {
            "data": [
                {
                    "event": "Live Chess",
                    "site": "Chess.com",
                    "date": "2020.06.04",
                    "username_1": "eljero66",
                    "username_2": "ridderdavid",
                    "result": "0-1",
                    "elo_score_contestant_1": 550,
                    "elo_score_contestant_2": 650,
                    "time_control": "300+5",
                    "move_list": [
                        "e4",
                        "e5",
                        "Nc3",
                        "Nc6",
                        "Nf3",
                        "Bc5",
                        "Bc4",
                        "d6",
                        "d3",
                        "Nf6",
                        "O-O",
                        "Bg4",
                        "Bg5",
                        "h6",
                        "Bh4",
                        "g5",
                        "h3",
                        "Bxh3",
                        "gxh3",
                        "gxh4",
                        "Nxh4",
                        "Qd7",
                        "Nd5",
                        "Nxd5",
                        "exd5",
                        "Qxh3",
                        "Qe2",
                        "Rg8+",
                        "Ng2",
                        "Rxg2#"
                    ],
                    "contestant_1_public_key": "-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDEPqNaRLN793u+x30L1vUIRSY+\n3Mrp8vYqprwIsaKzYQDgPIGSgK9bpKI6iaoKPb51msYJ8Hz2TpTmNezMufERjpkK\n/2jNmeCqTRe/fZOfeNCs+sqOrZ39KKqLvd9bw/0EK2iP6so9Z83PTIt+iGQal/Jy\nE/HzIcy6YEr/GC5/dwIDAQAB\n-----END PUBLIC KEY-----",
                    "contestant_2_public_key": "-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDRDgNW9zEsw/fCNUqBWsk5RJh7\n/6lbSlxc1VVqbjWHNuxZWxgd7WEFiSP9LtoK9UFt8M/zPwipOLoWP/nDcZKdSTQI\nGfO9pCeVw8XxfZ2hWk3a9XcD4cRVryysD/37d7Lxc51pafKjZViZVQ+vz22k9ocq\na0Do/nHuKvbi5T898wIDAQAB\n-----END PUBLIC KEY-----",
                    "number_of_games_contestant_1": 0,
                    "number_of_games_contestant_2": 0
                },
                {
                    "event": "Live Chess",
                    "site": "Chess.com",
                    "date": "2020.06.04",
                    "username_1": "eljero66",
                    "username_2": "ridderdavid",
                    "result": "0-1",
                    "elo_score_contestant_1": 550,
                    "elo_score_contestant_2": 650,
                    "time_control": "300+5",
                    "move_list": [
                        "e4",
                        "e5",
                        "Nc3",
                        "Nc6",
                        "Nf3",
                        "Bc5",
                        "Bc4",
                        "d6",
                        "d3",
                        "Nf6",
                        "O-O",
                        "Bg4",
                        "Bg5",
                        "h6",
                        "Bh4",
                        "g5",
                        "h3",
                        "Bxh3",
                        "gxh3",
                        "gxh4",
                        "Nxh4",
                        "Qd7",
                        "Nd5",
                        "Nxd5",
                        "exd5",
                        "Qxh3",
                        "Qe2",
                        "Rg8+",
                        "Ng2",
                        "Rxg2#"
                    ],
                    "contestant_1_public_key": "-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDEPqNaRLN793u+x30L1vUIRSY+\n3Mrp8vYqprwIsaKzYQDgPIGSgK9bpKI6iaoKPb51msYJ8Hz2TpTmNezMufERjpkK\n/2jNmeCqTRe/fZOfeNCs+sqOrZ39KKqLvd9bw/0EK2iP6so9Z83PTIt+iGQal/Jy\nE/HzIcy6YEr/GC5/dwIDAQAB\n-----END PUBLIC KEY-----",
                    "contestant_2_public_key": "-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDRDgNW9zEsw/fCNUqBWsk5RJh7\n/6lbSlxc1VVqbjWHNuxZWxgd7WEFiSP9LtoK9UFt8M/zPwipOLoWP/nDcZKdSTQI\nGfO9pCeVw8XxfZ2hWk3a9XcD4cRVryysD/37d7Lxc51pafKjZViZVQ+vz22k9ocq\na0Do/nHuKvbi5T898wIDAQAB\n-----END PUBLIC KEY-----",
                    "number_of_games_contestant_1": 0,
                    "number_of_games_contestant_2": 0
                }
            ],
            "previous_hash": "8d144cab5deb1afb568bf86fe7203bdea6d3c0c51c013c51f2cb2d75601cf73a"
        }
    ]}
