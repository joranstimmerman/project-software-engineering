The Blockchain
==============

Node
=====

The processing of the local node blockchain is done in here.

.. toctree::
   :maxdepth: 1

   blockchain/node.rst

p2pnetwork
==========

This folder contains Maurice Snoeren's p2pnetwork module, which forms the base for our blockchain p2p network implementation.

.. toctree::
   :maxdepth: 1

   blockchain/p2pnetwork.rst
