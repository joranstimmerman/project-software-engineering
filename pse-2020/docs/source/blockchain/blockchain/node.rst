Documentation of the Blockchain nodes
=====================================

The processing of the local node blockchain is done in here.
For this, the following files are used:

* **blockchain.json**: The blockchain.json file is an example of how a blockchain is stored.
* **blockchain.py**: The blockchain.py is the actual blockchain data structure. It can put arbitrary data in a chain and parse it in diffrent ways.
* **blocknode.py**: This file contains the BlockNode class, which handles peer-to-peer communication between nodes running the blockchain. This class extends the SecureNode class found in the p2pnetwork module.
* **local_node.py**: This file contains the Server class, which communicates with the website back-end and calls functions from the BlockNode class.
* **node_test.py**: This is a test client to 'talk' to the server.
* **rsa.py**: The rsa.py makes it possible to sign matches to be sure the games are actually played by the person with the private key.


**Blockchain:**
-----------------

.. automodule:: blockchain_app.node.blockchain
    :members:
    :undoc-members:
    :show-inheritance:

**Blocknode:**
-----------------

.. automodule:: blockchain_app.node.blocknode
    :members:
    :undoc-members:
    :show-inheritance:

**Local node:**
-----------------

.. automodule:: blockchain_app.node.local_node
    :members:
    :undoc-members:
    :show-inheritance:

**Local test:**
-----------------

.. automodule:: blockchain_app.node.node_test
    :members:
    :undoc-members:
    :show-inheritance:
