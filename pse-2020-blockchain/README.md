## DISCLAIMER:
Due to research, pair programming or meetings, the number of commits to this
repository might not be representative of each members contributions.

## Files

### /blockchain

The blockchain directory is where the blockchain itself and the connections in between nodes are implemented.

### docker-compose.yml

This is the setup for the blockchain and chaintest dockers. Thanks to this docker-compose file, it is possible for the blockchain and the chaintest to communicate.

## Installation

Clone or download this repository to a folder of your preference. Make sure the requirements below are installed to be able to run the program.

### Requirements

To run this program, you need the following:

* [Docker](https://docs.docker.com/get-docker/)

### Build

The program uses docker for encapsulating the application. Assuring Consistency
across multiple development environments.

The Docker containers can be setup using the setup script or with docker-compose. Use the following
commands.

#### Automatic setup

First, the right permissions have to be set:
```bash
chmod +x startup.sh
```
Start the setup with:
```bash
./startup.sh
```

#### Manual setup

To create docker networks:

```bash
docker network create -d bridge blockchain-network
docker network create -d bridge node-network
```

To build the images:

```bash
docker-compose build
```

To run the image in a container:

```bash
docker-compose up
```

### Contributing

This project is not open to contribution at the moment, as it is part of a university course assignment.

## License

This project is licensed under the MIT License.

## Acknowledgments

* Thanks to Ana Oprescu for designing the PSE course
* Thanks to Jelle van Dijk for guiding the team and overseeing the progress.
* Thanks to Stephen Swatman for creating the "Competitive Game Ranking using Blockchain" case (as specified on [canvas](https://canvas.uva.nl/courses/10521/pages/competitive-game-ranking-using-blockchain-team-c?module_item_id=504378)).
