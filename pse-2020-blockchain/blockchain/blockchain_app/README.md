## Files

### /node

The processing of the local node blockchain is done in here.

### /p2pnetwork

This folder contains Maurice Snoeren's p2pnetwork module, which forms the base for our blockchain p2p network implementation.

## Installation

Clone or download this repository to a folder of your preference. Make sure the requirements below are installed to be able to run the program.
Follow the installation guide in the root directory's README.

### Requirements

To run this program, you need the following:

* [Docker](https://docs.docker.com/get-docker/)

### Build

The program uses docker for encapsulating the application. Assuring Consistency
across multiple development environments.