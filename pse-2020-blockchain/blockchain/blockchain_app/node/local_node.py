"""
This file handles api calls from the website back-end. The communication
is set up using (TCP) sockets. A connection is set up each time a request
is sent to the server socket by the client socket of the back-end. All
possible requests are:
    - get_block
    - fetch_last_block
    - check_chain
    - add_block
    - save_chain
    - find_games
    - find_blocks
    - admin_add_block
    - generate_keys
Each request will send a response to the client socket that sent the request.
"""

__author__ = ["Jari Andersen", "Lennart de Boer", "Stan van der Kroon",
              "Joris op ten Berg"]
__credits__ = ["Jari Andersen", "Lennart de Boer", "Stan van der Kroon",
               "Joris op ten Berg"]
__maintainer__ = ["Jari Andersen", "Lennart de Boer", "Stan van der Kroon",
                  "Joris op ten Berg"]

import socket
import select
import threading
import time
import json

from blockchain_app.node import Blockchain, Block, BlockNode, rsa


BUFSIZE = 2048


class Server():
    """
    Server class that creates a socket which can
    receive requests from the website back-end.
    """

    def __init__(self, host, s_port, n_port, chain):
        """
        Initialization of the server.

        :param host: Ip adress to bind the socket to
        :type host: String
        :param port: Port to bind the socket to
        :type port: Integer
        :param chain: The blockchain of the node
        :type chain: Blockchain class
        """
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.host = host
        self.s_port = 12345
        self.s.bind((self.host, self.s_port))

        self.running = True
        self.connected_sockets = [self.s]
        self.socket_mapping = {self.host: self.s}
        # self.queue = []
        self.node = BlockNode(self.host, n_port, Blockchain(chain))
        pass

    def run(self, node_list):
        """
        Runs the server process. In this function the connection with the
        client (server back-end) is established and incomming messages are
        processed.
        """
        self.node.start()
        for (host, port) in node_list:
            self.node.connect_with_node(host, port)
        if node_list:
            print('requesting blockchain')
            self.node.send_blockchain_request()
        print(f'Server running on adress: {self.host}:{self.s_port}',
              flush=True)
        self.s.listen(10)

        while self.running:
            read_s, _, _ = select.select(self.connected_sockets, [], [])
            for s in read_s:
                if s == self.s:
                    self.handle_connection()
                else:
                    try:
                        message = s.recv(BUFSIZE).decode('utf-8')
                        if message != '':
                            self.handle_message(s, message)

                        target_host = None
                        for host in self.socket_mapping:
                            if self.socket_mapping[host] == s:
                                target_host = host
                        self.handle_close({target_host: s})
                    except Exception as e:
                        print(f'Error occured: {e}', flush=True)

        self.handle_close(reason=False)
        pass

    def handle_connection(self):
        """
        Handles the connection with the server back-end client socket.
        """
        c_socket, (c_host, _) = self.s.accept()

        self.connected_sockets.append(c_socket)
        self.socket_mapping[c_host] = c_socket

        c_socket.send('Connection succesfull.'.encode('utf-8'))
        print(f'\nNode with ip: {c_host} connected.', flush=True)
        pass

    def get_block(self, s, parameter):
        """
        Get a block from the blockchain object with the given data.

        :param s: Socket to send responses through.
        :type s: Socket object
        :param parameter: Parameter that is either an index or hash. Used to
        retrieve a block.
        :type parameter: String
        """
        # Try to see if the given string is a hash (string) or index (int).
        try:
            index = int(parameter)
        except Exception:
            index = parameter
        block = self.node.chain.fetch_a_block(index)
        if not block:
            s.send('block not found'.encode('utf-8'))
        else:
            s.send(json.dumps(block.data).encode('utf-8'))
        pass

    def fetch_last_block(self, s):
        """
        Fetch the last block from the blockchain and respond correctly on
        succes and failure.

        :param s: Socket to send responses through.
        :type s: Socket object
        """
        block = self.node.chain.fetch_last_block()
        if not block:
            s.send('last block not found'.encode('utf-8'))
        else:
            s.send(json.dumps(block.data).encode('utf-8'))
        pass

    def check_chain(self, s):
        """
        Check the chain in the blockchain object an respond corretly on
        succes and failure.

        :param s: Socket to send responses through.
        :type s: Socket object
        """
        if self.node.chain.check_chain():
            s.send('chain correct'.encode('utf-8'))
        else:
            s.send('chain incorrect'.encode('utf-8'))
        pass

    def add_block(self, s, params):
        """
        Add a block to the last block from the blockchain and respond correctly
        on succes and failure.

        :param s: Socket to send responses through
        :type s: Socket object
        :param params: Parameters of the request
        :type params: String
        """
        self.node.queue.append(params)
        if len(self.node.queue) >= 1:
            self.node.send_lock_request()
        s.send('block added in queue'.encode('utf-8'))

    def admin_add_block(self, s, params,
                        file='blockchain_app/node/blockchain.json'):
        """
        Add a block to the chain. Don't push it to the other nodes. This
        function is made solely for testing a singular node.
        Respond correctly on succes and failure.

        :param s: Socket to send responses through
        :type s: Socket object
        :param params: Parameters of the request
        :type params: String
        """
        block = Block([params], self.node.chain.fetch_last_block().make_hash())
        self.node.chain.add_block(block, file)
        msg = 'added block onto our chain with the admin add block function'
        msg += ' (so not pushed to others).'
        s.send(msg.encode('utf-8'))
        pass

    def save_chain(self, s):
        """
        Save the chain from the blockchain and respond correctly on
        succes and failure.

        :param s: Socket to send responses through.
        :type s: Socket object
        """
        try:
            self.node.chain.save_blockchain_to_file()
            s.send('save succesful'.encode('utf-8'))
        except Exception:
            s.send('save failed'.encode('utf-8'))
        pass

    def find_games(self, s, params):
        """
        Find the games in the chain from the blockchain with the params
        and respond with the games.

        :param s: Socket to send responses through.
        :type s: Socket object
        :param params: The key value tupple.
        :type params: String
        """
        game = self.node.chain.find_games(*params.split(' ', 1))
        print(json.dumps(game), flush=True)
        s.send(json.dumps(game).encode('utf-8'))
        pass

    def find_blocks(self, s, params):
        """
        Find the blocks in the chain from the blockchain with the params
        and respond with the blocks.

        :param s: Socket to send responses through.
        :type s: Socket object
        :param params: The key value tupple.
        :type params: String
        """
        blocks = self.node.chain.find_blocks(*params.split(' '))
        s.send(json.dumps([block.data for block in blocks]).encode('utf-8'))
        pass

    def get_all_games(self, s):
        """
        Get all games from the chain. And respond with a json file.

        :param s: Socket to send responses through.
        :type s: Socket object
        """
        games = self.node.chain.get_all_games()
        s.send(json.dumps(games).encode('utf-8'))
        pass

    def get_new_keys(self, s, keysize):
        """
        Get new keypair from rsa.py and send it to socket s

        :param s: Socket to send the response to.
        :type s: Socket object
        :param keysize: The desired size of the keys.
        :type keysize: Int
        """
        private, public = rsa.generate_keys(int(keysize))
        msg = rsa.key_text(private) + rsa.key_text(public)
        s.send(msg.encode('utf-8'))
        pass

    def handle_message(self, s, message,
                       file='blockchain_app/node/blockchain.json'):
        """
        Handles the incoming request.

        :param s: Socket that the back-end client connected to
        :type s: Socket object
        :param message: The request and other required parameters.
        :type message: String
        """
        try:
            request, parameters = message.split(' ', 1)
        except Exception:
            request = message
            parameters = ""

        if request == 'get_block':
            self.get_block(s, json.loads(parameters))
        elif request == 'fetch_last_block':
            self.fetch_last_block(s)
        elif request == 'check_chain':
            self.check_chain(s)
        elif request == 'add_block':
            self.add_block(s, json.loads(parameters))
        elif request == 'save_chain':
            self.save_chain(s)
        elif request == 'find_games':
            self.find_games(s, parameters)
        elif request == 'find_blocks':
            self.find_blocks(s, parameters)
        elif request == 'admin_add_block':
            self.admin_add_block(s, json.loads(parameters), file)
        elif request == 'generate_keys':
            self.get_new_keys(s, parameters)
        elif request == 'print_blocks':  # Mostly used for debugging
            self.node.chain.print_all_blocks()
            s.send('blocks printed in server terminal'.encode('utf-8'))
        elif request == 'get_all_recent_games':
            self.get_all_games(s)
        else:
            s.send('request not found'.encode('utf-8'))
        pass

    def handle_close(self, sockets=None, reason=True):
        """
        Close all sockets in the sockets argument.

        :param sockets: Sockets that should be closed, if none, set it to all
        nodes.
        :type sockets: Dictionary
        :param reason: If True, print that a connection with a client has
        ended.
        :type reason: Boolean
        """
        if not sockets:
            sockets = self.socket_mapping

        for host in sockets:
            if reason:
                print(f'Connection with client with ip: {host} ended.',
                      flush=True)
                self.connected_sockets.remove(sockets[host])
                del self.socket_mapping[host]

            sockets[host].close()
        pass
