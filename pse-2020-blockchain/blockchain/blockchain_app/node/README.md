## Files

### blockchain.json

The blockchain.json file is an example of how a blockchain is stored.

### blockchain.py

The blockchain.py is the actual blockchain data structure. It can put arbitrary data in a chain and parse it in diffrent ways.

### blocknode.py

This file contains the BlockNode class, which handles peer-to-peer communication between nodes running the blockchain.
This class extends the SecureNode class found in the p2pnetwork module.

### local_node.py

This file contains the Server class, which communicates with the website back-end and calls functions from the BlockNode class.

### node_test.py

This is a test client to 'talk' to the server.

### rsa.py

The rsa.py makes it possible to sign matches to be sure the games are actually played by the person with the private key.


## Installation

Clone or download this repository to a folder of your preference. Make sure the requirements below are installed to be able to run the program.
Follow the installation guide in the root directory's README.

### Requirements

To run this program, you need the following:

* [Docker](https://docs.docker.com/get-docker/)

### Build

The program uses docker for encapsulating the application. Assuring Consistency
across multiple development environments.