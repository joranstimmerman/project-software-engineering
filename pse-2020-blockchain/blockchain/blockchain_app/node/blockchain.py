"""
This is the Blockchain api file. It serves as the backbone of the blockchain.
The file consists out of 2 classes. A Block class (for blocks in the chain)
and a Blockchain class (the actual chain). The block class holds the actual
data and the blockchain class holds the blocks. The block class has all the
functionalities for a block (such as make_hash) and also some added
functionalities (such as saving the block to a file).
The Blockchain class has all the functionalities of a blockchain (such as
the ability to add blocks, check the chain for integrity, creating a
genesis block and checking an individual block) but it also consists out of
some added functionalities (such as saving the chain to a file, fetching data,
fetching blocks, fetching games, printing all blocks and recreating the chain
out of a file).
"""

__author__ = ["Jari Andersen", "Roberto"]
__credits__ = ["Jari Andersen", "Roberto"]
__maintainer__ = ["Jari Andersen"]

import hashlib
import json
import datetime
from time import time


class Block:
    def __init__(self, data, previous_hash):
        """
        The constructor of the block class.

        :param data: The data attached to the block (you can give anything,
        even an entire JSON file).
        :type: JSON (array of games)
        :param previous_hash: The hash of the previous block in the chain.
        :type: Hash (string)
        """
        self.previous_hash = previous_hash
        self.data = data

        pass

    def make_dict(self):
        return {'data': self.data, 'previous_hash': self.previous_hash}

    def save_block_to_file(self, file='block_save.json'):
        '''
        Save the entire block to a file in a JSON format.
        '''
        block = self.make_dict()

        with open(file, 'w', encoding='utf-8') as outfile:
            json.dump(block, outfile, ensure_ascii=False, indent=4)

        pass

    def make_hash(self):
        """
        Makes a hash of the block in JSON format.
        """
        block_string = json.dumps(self.__dict__, sort_keys=True)

        return hashlib.sha256(block_string.encode()).hexdigest()


class Blockchain:
    def __init__(self, chain_data=0):
        """
        Constructor for the Blockchain class.

        :param chain_data: You could give a JSON file to this keyword. If you
        do a function in the Blockchain will be called to recreate that
        chain_data.
        :type: JSON file (with blockchain in it)
        """
        if chain_data:
            self.recreate_chain(chain_data)
        else:
            self.chain = []
            self.create_genesis_block()

        pass

    def create_genesis_block(self):
        """
        A function to generate genesis block and appends it to
        the chain. The block has index 0, previous_hash as 0, and
        a valid hash.
        This function is only used by the first person to create the chain.
        """
        genesis_block = Block(
            ["genesis_block"], "0")
        self.chain.append(genesis_block)

        pass

    def fetch_last_block(self):
        """
        A function to retrieve the most recent block in the chain.
        :return: Last block
        :rtype: Object (Block)
        """
        return self.chain[-1]

    def fetch_a_block(self, index):
        '''
        Fetch a block in the chain with an index or a hash. Return False if
        the function wasn't able to find the block.

        :param index: Either an index of a block in the chain or a hash of
        a block.
        :type: Int or String
        :return: Object (Block) or False
        '''
        # Fetch on index
        if type(index) == int:
            if index >= len(self.chain):
                print("Index out of range", flush=True)
                return False
            return self.chain[index]

        # Fetch on id by checking the previous hash
        for i in range(1, len(self.chain)):
            if self.chain[i].previous_hash == index:
                return self.chain[i - 1]

        # Fetch on id for the last block, as it doesn't have a
        # next block to point at it.
        if self.chain[-1].make_hash() == index:
            return self.chain[-1]

        return False

    def print_all_blocks(self):
        '''
        Print all the blocks' data and previoush_hashes in the chain.
        '''
        for block in self.chain:
            if block.data == ['genesis_block']:
                print(block.data, flush=True)
            else:
                for game in block.data:
                    print(f"DATA: {game}", flush=True)
                print("\tPrevious_hash: " + block.previous_hash, flush=True)
        pass

    def add_block(self, block, file='blockchain_app/node/blockchain.json'):
        """
        A function that adds the block to the chain after verification.
        Verificating goes as follows:
        The previous_hash referred in the block and the hash of a latest
        block in the chain match.
        return True if it is correct and False if the block isn't correct.
        On True we append the block to the chain and we save the chain to the
        blockchain.json file.

        :param block: A block that gets to be added to the chain. It
        already should have a valid previous_hash and data.
        :type: Object (Block)
        :return: False or True. Depdends if the block was added.
        :rtype: boolean
        """
        previous_hash = self.fetch_last_block().make_hash()

        if previous_hash != block.previous_hash:
            print("Your chains last block isn't the same as your",
                  "previous_hash incorrect.")
            return False

        self.chain.append(block)
        self.save_blockchain_to_file(file)

        return True

    def save_blockchain_to_file(self,
                                file='blockchain_app/node/blockchain.json'):
        '''
        Save all the blocks in the blockchain to a file in a json format.
        '''
        block_list = {}
        block_list['blocks'] = []

        for block in self.chain:
            block_list['blocks'].append({
                'data': block.data,
                'previous_hash': block.previous_hash
            })

        with open(file, 'w', encoding='utf-8') as outfile:
            json.dump(block_list, outfile, ensure_ascii=False, indent=4)

        pass

    def recreate_chain(self, chain_data):
        '''
        Recreate the blockchain out of chain_data.

        :param chain_data: Chain data to be recreated out of a JSON file.
        :type: String
        '''
        self.chain = []

        for block in chain_data['blocks']:
            self.chain.append(Block(block['data'], block['previous_hash']))

    def find_blocks(self, param, value):
        '''
        Returns the blocks from the chain that have a certain value for the
        parameter.

        :param param: The key of the key value dictionary pair.
        :type: string
        :param value: The value of the key value dictionary pair.
        :type: string
        :return: Dictionary of Blocks (Objects)
        :rtype: dictionary
        '''
        blocks = {'blocks': []}

        for block in self.chain[1:]:
            for game in block.data:
                if param == 'username':
                    if (game['username_1'] == value) | (game['username_2'] ==
                                                        value):
                        blocks['blocks'].append(block)
                        break
                elif param in game and game[param] == value:
                    blocks['blocks'].append(block)
                    break

        return blocks

    def find_games(self, param, value):
        '''
        Returns the games from the chain that have a certain value for the
        parameter.

        :param param: The key of the key value dictionary pair.
        :type: string
        :param value: The value of the key value dictionary pair.
        :type: string
        :return: Dictionary of games (strings)
        :rtype: dictionary
        '''
        games = {'games': []}

        for block in self.chain[1:]:
            for game in block.data:
                if param == 'username':
                    if (game['username_1'] == value) | (game['username_2'] ==
                                                        value):
                        games['games'].append(game)
                elif param in game and game[param] == value:
                    games['games'].append(game)

        return games

    def get_all_games(self):
        """
        Return all the games from the chain in a dictionary.

        :return: dictionary of games (strings)
        :rtype: dictionary
        """
        games = {"games": []}
        if len(self.chain) > 20:
            start_block = len(self.chain) - 20
        else:
            start_block = 1

        for block in self.chain[start_block:]:
            for game in block.data:
                games["games"].append(game)

        return games

    def check_chain(self):
        '''
        Check entire chain by using a previous hash and the make_hash function
        in the Block class.

        :return: True or False. Depending on the integrity of the chain.
        :rtype: boolean
        '''
        for i in range(1, len(self.chain)):
            if self.chain[i-1].make_hash() != self.chain[i].previous_hash:
                print("Chain is invalid!\nBlock at index: " + str(i-1) +
                      " doesn't have the same hash as the previous_hash at",
                      " index: " + str(i))
                return False
        print("The chain is good!")

        return True

    def check_block(self, index):
        '''
        Check if a given block has a changed hash. If the block has altered
        data: return False. Otherwise return True.

        :param index: An index or hash for a given block.
        :type: int or string
        :return: True or False. Depending if the block on the index is correct.
        :rtype: boolean
        '''
        block = self.fetch_a_block(index)

        if block is False:
            return False

        if block == self.fetch_last_block():
            print("The given block is the last in the chain so there is no",
                  "way to check integrity.")
            return True
        else:
            next_block = self.fetch_a_block(index+1)
            if next_block.previous_hash != block.make_hash():
                print("The next block has a different previous hash than the",
                      "given block hashed. So data has changed.", flush=True)
                return False

        print("No data has been changed in this block.")

        return True

    def make_dict(self):
        return {'blocks': [b.make_dict() for b in self.chain]}
