"""
This is the BlockNode class file. It is used for the nodes that run the
blockchain. It is used to send and receive messages like lock and blockchain
requests.
"""

__author__ = ["Stan van der Kroon", "Joris op ten Berg", "Lennart de Boer"]
__credits__ = ["Stan van der Kroon", "Joris op ten Berg", "Lennart de Boer"]
__maintainer__ = ["Stan van der Kroon", "Joris op ten Berg", "Lennart de Boer"]

from blockchain_app.p2pnetwork import SecureNode
from blockchain_app.node import Blockchain, Block, verify_transaction
import time
import json


class BlockNode(SecureNode):
    def __init__(self, host, port, blockchain):
        """
        The constructor of the BlockNode class.

        :param host: Ip-address of the host
        :type host: String
        :param port: Port that the node listens to.
        :type port: Integer
        :param blockchain: The blockchain.
        :type blockchain: Blockchain object
        """
        super(BlockNode, self).__init__(host, port)

        self.chain = blockchain
        self.block = None
        self.lock = False
        self.lock_info = None
        self.neighbors_left = 0
        self.parent = None
        self.blockchain_neighbors_left = 0
        self.parent_blockchain = None
        self.temp_blockchains = []
        self.queue = []

    def node_message(self, connected_node, message):
        """
        Get the message from another node and check what type of message it is.
        Call the corresponding function.

        :param connected_node: Node connected to this node.
        :type connected node: BlockNode object
        :param message: The message, formatted in JSON.
        :type message: String
        """
        try:
            data = json.loads(message)
            if self.check_message(data):
                if (data['_type'] == 'lock_request'):
                    self.handle_lock_request(connected_node, data)
                elif (data['_type'] == 'lock_response'):
                    self.handle_lock_response(data)
                elif (data['_type'] == 'lock_release'):
                    self.handle_lock_release(data)
                elif (data['_type'] == 'blockchain_request'):
                    self.handle_blockchain_request(connected_node, data)
                elif (data['_type'] == 'blockchain_response'):
                    self.handle_blockchain_response(data)
                elif (data['_type'] == 'block'):
                    self.handle_block(data)
            else:
                print("Received message is corrupted and cannot be processed!")

        except Exception as e:
            print("Data could not be parsed (%s) (%s)" % (message, str(e)))

    def send_to_nodes(self, data, exclude=[]):
        """
        Sends a message to all neighbours except the ones you give in exclude.

        :param data: JSON formatted string containing the message.
        :type data: String
        :param exclude: List of nodes to exclude.
        :type exclude: List of strings
        """
        self.message_count_send = self.message_count_send + 1
        for n in self.nodes_inbound:
            if n.id not in exclude:
                self.send_to_node(n, data)

        for n in self.nodes_outbound:
            if n.id not in exclude:
                self.send_to_node(n, data)

    def send_lock_request(self):
        """
        Sends a lock request to all neighbours.
        """
        self.lock = True
        self.lock_info = self.id
        self.neighbors_left = self.count_available_neigbors()

        if self.neighbors_left == 0:
            self.send_block()
            return

        exclude_list = self.build_exclude_list()
        msg = self.msg('lock_request', self.id, time.time(), exclude_list)
        self.send_to_nodes(msg)

    def handle_lock_request(self, node, data):
        """
        If you are not locked, lock yourself and send it through. If you get a
        lock request with an id you already have or if the node has no
        neighbours not already excluded, send a lock response.

        :param node: Node from which you got the lock request.
        :type node: BlockNode object
        :param data: A dictionary with data from the message.
        :type data: Dictionary
        """
        if not self.lock:
            self.lock = True
            self.lock_info = data['id']
            self.parent = node
            self.neighbors_left = self.count_available_neigbors(
                data['exclude'])
            if self.neighbors_left == 0:  # node is a leaf
                self.handle_lock_response(data)
                return

            exclude_list = self.build_exclude_list(data['exclude'])
            msg = self.msg('lock_request', data['id'], data['timestamp'],
                           exclude_list)
            self.send_to_nodes(msg, exclude=data['exclude'])
        elif self.lock_info == data['id']:
            msg = self.msg('lock_response', data['id'], data['timestamp'])
            self.send_to_node(node, msg)
        else:
            self.send_lock_release()

    def handle_lock_response(self, data):
        """
        Handle receiving a lock_response. If the lock request has been to all
        nodes, forward it to your parent unless you were the one requesting
        the lock. In that case, call send_block.

        :param data: Dictionary with data from the message
        :type data: Dictionary
        """
        self.neighbors_left -= 1

        if self.neighbors_left <= 0:  # all neighbours have sent lock_responses
            if self.id == data['id']:  # node was the one requesting the lock
                self.send_block()
            else:
                msg = self.msg('lock_response', data['id'], data['timestamp'])
                self.send_to_node(self.parent, msg)
            self.parent = None
            self.neighbors_left = 0

    def send_lock_release(self):
        """
        Send a message to all nodes telling them to release their lock.
        """
        self.lock = False
        self.parent = None
        self.lock_info = None

        msg = self.msg('lock_release', self.id, time.time(),
                       self.build_exclude_list())
        self.send_to_nodes(msg)

    def handle_lock_release(self, data):
        """
        Handle a request to release a nodes lock.

        :param data: The data of the message.
        :type data: Dictionary
        """
        self.lock = False
        self.lock_info = None
        self.parent = None

        msg = self.msg('lock_release', data['id'], data['timestamp'],
                       self.build_exclude_list(data['exclude']))
        self.send_to_nodes(msg, data['exclude'])

    def send_blockchain_request(self, exclude_list=[]):
        """
        Send a request for the whole blockchain.

        :param exclude_list: List of nodes to exclude.
        :type exclude_list: List of strings
        """
        self.blockchain_neighbors_left = \
            self.count_available_neigbors(exclude_list)

        new_exclude_list = self.build_exclude_list(exclude_list)
        msg = self.msg('blockchain_request', self.id,
                       time.time(), new_exclude_list)
        self.send_to_nodes(msg, exclude_list)

    def handle_blockchain_request(self, parent, data):
        """
        Handle a request for a blockchain by checking your chain and sending it
        to the requesting node if it is valid. If it is not valid, request a
        new chain for yourself from your neighbours.

        :param parent: The node you received the request from.
        :type parent: BlockNode object
        :param data: The data of the message.
        :type data: Dictionary
        """
        self.parent_blockchain = parent
        if self.chain.check_chain():
            msg = self.msg('blockchain_response', self.id,
                           time.time(), content=self.chain.make_dict())
            self.send_to_node(parent, msg)
            self.parent_blockchain = None
        else:
            self.blockchain_neighbors_left = \
                self.count_available_neigbors(data['exclude'])
            if self.blockchain_neighbors_left == 0:
                self.chain = Blockchain()
                msg = self.msg('blockchain_response', self.id,
                               time.time(), content=self.chain.make_dict())
                self.send_to_node(parent, msg)
                self.parent_blockchain = None
            else:
                self.send_blockchain_request(data['exclude'])

    def handle_blockchain_response(self, data):
        """
        Handle receiving a new blockchain. Possible blockchains from
        neighbours are collected and the most common chain is adopted. The
        blockchain should be contained in the 'content' entry of the data.

        :param data: The data of the message.
        :type data: Dictionary
        """
        self.blockchain_neighbors_left -= 1
        self.temp_blockchains.append(Blockchain(data['content']))
        if self.blockchain_neighbors_left == 0:
            candidates = [chain.make_dict().__str__()
                          for chain in self.temp_blockchains
                          if chain.check_chain()]
            if candidates:
                self.chain = Blockchain(eval(max(set(candidates),
                                                 key=candidates.count)))
            elif not self.chain.check_chain():
                self.chain = Blockchain()
            if self.parent_blockchain is not None:
                msg = self.msg('blockchain_response', data['id'],
                               data['timestamp'],
                               content=self.chain.make_dict())
                self.send_to_node(self.parent_blockchain, msg)
                self.parent_blockchain = None
            self.chain.save_blockchain_to_file()
        self.temp_blockchains = []

    def send_block(self):
        """
        Send a block. The block will reach the entire network.
        """
        if self.lock and self.lock_info == self.id:
            new_block = Block(self.queue.copy(),
                              self.chain.fetch_last_block().make_hash())

            if not self.chain.add_block(new_block):
                self.send_lock_release()
                self.send_blockchain_request()
                return

            self.queue = []
            self.lock = False
            self.lock_info = None
            self.parent = None

            exclude_list = self.build_exclude_list()
            msg = self.msg('block', self.id, time.time(), exclude_list,
                           new_block.make_dict())
            self.send_to_nodes(msg)
        else:
            self.send_lock_release()
            pass

    def handle_block(self, data):
        """
        Handle receiving a new block. The block is forwarded to any neighbours
        not yet in the exclude list. The block should be contained in the
        'content' entry of the data.

        :param data: The data of the message.
        :type data: Dictionary
        """
        if self.lock and data['id'] == self.lock_info:
            new_block = Block(*data['content'].values())

            if self.chain.fetch_last_block().make_dict() == data['content']:
                self.lock = False
                self.lock_info = None
                self.parent = None
                return

            if not self.chain.add_block(new_block):
                self.send_lock_release()
                self.send_blockchain_request()
                return
            self.lock = False
            self.lock_info = None
            self.parent = None

            exclude_list = self.build_exclude_list(data['exclude'])
            msg = self.msg('block', data['id'], data['timestamp'],
                           exclude_list, data['content'])
            self.send_to_nodes(msg, exclude=data['exclude'])
        else:
            self.send_lock_release()
            pass

    def msg(self, m_type, m_id, time, exclude=False, content=False):
        """
        Create a message to send to peers.

        :param m_type: The tpe of message.
        :type m_type: String
        :param m_id: The node id to be included.
        :type m_id: String
        :param time: The timestamp to be included.
        :type time: Float
        :param exclude: (optional) The exclude list to be included.
        :type exclude: List of strings
        :param content: (optional) Other content to be included.
        :type content: Dictionary
        :return: A json formatted string containing the message.
        :rtype: String
        """
        data = {'_type': m_type,
                'id': m_id,
                'timestamp': time}
        if exclude:
            data['exclude'] = exclude
        if content:
            data['content'] = content
        return self.create_message(data)

    def build_exclude_list(self, exclude_list=[]):
        """
        Add all neighbours to the list of nodes to exclude, ignoring
        duplicates.

        :param exclude_list: (optional) The current exclude list.
        :type exclude_list: List of strings
        :return: The new exclude list
        :rtype: List of strings
        """
        new_exclude_list = exclude_list.copy()
        for c in self.all_nodes:
            (id0, id1) = c.get_connection_ids()
            if id0 not in exclude_list:
                new_exclude_list.append(id0)
            if id1 not in exclude_list:
                new_exclude_list.append(id1)
        return new_exclude_list

    def count_available_neigbors(self, exclude_list=[]):
        """
        Get the number of neighbours not yet in the list of nodes to exclude.

        :param exclude_list: (optional) The current exclude list.
        :type exclude_list: List of strings
        :return: The number of neighbours left.
        :rtype: Integer
        """
        neighbors_left = 0
        for n in self.nodes_inbound:
            if n.id not in exclude_list:
                neighbors_left += 1
        for n in self.nodes_outbound:
            if n.id not in exclude_list:
                neighbors_left += 1
        return neighbors_left

    def verify_games(self, block):
        """
        Verifies all the games in a block with the public keys.

        :param block: Block that is going to be added onto the blockchain.
        :type block: Block object
        :return: True if all games are correct else False.
        :rtype: Boolean
        """
        for game in block.data:
            if not verify_transaction(game):
                return False
        return True
