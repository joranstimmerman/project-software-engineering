## DISCLAIMER:
This directory contains the p2pnetwork module as written by Maurice Snoeren, with minimal changes.
Please see the following repository for the full code and licensing information:
https://github.com/macsnoeren/python-p2p-network

## Files


### \_\_init\_\_.py

This is the initialize file, some environment variables are set in here.

### node.py

This is the file where the Node class is defined and the function calls are being made.

### nodeconnection.py

This file is where the NodeConnection is defined. This is used by node.py. It establishes connections to other nodes.

### securenode.py

This file contains a functional implementation of the node class, focussing on secure messaging.
The SecureNode class from this file is the parent class of our own BlockNode class for p2p communication between nodes running the blockchain.

## Installation

Clone or download this repository to a folder of your preference. Make sure the requirements below are installed to be able to run the program.
Follow the installation guide in the root directory's README.

### Requirements

To run this program, you need the following:

* [Docker](https://docs.docker.com/get-docker/)