"""
This file tests the communication with the server that can
interact with the blockchain (local_node.py).
Several requests are tested.
"""

__author__ = ["Lennart de Boer"]
__credits__ = ["Lennart de Boer"]
__maintainer__ = ["Lennart de Boer"]

from blockchain_app.node import Server
from blockchain_app.node import Blockchain
import socket
import pytest
import json

def get_chain():
    return {
                "blocks": [
                    {
                        "data": [
                            "genesis_block"
                        ],
                        "previous_hash": "10"
                    },
                    {
                        "data": [
                            {
                                "result": "1-0",
                                "elo_score_contestant_1": 600,
                                "elo_score_contestant_2": 500,
                                "move_list": [],
                                "date": "2020-06-11",
                                "username_1": "eljero66",
                                "username_2": "gerard",
                                "public_key_contestant_1": "--- Begin Key --- ANDAWIJDKAWJDLAWd",
                                "public_key_contestant_2": "--- Begin Key --- ANDAWIJDKAWJDLAWd"
                            },
                            {
                                "result": "1-0",
                                "elo_score_contestant_1": 600,
                                "elo_score_contestant_2": 500,
                                "move_list": [],
                                "date": "2020-06-11",
                                "username_1": "klaas",
                                "username_2": "gerard",
                                "public_key_contestant_1": "--- Begin Key --- ANDAWIJDKAWJDLAWd",
                                "public_key_contestant_2": "--- Begin Key --- ANDAWIJDKAWJDLAWd"
                            },
                            {
                                "result": "1-0",
                                "elo_score_contestant_1": 600,
                                "elo_score_contestant_2": 500,
                                "move_list": [],
                                "date": "2020-06-11",
                                "username_1": "klaas",
                                "username_2": "gerard",
                                "public_key_contestant_1": "--- Begin Key --- ANDAWIJDKAWJDLAWd",
                                "public_key_contestant_2": "--- Begin Key --- ANDAWIJDKAWJDLAWd"
                            }
                        ],
                        "previous_hash": "8d144cab5deb1afb568bf86fe7203bdea6d3c0c51c013c51f2cb2d75601cf73a"
                    }
                ]
            }

@pytest.fixture(scope='module')
def new_server():
    chain = Blockchain(get_chain()).chain

    block_list = {}
    block_list['blocks'] = []

    for block in chain:
        block_list['blocks'].append({
            'data': block.data,
            'previous_hash': block.previous_hash
        })

    with open('tests/local_node_chain.json', 'w', encoding='utf-8') as outfile:
        json.dump(block_list, outfile, ensure_ascii=False, indent=4)

    with open('tests/local_node_chain.json') as node_chain:
        server_class = Server('127.0.0.2', 12345, 12344, json.load(node_chain))

    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    server.bind(('127.0.0.1', 12345))
    server.listen(10)

    client.bind(('127.0.0.1', 12334))
    client.connect(('127.0.0.1', 12345))

    c_socket, _ = server.accept()
    return client, server_class, c_socket


def get_block(i):
    if i == 1:
        return [{
            "result": "1-0",
            "elo_score_contestant_1": 600,
            "elo_score_contestant_2": 500,
            "move_list": [],
            "date": "2020-06-11",
            "username_1": "eljero66",
            "username_2": "gerard",
            "public_key_contestant_1": "--- Begin Key --- ANDAWIJDKAWJDLAWd",
            "public_key_contestant_2": "--- Begin Key --- ANDAWIJDKAWJDLAWd"
        }, {
            "result": "1-0",
            "elo_score_contestant_1": 600,
            "elo_score_contestant_2": 500,
            "move_list": [],
            "date": "2020-06-11",
            "username_1": "klaas",
            "username_2": "gerard",
            "public_key_contestant_1": "--- Begin Key --- ANDAWIJDKAWJDLAWd",
            "public_key_contestant_2": "--- Begin Key --- ANDAWIJDKAWJDLAWd"
        }, {
            "result": "1-0",
            "elo_score_contestant_1": 600,
            "elo_score_contestant_2": 500,
            "move_list": [],
            "date": "2020-06-11",
            "username_1": "klaas",
            "username_2": "gerard",
            "public_key_contestant_1": "--- Begin Key --- ANDAWIJDKAWJDLAWd",
            "public_key_contestant_2": "--- Begin Key --- ANDAWIJDKAWJDLAWd"
        }]
    if i == 2:
        return {
            "result": "0-1",
            "elo_score_contestant_1": 700,
            "elo_score_contestant_2": 800,
            "move_list": [],
            "date": "2020-06-11",
            "username_1": "testpersoon1",
            "username_2": "testpersoon2",
            "public_key_contestant_1": "--- Begin Key --- ANDAWIJDKAWJDLAWd",
            "public_key_contestant_2": "--- Begin Key --- ANDAWIJDKAWJDLAWd"
        }


def test_get_block(new_server):
    """
    Check retreiving the block with index 1.
    """
    client, server_class, c_socket = new_server
    Server.handle_message(server_class, client, 'get_block 1')
    response = c_socket.recv(10240).decode('utf-8')

    block_1 = get_block(1)
    assert response == json.dumps(block_1)


def test_fetch_last_block(new_server):
    """
    Check retreiving the last block in the chain.
    """
    client, server_class, c_socket = new_server
    Server.handle_message(server_class, client, 'fetch_last_block a b')
    response = c_socket.recv(10240).decode('utf-8')

    block_1 = get_block(1)
    assert response == json.dumps(block_1)


def test_check_chain(new_server):
    """
    Check checking the chain.
    """
    client, server_class, c_socket = new_server
    Server.handle_message(server_class, client, 'check_chain')
    response = c_socket.recv(10240).decode('utf-8')
    assert response == 'chain correct' or response == 'chain incorrect'


def test_find_games(new_server):
    """
    Check retreiving all games where username_1 is eljero66 (1 expected) and
    Check retreiving all games where username_1 is klaas (2 expected).
    """
    client, server_class, c_socket = new_server
    Server.handle_message(server_class, client,
                          'find_games username_1 eljero66')
    response = c_socket.recv(10240).decode('utf-8')

    block_1 = get_block(1)
    game_1 = json.dumps({"games": [block_1[0]]})
    assert response == game_1

    Server.handle_message(server_class, client, 'find_games username_1 klaas')
    response = c_socket.recv(10240).decode('utf-8')

    block_1 = get_block(1)
    game_2 = json.dumps({"games": block_1[1:3]})
    assert response == game_2


def test_admin_add_block(new_server):
    """
    Check wether adding a block succeeds.
    """
    client, server_class, c_socket = new_server
    block_data = get_block(2)

    Server.handle_message(server_class, client, 'admin_add_block ' +
                          json.dumps(block_data), 'tests/local_node_chain.json')
    response = c_socket.recv(10240).decode('utf-8')
    msg = 'added block onto our chain with the admin add block function'
    msg += ' (so not pushed to others).'
    assert response == msg

    Server.handle_message(server_class, client,
                          'find_games username_1 testpersoon1')
    response = c_socket.recv(10240).decode('utf-8')
    assert response == json.dumps({"games": [block_data]})
