"""
This file tests the blockchain api file (blockchain.py).
It uses blockchain.json, blockchain_test.json and block_save.json in some
tests.
"""

__author__ = ["Jari Andersen"]
__credits__ = ["Jari Andersen"]
__maintainer__ = ["Jari Andersen"]

from blockchain_app.node import Blockchain, Block
import pytest
import json


@pytest.fixture(scope='module')
def new_blockchain():
    bc = Blockchain()
    return bc


@pytest.fixture(scope='module')
def filled_chain():
    bc = Blockchain()

    # Make first block
    new_block = Block(["BLOCK1_data"], bc.fetch_last_block().make_hash())
    bc.add_block(new_block, 'tests/blockchain_test_2.json')

    # Make second block
    new_block_2 = Block(
        ["BLOCK2_data"], bc.fetch_last_block().make_hash())
    bc.add_block(new_block_2, 'tests/blockchain_test_2.json')

    # Make third block
    new_block_3 = Block(
        ["BLOCK3_data"], bc.fetch_last_block().make_hash())
    bc.add_block(new_block_3, 'tests/blockchain_test_2.json')
    return bc


def test_1_save_to_chain_and_read(new_blockchain):
    '''
    TEST 1: Check if we can make a chain, add blocks and read chain
            information.
    '''
    # Make first block by adding the hash from the chain
    new_block = Block(
        ["BLOCK1_data"], new_blockchain.fetch_last_block().make_hash())

    # Make second block by adding the hash from the first block
    new_block_2 = Block(["BLOCK2_data"], new_block.make_hash())

    # Add the first block and test if it is added to the chain
    new_blockchain.add_block(new_block, 'tests/blockchain_test_2.json')
    assert new_blockchain.fetch_last_block().data == ["BLOCK1_data"]

    # Add the second block and test if it is added to the chain
    new_blockchain.add_block(new_block_2, 'tests/blockchain_test_2.json')
    assert new_blockchain.fetch_last_block().data == ["BLOCK2_data"]

    # Make third block by adding the hash from the first block
    new_block_3 = Block(
        ["BLOCK3_data"], new_blockchain.fetch_last_block().make_hash())

    # Add the third block and test if it is added to the chain
    new_blockchain.add_block(new_block_3, 'tests/blockchain_test_2.json')
    assert new_blockchain.fetch_last_block().data == ["BLOCK3_data"]


def test_2_add_block_with_wrong_hash(new_blockchain):
    '''
    TEST 2: Check if we can add a block that has a wrong hash.
    '''
    wrong_hashed_block = Block(["Wrong_hash_block_data"], 3)

    # Add the wrong hashed block and test if it is added to the chain
    assert new_blockchain.add_block(
        wrong_hashed_block, 'tests/blockchain_test_2.json') is False


def test_3_add_block_multiple_times(new_blockchain):
    '''
    TEST 3: Check if we can add a block multiple times
    '''
    for _ in range(3):
        new_block = Block(
            ["BLOCK_data"], new_blockchain.fetch_last_block().make_hash())

        assert new_blockchain.add_block(
            new_block, 'tests/blockchain_test_2.json') is True


def test_4_fetch_blocks(new_blockchain):
    '''
    TEST 4: Check if we can fetch the information of an arbitrary block in the
    chain by using the fetch_a_block() function in the Blockchain object.
    '''
    # Make first block
    new_block = Block(
        ["BLOCK1_data"], new_blockchain.fetch_last_block().make_hash())
    new_blockchain.add_block(new_block, 'tests/blockchain_test_2.json')

    # Make second block
    new_block_2 = Block(
        ["BLOCK2_data"], new_blockchain.fetch_last_block().make_hash())
    new_blockchain.add_block(new_block_2, 'tests/blockchain_test_2.json')

    # Make third block
    new_block_3 = Block(
        ["BLOCK3_data"], new_blockchain.fetch_last_block().make_hash())
    new_blockchain.add_block(new_block_3, 'tests/blockchain_test_2.json')

    # Search on block_ID
    found_block = new_blockchain.fetch_a_block(new_block.make_hash())
    assert found_block.data == ["BLOCK1_data"]

    # Search on index
    found_block = new_blockchain.fetch_a_block(2)
    assert found_block.data == ["BLOCK2_data"]

    # Search last block on block_ID
    found_block = new_blockchain.fetch_a_block(new_block_3.make_hash())
    assert found_block.data == ["BLOCK3_data"]


def test_5_save_chain_to_file_read_file(filled_chain):
    '''
    TEST 5: Check if we can save the blockchain to a file.
    '''
    # Save
    filled_chain.save_blockchain_to_file('tests/blockchain_test_2.json')

    # Read
    with open('tests/blockchain_test_2.json') as json_file:
        recreated_chain = Blockchain(chain_data=(json.load(json_file)))

    # Check data and previous_hash of every block
    for i in range(len(filled_chain.chain)):
        assert filled_chain.fetch_a_block(
            i).data == recreated_chain.fetch_a_block(i).data
        assert filled_chain.fetch_a_block(
            i).previous_hash == recreated_chain.fetch_a_block(i).previous_hash


def test_6_check_entire_chain_integrity(filled_chain):
    '''
    TEST 6: Check if the chain is correct by walking through it. And then
    destroying it and running the check again.
    '''
    assert filled_chain.check_chain() is True

    # Destroy the chain for the test
    filled_chain.chain[1].data = "SOMETHING ELSE"
    assert filled_chain.check_chain() is False


def test_7_check_single_block_integrity(filled_chain):
    '''
    TEST 7: Check individual blocks if they have altered data or not.
    '''
    # Read
    with open('tests/blockchain_test_2.json') as json_file:
        filled_chain = Blockchain(chain_data=(json.load(json_file)))

    # Check if block with index 1 is valid in the chain (it should be)
    assert filled_chain.check_block(1) is True

    # Check last block (shouldnt be possible so we get a response for
    # the last block and the function will return True)
    assert filled_chain.check_block(3) is True

    # Check index out of range
    assert filled_chain.check_block(5) is False

    # Change data in block 1 so we can get a response if we check that
    # this block is now invalid.
    temp = filled_chain.chain[1].data
    filled_chain.chain[1].data = "something else"
    assert filled_chain.check_block(1) is False

    filled_chain.chain[1].data = temp
    assert filled_chain.check_block(1) is True


def test_8_read_out_data():
    '''
    TEST 8: Tests loading json file in data of block and searching values in
    the json that is saved in the data part of the block.
    '''
    with open('tests/blockchain_test.json') as json_file:
        test_chain = Blockchain(chain_data=(json.load(json_file)))

    foundBlocks = test_chain.find_blocks("username", "gerard")
    assert len(foundBlocks) == 1

    foundGames = test_chain.find_games("result", "1-0")
    assert len(foundGames['games']) == 3

    foundBlocks = test_chain.find_blocks("result", "1-0")
    assert len(foundBlocks) == 1

    foundGames = test_chain.find_games("username", "gerard")
    assert len(foundGames['games']) == 3


def test_9_saving_individual_blocks(filled_chain):
    '''
    TEST 9: check if we can save individual blocks and recreate them.
    '''
    block_1 = Block(["block_1 data"],
                    filled_chain.fetch_last_block().make_hash())
    block_1.save_block_to_file('tests/block_save.json')

    with open('tests/block_save.json') as json_file:
        data = json.load(json_file)
        recreated_block = Block(data['data'], data['previous_hash'])

    assert block_1.data == recreated_block.data
    assert block_1.previous_hash == recreated_block.previous_hash


def test_10_change_middle_block_data_next_hash(filled_chain):
    '''
    TEST 10: check if it matters if we edit a block in the middle and its next
    neighbors previous_hash.
    '''
    with open('tests/blockchain_test_2.json') as json_file:
        filled_chain = Blockchain(chain_data=(json.load(json_file)))

    filled_chain.add_block(Block(["BLOCK4_data"],
                                 filled_chain.fetch_last_block().make_hash()),
                           'tests/blockchain_test_2.json')
    filled_chain.add_block(Block(["BLOCK5_data"],
                                 filled_chain.fetch_last_block().make_hash()),
                           'tests/blockchain_test_2.json')
    assert filled_chain.check_chain() is True

    filled_chain.chain[1].data = ["something else"]
    filled_chain.chain[2].previous_hash = filled_chain.chain[1].make_hash()
    assert filled_chain.check_chain() is False
