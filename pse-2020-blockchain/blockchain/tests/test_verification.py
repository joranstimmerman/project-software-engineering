"""
This file tests the verification of blocks.
This is to see if tampering is actually prevented by verfification.
"""

__author__ = ["Stan van der Kroon"]
__credits__ = ["Stan van der Kroon"]
__maintainer__ = ["Stan van der Kroon"]

from blockchain_app.node import generate_keys, key_text, \
    sign_transaction, verify_transaction
import pytest


@pytest.fixture(scope='module')
def new_keys():
    pri_key, pub_key = generate_keys(2048)
    return (pri_key, pub_key)


@pytest.fixture(scope='module')
def new_transaction():
    pri_key, pub_key = generate_keys(2048)
    pri_key2, pub_key2 = generate_keys(2048)
    return ({"test": "test",
             "signature1": '',
             "signature2": '',
             "contestant1": key_text(pub_key),
             "contestant2": key_text(pub_key2)},
            key_text(pri_key),
            key_text(pri_key2))


def test_correct_verification(new_transaction):
    pri_key1 = new_transaction[1]
    pri_key2 = new_transaction[2]
    transaction = new_transaction[0]
    sign_transaction(pri_key1, transaction, 1)
    sign_transaction(pri_key2, transaction, 2)
    assert verify_transaction(transaction) is True


def test_altered_transaction(new_transaction):
    pri_key1 = new_transaction[1]
    pri_key2 = new_transaction[2]
    transaction = new_transaction[0]
    sign_transaction(pri_key1, transaction, 1)
    sign_transaction(pri_key2, transaction, 2)
    transaction['test'] = 'tes'
    assert verify_transaction(transaction) is False


def test_verification_wrong_public_key(new_transaction, new_keys):
    pri_key1 = new_transaction[1]
    pri_key2 = new_transaction[2]
    transaction = new_transaction[0]
    transaction['contestant1'] = key_text(new_keys[1])
    sign_transaction(pri_key1, transaction, 1)
    sign_transaction(pri_key2, transaction, 2)
    assert verify_transaction(transaction) is False


def test_verification_without_signatures(new_transaction, new_keys):
    pri_key1 = new_transaction[1]
    pri_key2 = new_transaction[2]
    transaction = new_transaction[0]
    assert verify_transaction(transaction) is False


def test_verification_one_signature(new_transaction, new_keys):
    pri_key1 = new_transaction[1]
    pri_key2 = new_transaction[2]
    transaction = new_transaction[0]
    sign_transaction(pri_key1, transaction, 1)
    assert verify_transaction(transaction) is False


def test_verification_incorrect_transaction_format(new_transaction, new_keys):
    pri_key1 = new_transaction[1]
    pri_key2 = new_transaction[2]
    transaction = new_transaction[0]
    sign_transaction(pri_key1, transaction, 1)
    sign_transaction(pri_key2, transaction, 2)
    del transaction['signature1']
    del transaction['signature2']
    assert verify_transaction(transaction) is False
