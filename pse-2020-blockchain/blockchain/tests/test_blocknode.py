"""
This file tests the BlockNode class file (blocknode.py).
It uses blockchain.json, blockchain_wrong_ test.json in some tests.
Send blocks using locks and requesting blockchains is tested.
"""

__author__ = ["Stan van der Kroon"]
__credits__ = ["Stan van der Kroon", "Joris op ten Berg"]
__maintainer__ = ["Stan van der Kroon"]


from blockchain_app.node import BlockNode, Blockchain, Block
import pytest
import time
import json


class BlockNodeTest(BlockNode):
    def __init__(self, host, port, blockchain):
        """
        The constructor of the BlockNode class.

        :param host: Ip-address of the host
        :type host: String
        :param port: Port that the node listens to.
        :type port: Integer
        :param blockchain: The blockchain.
        :type blockchain: Blockchain object
        """
        super(BlockNodeTest, self).__init__(host, port, blockchain)

    def handle_blockchain_response(self, data):
        """
        Handle receiving a new blockchain. Possible blockchains from
        neighbours are collected and the most common chain is adopted. The
        blockchain should be contained in the 'content' entry of the data.

        :param data: The data of the message.
        :type data: Dictionary
        """
        self.blockchain_neighbors_left -= 1
        self.temp_blockchains.append(Blockchain(data['content']))
        if self.blockchain_neighbors_left == 0:
            candidates = [chain.make_dict().__str__()
                          for chain in self.temp_blockchains
                          if chain.check_chain()]
            if candidates:
                self.chain = Blockchain(eval(max(set(candidates),
                                                 key=candidates.count)))
            elif not self.chain.check_chain():
                self.chain = Blockchain()
            if self.parent_blockchain is not None:
                msg = self.msg('blockchain_response', data['id'],
                               data['timestamp'],
                               content=self.chain.make_dict())
                self.send_to_node(self.parent_blockchain, msg)
                self.parent_blockchain = None
            self.chain.save_blockchain_to_file('tests/blocknode_test.json')
        self.temp_blockchains = []

    def send_block(self):
        """
        Send a block. The block will reach the entire network.
        """
        if self.lock and self.lock_info == self.id:
            new_block = Block(self.queue.copy(),
                              self.chain.fetch_last_block().make_hash())
            if not self.chain.add_block(new_block,
                                        'tests/blocknode_test.json'):
                self.send_lock_release()
                self.send_blockchain_request()
                return

            self.queue = []
            self.lock = False
            self.lock_info = None
            self.parent = None

            exclude_list = self.build_exclude_list()
            msg = self.msg('block', self.id, time.time(), exclude_list,
                           new_block.make_dict())
            self.send_to_nodes(msg)
        else:
            self.send_lock_release()
            pass

    def handle_block(self, data):
        """
        Handle receiving a new block. The block is forwarded to any neighbours
        not yet in the exclude list. The block should be contained in the
        'content' entry of the data.

        :param data: The data of the message.
        :type data: Dictionary
        """
        if self.lock and data['id'] == self.lock_info:
            new_block = Block(*data['content'].values())
            if self.chain.fetch_last_block().make_dict() == data['content']:
                self.lock = False
                self.lock_info = None
                self.parent = None
                return

            if not self.chain.add_block(new_block,
                                        'tests/blocknode_test.json'):
                self.send_lock_release()
                self.send_blockchain_request()
                return
            self.lock = False
            self.lock_info = None
            self.parent = None

            exclude_list = self.build_exclude_list(data['exclude'])
            msg = self.msg('block', data['id'], data['timestamp'],
                           exclude_list, data['content'])
            self.send_to_nodes(msg, exclude=data['exclude'])
        else:
            self.send_lock_release()
            pass


@pytest.fixture(scope='module')
def new_nodes():
    node1 = BlockNodeTest('127.0.0.1', 12346, Blockchain())
    node2 = BlockNodeTest('127.0.0.1', 12347, Blockchain())
    node3 = BlockNodeTest('127.0.0.1', 12348, Blockchain())
    node4 = BlockNodeTest('127.0.0.1', 12349, Blockchain())
    node1.start()
    node2.start()
    node3.start()
    node4.start()
    node1.connect_with_node('127.0.0.1', 12347)
    node2.connect_with_node('127.0.0.1', 12348)
    node3.connect_with_node('127.0.0.1', 12349)
    return node1, node2, node3, node4


def test_send_block(new_nodes):
    node1, node2, node3, node4 = new_nodes
    new_block = Block([{'test': 'test'}],
                      node1.chain.fetch_last_block().make_hash())
    node1.queue.append({'test': 'test'})
    node1.send_lock_request()
    time.sleep(1)
    assert node1.chain.fetch_last_block().make_dict() == \
        new_block.make_dict() and \
        node2.chain.fetch_last_block().make_dict() == \
        new_block.make_dict() and \
        node3.chain.fetch_last_block().make_dict() == \
        new_block.make_dict() and \
        node4.chain.fetch_last_block().make_dict() == new_block.make_dict()


def test_send_blockchain(new_nodes):
    node1, node2, node3, node4 = new_nodes
    with open('tests/blockchain_wrong_test.json') as b:
        node1.chain = Blockchain(json.load(b))
    with open('tests/blockchain_wrong_test.json') as b:
        node2.chain = Blockchain(json.load(b))

    node1.send_blockchain_request()
    time.sleep(1)
    assert node1.chain.make_dict() == node3.chain.make_dict() and \
        node2.chain.make_dict() == node3.chain.make_dict()


def test_send_block_wrong_blockchain(new_nodes):
    node1, node2, node3, node4 = new_nodes

    new_block = Block([{'test2': 'test2'}],
                      node4.chain.fetch_last_block().make_hash())
    node4.chain.add_block(new_block)
    node1.queue.append({'test3': 'test3'})
    node1.send_lock_request()
    time.sleep(1)
    assert node1.chain.make_dict() == node4.chain.make_dict() and \
        node2.chain.make_dict() == node4.chain.make_dict() and \
        node3.chain.make_dict() == node4.chain.make_dict()


def test_send_block_diamond(new_nodes):
    node1, node2, node3, node4 = new_nodes
    node4.connect_with_node('127.0.0.1', 12346)
    time.sleep(1)
    new_block = Block([{'test2': 'test2'}],
                      node1.chain.fetch_last_block().make_hash())
    node1.queue.append({'test2': 'test2'})
    node1.send_lock_request()
    time.sleep(1)
    assert node1.chain.fetch_last_block().make_dict() == \
        new_block.make_dict() and \
        node2.chain.fetch_last_block().make_dict() == \
        new_block.make_dict() and \
        node3.chain.fetch_last_block().make_dict() == \
        new_block.make_dict() and \
        node4.chain.fetch_last_block().make_dict() == \
        new_block.make_dict()


def test_stop_nodes(new_nodes):
    node1, node2, node3, node4 = new_nodes
    node1.stop()
    node2.stop()
    node3.stop()
    node4.stop()
