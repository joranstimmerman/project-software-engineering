## Files

### /blockchain_app

The actual application functionalities are in here.

### /docker

In this directory, a docker container is created to run the local_node.py in a safe manner.

### /tests

The test files for the functionalities implemented in the sibling directories are in here.
The tests in in this directory must be run in a docker environment with the following command:
```
docker-compose exec blockchain pytest
```

### requirements.txt

These are the pyhon packages that will be installed with pip3 in python.

## Installation

Clone or download this repository to a folder of your preference. Make sure the requirements below are installed to be able to run the program.
Follow the installation guide in the root directory's README.

### Requirements

To run this program, you need the following:

* [Docker](https://docs.docker.com/get-docker/)

### Build

The program uses docker for encapsulating the application. Assuring Consistency
across multiple development environments.