"""
run.py does the execution of all the code needed to run the blockchain. It
launches a node and a server (which act through the same docker container)
and these 2 launch the blockchain.
"""

__author__ = ["Jari Andersen", "Joran Timmerman", "Stan van der Kroon",
              "Lennart de Boer", "Joris op ten Berg"]
__credits__ = ["Jari Andersen", "Joran Timmerman", "Stan van der Kroon",
               "Lennart de Boer", "Joris op ten Berg"]
__maintainer__ = ["Jari Andersen", "Joran Timmerman", "Stan van der Kroon",
                  "Lennart de Boer", "Joris op ten Berg"]

from blockchain_app.node import Server
import json

if __name__ == "__main__":
    with open('/project/blockchain_app/node/blockchain.json') as b:
        server = Server('blockchain', 12345, 12344, json.load(b))

    # server.node.debug = True
    server.run([])
