#!/bin/bash

docker network create -d bridge blockchain-network
docker network create -d bridge node-network
docker-compose build
docker-compose up
